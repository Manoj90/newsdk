#
# Be sure to run `pod lib lint HarpoonApi.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = "HarpoonApi"
    s.version          = "1.0.0"

    s.summary          = "harpoon-api"
    s.platform     = :ios, '7.0'
    s.requires_arc = true

    s.framework    = 'SystemConfiguration'

    s.homepage     = "https://Manoj90@bitbucket.org/Manoj90/newsdk"
    s.license      = "Apache License, Version 2.0"
    s.source       = { :git => "https://Manoj90@bitbucket.org/Manoj90/newsdk.git", :tag => "#{s.version}" }
    s.author       = { "Harpoon Connect" => "hello@harpoonconnect.com" }

    s.source_files = 'HarpoonApi/**/*.{m,h}'
    s.public_header_files = 'HarpoonApi/**/*.h'


    s.dependency 'AFNetworking', '~> 3'
    s.dependency 'JSONModel', '~> 1.2'
    s.dependency 'ISO8601', '~> 0.5'
end

