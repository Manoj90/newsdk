# HRPRssFeedGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | 
**sortOrder** | **NSNumber*** |  | [optional] [default to @0.0]
**_id** | **NSNumber*** |  | [optional] 
**appId** | **NSString*** |  | [optional] 
**rssFeeds** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


