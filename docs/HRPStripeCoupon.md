# HRPStripeCoupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object** | **NSString*** |  | [optional] 
**amountOff** | **NSNumber*** |  | [optional] 
**created** | **NSNumber*** |  | [optional] 
**currency** | **NSString*** |  | [optional] 
**durationInMonths** | **NSNumber*** |  | [optional] 
**livemode** | **NSNumber*** |  | [optional] 
**maxRedemptions** | **NSNumber*** |  | [optional] 
**metadata** | **NSString*** |  | [optional] 
**percentOff** | **NSNumber*** |  | [optional] 
**redeemBy** | **NSNumber*** |  | [optional] 
**timesRedeemed** | **NSNumber*** |  | [optional] 
**valid** | **NSNumber*** |  | [optional] 
**duration** | **NSString*** |  | 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


