# HRPStripeInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object** | **NSString*** |  | [optional] 
**amountDue** | **NSNumber*** |  | [optional] 
**applicationFee** | **NSNumber*** |  | [optional] 
**attemptCount** | **NSNumber*** |  | [optional] 
**attempted** | **NSNumber*** |  | [optional] 
**charge** | **NSString*** |  | [optional] 
**closed** | **NSNumber*** |  | [optional] 
**currency** | **NSString*** |  | [optional] 
**date** | **NSNumber*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**endingBalance** | **NSNumber*** |  | [optional] 
**forgiven** | **NSNumber*** |  | [optional] 
**livemode** | **NSNumber*** |  | [optional] 
**metadata** | **NSString*** |  | [optional] 
**nextPaymentAttempt** | **NSNumber*** |  | [optional] 
**paid** | **NSNumber*** |  | [optional] 
**periodEnd** | **NSNumber*** |  | [optional] 
**periodStart** | **NSNumber*** |  | [optional] 
**receiptNumber** | **NSString*** |  | [optional] 
**subscriptionProrationDate** | **NSNumber*** |  | [optional] 
**subtotal** | **NSNumber*** |  | [optional] 
**tax** | **NSNumber*** |  | [optional] 
**taxPercent** | **NSNumber*** |  | [optional] 
**total** | **NSNumber*** |  | [optional] 
**webhooksDeliveredAt** | **NSNumber*** |  | [optional] 
**customer** | **NSString*** |  | 
**discount** | [**HRPStripeDiscount***](HRPStripeDiscount.md) |  | [optional] 
**statmentDescriptor** | **NSString*** |  | [optional] 
**subscription** | [**HRPStripeSubscription***](HRPStripeSubscription.md) |  | [optional] 
**lines** | [**NSArray&lt;HRPStripeInvoiceItem&gt;***](HRPStripeInvoiceItem.md) |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


