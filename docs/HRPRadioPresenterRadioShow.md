# HRPRadioPresenterRadioShow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | [optional] 
**radioPresenterId** | **NSNumber*** |  | [optional] 
**radioShowId** | **NSNumber*** |  | [optional] 
**radioPresenter** | **NSObject*** |  | [optional] 
**radioShow** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


