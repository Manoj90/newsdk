# HRPFormRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadataKey** | **NSString*** |  | 
**type** | **NSString*** |  | [default to @"text"]
**title** | **NSString*** |  | 
**defaultValue** | **NSString*** |  | [optional] 
**placeholder** | **NSString*** |  | [optional] 
**hidden** | **NSNumber*** |  | [optional] [default to @0]
**required** | **NSNumber*** |  | [optional] [default to @1]
**validationRegExp** | **NSString*** |  | [optional] 
**validationMessage** | **NSString*** |  | [optional] 
**selectorOptions** | [**NSArray&lt;HRPFormSelectorOption&gt;***](HRPFormSelectorOption.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


