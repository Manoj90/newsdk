# HRPPlaylistItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **NSString*** | Stream title | [optional] 
**shortDescription** | **NSString*** | Stream short description (on SDKs is under desc) | [optional] 
**image** | **NSString*** | Playlist image | [optional] 
**file** | **NSString*** | Stream file | [optional] 
**type** | **NSString*** | Stream type (used only in JS SDK) | [optional] 
**mediaType** | **NSString*** | Stream media type (e.g. video , audio , radioStream) | [optional] 
**mediaId** | **NSNumber*** | Ad media id (on SDKs is under mediaid) | [optional] 
**order** | **NSNumber*** | Sort order index | [optional] [default to @0.0]
**_id** | **NSNumber*** |  | [optional] 
**playlistId** | **NSNumber*** |  | [optional] 
**playlist** | **NSObject*** |  | [optional] 
**playerSources** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**playerTracks** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**radioShows** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


