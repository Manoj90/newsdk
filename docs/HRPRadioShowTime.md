# HRPRadioShowTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | **NSString*** | Time when the show starts, format HHmm | [optional] [default to @"0000"]
**endTime** | **NSString*** | Time when the show ends, format HHmm | [optional] [default to @"2359"]
**weekday** | **NSNumber*** | Day of the week when the show is live, 1 &#x3D; Monday / 7 &#x3D; Sunday | [optional] [default to @1.0]
**_id** | **NSNumber*** |  | [optional] 
**radioShowId** | **NSNumber*** |  | [optional] 
**radioShow** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


