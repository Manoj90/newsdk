# HRPOfferApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**offerFind**](HRPOfferApi.md#offerfind) | **GET** /Offers | Find all instances of the model matched by filter from the data source.
[**offerFindById**](HRPOfferApi.md#offerfindbyid) | **GET** /Offers/{id} | Find a model instance by {{id}} from the data source.
[**offerReplaceById**](HRPOfferApi.md#offerreplacebyid) | **POST** /Offers/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**offerReplaceOrCreate**](HRPOfferApi.md#offerreplaceorcreate) | **POST** /Offers/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**offerUpsertWithWhere**](HRPOfferApi.md#offerupsertwithwhere) | **POST** /Offers/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **offerFind**
```objc
-(NSNumber*) offerFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPOffer>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPOfferApi*apiInstance = [[HRPOfferApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance offerFindWithFilter:filter
          completionHandler: ^(NSArray<HRPOffer>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPOfferApi->offerFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPOffer>***](HRPOffer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **offerFindById**
```objc
-(NSNumber*) offerFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPOffer* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPOfferApi*apiInstance = [[HRPOfferApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance offerFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPOffer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPOfferApi->offerFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPOffer***](HRPOffer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **offerReplaceById**
```objc
-(NSNumber*) offerReplaceByIdWithId: (NSString*) _id
    data: (HRPOffer*) data
        completionHandler: (void (^)(HRPOffer* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPOffer* data = [[HRPOffer alloc] init]; // Model instance data (optional)

HRPOfferApi*apiInstance = [[HRPOfferApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance offerReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPOffer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPOfferApi->offerReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPOffer***](HRPOffer*.md)| Model instance data | [optional] 

### Return type

[**HRPOffer***](HRPOffer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **offerReplaceOrCreate**
```objc
-(NSNumber*) offerReplaceOrCreateWithData: (HRPOffer*) data
        completionHandler: (void (^)(HRPOffer* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPOffer* data = [[HRPOffer alloc] init]; // Model instance data (optional)

HRPOfferApi*apiInstance = [[HRPOfferApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance offerReplaceOrCreateWithData:data
          completionHandler: ^(HRPOffer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPOfferApi->offerReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPOffer***](HRPOffer*.md)| Model instance data | [optional] 

### Return type

[**HRPOffer***](HRPOffer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **offerUpsertWithWhere**
```objc
-(NSNumber*) offerUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPOffer*) data
        completionHandler: (void (^)(HRPOffer* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPOffer* data = [[HRPOffer alloc] init]; // An object of model property name/value pairs (optional)

HRPOfferApi*apiInstance = [[HRPOfferApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance offerUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPOffer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPOfferApi->offerUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPOffer***](HRPOffer*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPOffer***](HRPOffer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

