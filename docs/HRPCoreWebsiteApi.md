# HRPCoreWebsiteApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**coreWebsiteCount**](HRPCoreWebsiteApi.md#corewebsitecount) | **GET** /CoreWebsites/count | Count instances of the model matched by where from the data source.
[**coreWebsiteCreate**](HRPCoreWebsiteApi.md#corewebsitecreate) | **POST** /CoreWebsites | Create a new instance of the model and persist it into the data source.
[**coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream**](HRPCoreWebsiteApi.md#corewebsitecreatechangestreamgetcorewebsiteschangestream) | **GET** /CoreWebsites/change-stream | Create a change stream.
[**coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream**](HRPCoreWebsiteApi.md#corewebsitecreatechangestreampostcorewebsiteschangestream) | **POST** /CoreWebsites/change-stream | Create a change stream.
[**coreWebsiteDeleteById**](HRPCoreWebsiteApi.md#corewebsitedeletebyid) | **DELETE** /CoreWebsites/{id} | Delete a model instance by {{id}} from the data source.
[**coreWebsiteExistsGetCoreWebsitesidExists**](HRPCoreWebsiteApi.md#corewebsiteexistsgetcorewebsitesidexists) | **GET** /CoreWebsites/{id}/exists | Check whether a model instance exists in the data source.
[**coreWebsiteExistsHeadCoreWebsitesid**](HRPCoreWebsiteApi.md#corewebsiteexistsheadcorewebsitesid) | **HEAD** /CoreWebsites/{id} | Check whether a model instance exists in the data source.
[**coreWebsiteFind**](HRPCoreWebsiteApi.md#corewebsitefind) | **GET** /CoreWebsites | Find all instances of the model matched by filter from the data source.
[**coreWebsiteFindById**](HRPCoreWebsiteApi.md#corewebsitefindbyid) | **GET** /CoreWebsites/{id} | Find a model instance by {{id}} from the data source.
[**coreWebsiteFindOne**](HRPCoreWebsiteApi.md#corewebsitefindone) | **GET** /CoreWebsites/findOne | Find first instance of the model matched by filter from the data source.
[**coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid**](HRPCoreWebsiteApi.md#corewebsiteprototypeupdateattributespatchcorewebsitesid) | **PATCH** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
[**coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid**](HRPCoreWebsiteApi.md#corewebsiteprototypeupdateattributesputcorewebsitesid) | **PUT** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
[**coreWebsiteReplaceById**](HRPCoreWebsiteApi.md#corewebsitereplacebyid) | **POST** /CoreWebsites/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**coreWebsiteReplaceOrCreate**](HRPCoreWebsiteApi.md#corewebsitereplaceorcreate) | **POST** /CoreWebsites/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**coreWebsiteUpdateAll**](HRPCoreWebsiteApi.md#corewebsiteupdateall) | **POST** /CoreWebsites/update | Update instances of the model matched by {{where}} from the data source.
[**coreWebsiteUpsertPatchCoreWebsites**](HRPCoreWebsiteApi.md#corewebsiteupsertpatchcorewebsites) | **PATCH** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
[**coreWebsiteUpsertPutCoreWebsites**](HRPCoreWebsiteApi.md#corewebsiteupsertputcorewebsites) | **PUT** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
[**coreWebsiteUpsertWithWhere**](HRPCoreWebsiteApi.md#corewebsiteupsertwithwhere) | **POST** /CoreWebsites/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **coreWebsiteCount**
```objc
-(NSNumber*) coreWebsiteCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance coreWebsiteCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteCreate**
```objc
-(NSNumber*) coreWebsiteCreateWithData: (HRPCoreWebsite*) data
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCoreWebsite* data = [[HRPCoreWebsite alloc] init]; // Model instance data (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance coreWebsiteCreateWithData:data
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCoreWebsite***](HRPCoreWebsite*.md)| Model instance data | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream**
```objc
-(NSNumber*) coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Create a change stream.
[apiInstance coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream**
```objc
-(NSNumber*) coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Create a change stream.
[apiInstance coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteDeleteById**
```objc
-(NSNumber*) coreWebsiteDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance coreWebsiteDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteExistsGetCoreWebsitesidExists**
```objc
-(NSNumber*) coreWebsiteExistsGetCoreWebsitesidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance coreWebsiteExistsGetCoreWebsitesidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteExistsGetCoreWebsitesidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteExistsHeadCoreWebsitesid**
```objc
-(NSNumber*) coreWebsiteExistsHeadCoreWebsitesidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance coreWebsiteExistsHeadCoreWebsitesidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteExistsHeadCoreWebsitesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteFind**
```objc
-(NSNumber*) coreWebsiteFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCoreWebsite>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance coreWebsiteFindWithFilter:filter
          completionHandler: ^(NSArray<HRPCoreWebsite>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPCoreWebsite>***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteFindById**
```objc
-(NSNumber*) coreWebsiteFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance coreWebsiteFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteFindOne**
```objc
-(NSNumber*) coreWebsiteFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance coreWebsiteFindOneWithFilter:filter
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid**
```objc
-(NSNumber*) coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesidWithId: (NSString*) _id
    data: (HRPCoreWebsite*) data
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CoreWebsite id
HRPCoreWebsite* data = [[HRPCoreWebsite alloc] init]; // An object of model property name/value pairs (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesidWithId:_id
              data:data
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CoreWebsite id | 
 **data** | [**HRPCoreWebsite***](HRPCoreWebsite*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid**
```objc
-(NSNumber*) coreWebsitePrototypeUpdateAttributesPutCoreWebsitesidWithId: (NSString*) _id
    data: (HRPCoreWebsite*) data
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CoreWebsite id
HRPCoreWebsite* data = [[HRPCoreWebsite alloc] init]; // An object of model property name/value pairs (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance coreWebsitePrototypeUpdateAttributesPutCoreWebsitesidWithId:_id
              data:data
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CoreWebsite id | 
 **data** | [**HRPCoreWebsite***](HRPCoreWebsite*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteReplaceById**
```objc
-(NSNumber*) coreWebsiteReplaceByIdWithId: (NSString*) _id
    data: (HRPCoreWebsite*) data
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCoreWebsite* data = [[HRPCoreWebsite alloc] init]; // Model instance data (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance coreWebsiteReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCoreWebsite***](HRPCoreWebsite*.md)| Model instance data | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteReplaceOrCreate**
```objc
-(NSNumber*) coreWebsiteReplaceOrCreateWithData: (HRPCoreWebsite*) data
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCoreWebsite* data = [[HRPCoreWebsite alloc] init]; // Model instance data (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance coreWebsiteReplaceOrCreateWithData:data
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCoreWebsite***](HRPCoreWebsite*.md)| Model instance data | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteUpdateAll**
```objc
-(NSNumber*) coreWebsiteUpdateAllWithWhere: (NSString*) where
    data: (HRPCoreWebsite*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCoreWebsite* data = [[HRPCoreWebsite alloc] init]; // An object of model property name/value pairs (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance coreWebsiteUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCoreWebsite***](HRPCoreWebsite*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteUpsertPatchCoreWebsites**
```objc
-(NSNumber*) coreWebsiteUpsertPatchCoreWebsitesWithData: (HRPCoreWebsite*) data
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCoreWebsite* data = [[HRPCoreWebsite alloc] init]; // Model instance data (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance coreWebsiteUpsertPatchCoreWebsitesWithData:data
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteUpsertPatchCoreWebsites: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCoreWebsite***](HRPCoreWebsite*.md)| Model instance data | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteUpsertPutCoreWebsites**
```objc
-(NSNumber*) coreWebsiteUpsertPutCoreWebsitesWithData: (HRPCoreWebsite*) data
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCoreWebsite* data = [[HRPCoreWebsite alloc] init]; // Model instance data (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance coreWebsiteUpsertPutCoreWebsitesWithData:data
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteUpsertPutCoreWebsites: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCoreWebsite***](HRPCoreWebsite*.md)| Model instance data | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **coreWebsiteUpsertWithWhere**
```objc
-(NSNumber*) coreWebsiteUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPCoreWebsite*) data
        completionHandler: (void (^)(HRPCoreWebsite* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCoreWebsite* data = [[HRPCoreWebsite alloc] init]; // An object of model property name/value pairs (optional)

HRPCoreWebsiteApi*apiInstance = [[HRPCoreWebsiteApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance coreWebsiteUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPCoreWebsite* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCoreWebsiteApi->coreWebsiteUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCoreWebsite***](HRPCoreWebsite*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCoreWebsite***](HRPCoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

