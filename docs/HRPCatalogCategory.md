# HRPCatalogCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**parentId** | **NSNumber*** |  | 
**createdAt** | **NSDate*** |  | [optional] 
**updatedAt** | **NSDate*** |  | [optional] 
**path** | **NSString*** |  | 
**position** | **NSNumber*** |  | 
**level** | **NSNumber*** |  | 
**childrenCount** | **NSNumber*** |  | 
**storeId** | **NSNumber*** |  | 
**allChildren** | **NSString*** |  | [optional] 
**availableSortBy** | **NSString*** |  | [optional] 
**children** | **NSString*** |  | [optional] 
**customApplyToProducts** | **NSNumber*** |  | [optional] 
**customDesign** | **NSString*** |  | [optional] 
**customDesignFrom** | **NSDate*** |  | [optional] 
**customDesignTo** | **NSDate*** |  | [optional] 
**customLayoutUpdate** | **NSString*** |  | [optional] 
**customUseParentSettings** | **NSNumber*** |  | [optional] 
**defaultSortBy** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**displayMode** | **NSString*** |  | [optional] 
**filterPriceRange** | **NSString*** |  | [optional] 
**image** | **NSString*** |  | [optional] 
**includeInMenu** | **NSNumber*** |  | [optional] 
**isActive** | **NSNumber*** |  | [optional] 
**isAnchor** | **NSNumber*** |  | [optional] 
**landingPage** | **NSNumber*** |  | [optional] 
**metaDescription** | **NSString*** |  | [optional] 
**metaKeywords** | **NSString*** |  | [optional] 
**metaTitle** | **NSString*** |  | [optional] 
**name** | **NSString*** |  | [optional] 
**pageLayout** | **NSString*** |  | [optional] 
**pathInStore** | **NSString*** |  | [optional] 
**thumbnail** | **NSString*** |  | [optional] 
**ummCatLabel** | **NSString*** |  | [optional] 
**ummCatTarget** | **NSString*** |  | [optional] 
**ummDdBlocks** | **NSString*** |  | [optional] 
**ummDdColumns** | **NSNumber*** |  | [optional] 
**ummDdProportions** | **NSString*** |  | [optional] 
**ummDdType** | **NSString*** |  | [optional] 
**ummDdWidth** | **NSString*** |  | [optional] 
**urlKey** | **NSString*** |  | [optional] 
**urlPath** | **NSString*** |  | [optional] 
**catalogProducts** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


