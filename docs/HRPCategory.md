# HRPCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | [optional] 
**productCount** | **NSNumber*** |  | [optional] [default to @0.0]
**hasChildren** | **NSNumber*** |  | [optional] [default to @0]
**parent** | [**HRPCategory***](HRPCategory.md) |  | [optional] 
**isPrimary** | **NSNumber*** |  | [optional] [default to @0]
**children** | [**NSArray&lt;HRPCategory&gt;***](HRPCategory.md) |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


