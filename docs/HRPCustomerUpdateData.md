# HRPCustomerUpdateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **NSString*** | New first name for the Customer | 
**lastName** | **NSString*** | New last name for the Customer | [optional] 
**email** | **NSString*** | New email for the Customer | [optional] 
**gender** | **NSString*** | New gender for the Customer | [optional] 
**dob** | **NSString*** | New date of birth for the Customer | [optional] 
**metadata** | **NSObject*** | New metadata collection for the Customer | [optional] 
**privacy** | [**HRPCustomerPrivacy***](HRPCustomerPrivacy.md) | New privacy settings for the Customer | [optional] 
**profilePictureUpload** | [**HRPMagentoImageUpload***](HRPMagentoImageUpload.md) | New profile picture for the Customer | [optional] 
**coverUpload** | [**HRPMagentoImageUpload***](HRPMagentoImageUpload.md) | New cover photo for the customer | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


