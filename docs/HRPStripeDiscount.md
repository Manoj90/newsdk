# HRPStripeDiscount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object** | **NSString*** |  | [optional] 
**customer** | **NSString*** |  | [optional] 
**end** | **NSNumber*** |  | [optional] 
**start** | **NSNumber*** |  | [optional] 
**subscription** | **NSString*** |  | [optional] 
**coupon** | [**HRPStripeCoupon***](HRPStripeCoupon.md) |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


