# HRPCustomerNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **NSString*** |  | [optional] 
**cover** | **NSString*** |  | [optional] 
**coverUpload** | [**HRPMagentoImageUpload***](HRPMagentoImageUpload.md) |  | [optional] 
**from** | [**HRPNotificationFrom***](HRPNotificationFrom.md) |  | [optional] 
**related** | [**HRPNotificationRelated***](HRPNotificationRelated.md) |  | [optional] 
**link** | **NSString*** |  | [optional] 
**actionCode** | **NSString*** |  | [optional] [default to @"post"]
**status** | **NSString*** |  | [optional] [default to @"unread"]
**sentAt** | **NSDate*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


