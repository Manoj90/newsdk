# HRPBranchConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**branchLiveKey** | **NSString*** |  | [optional] 
**branchTestKey** | **NSString*** |  | [optional] 
**branchLiveUrl** | **NSString*** |  | [optional] 
**branchTestUrl** | **NSString*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


