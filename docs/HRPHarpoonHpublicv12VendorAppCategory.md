# HRPHarpoonHpublicv12VendorAppCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**appId** | **NSNumber*** |  | 
**vendorId** | **NSNumber*** |  | 
**categoryId** | **NSNumber*** |  | 
**isPrimary** | **NSNumber*** |  | [optional] [default to @0.0]
**createdAt** | **NSDate*** |  | [optional] 
**updatedAt** | **NSDate*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


