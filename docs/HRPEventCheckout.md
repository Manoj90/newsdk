# HRPEventCheckout

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cartId** | **NSNumber*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**status** | **NSString*** |  | [default to @"unknown"]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


