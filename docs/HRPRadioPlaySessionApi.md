# HRPRadioPlaySessionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPlaySessionCount**](HRPRadioPlaySessionApi.md#radioplaysessioncount) | **GET** /RadioPlaySessions/count | Count instances of the model matched by where from the data source.
[**radioPlaySessionCreate**](HRPRadioPlaySessionApi.md#radioplaysessioncreate) | **POST** /RadioPlaySessions | Create a new instance of the model and persist it into the data source.
[**radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream**](HRPRadioPlaySessionApi.md#radioplaysessioncreatechangestreamgetradioplaysessionschangestream) | **GET** /RadioPlaySessions/change-stream | Create a change stream.
[**radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream**](HRPRadioPlaySessionApi.md#radioplaysessioncreatechangestreampostradioplaysessionschangestream) | **POST** /RadioPlaySessions/change-stream | Create a change stream.
[**radioPlaySessionDeleteById**](HRPRadioPlaySessionApi.md#radioplaysessiondeletebyid) | **DELETE** /RadioPlaySessions/{id} | Delete a model instance by {{id}} from the data source.
[**radioPlaySessionExistsGetRadioPlaySessionsidExists**](HRPRadioPlaySessionApi.md#radioplaysessionexistsgetradioplaysessionsidexists) | **GET** /RadioPlaySessions/{id}/exists | Check whether a model instance exists in the data source.
[**radioPlaySessionExistsHeadRadioPlaySessionsid**](HRPRadioPlaySessionApi.md#radioplaysessionexistsheadradioplaysessionsid) | **HEAD** /RadioPlaySessions/{id} | Check whether a model instance exists in the data source.
[**radioPlaySessionFind**](HRPRadioPlaySessionApi.md#radioplaysessionfind) | **GET** /RadioPlaySessions | Find all instances of the model matched by filter from the data source.
[**radioPlaySessionFindById**](HRPRadioPlaySessionApi.md#radioplaysessionfindbyid) | **GET** /RadioPlaySessions/{id} | Find a model instance by {{id}} from the data source.
[**radioPlaySessionFindOne**](HRPRadioPlaySessionApi.md#radioplaysessionfindone) | **GET** /RadioPlaySessions/findOne | Find first instance of the model matched by filter from the data source.
[**radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid**](HRPRadioPlaySessionApi.md#radioplaysessionprototypeupdateattributespatchradioplaysessionsid) | **PATCH** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid**](HRPRadioPlaySessionApi.md#radioplaysessionprototypeupdateattributesputradioplaysessionsid) | **PUT** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPlaySessionReplaceById**](HRPRadioPlaySessionApi.md#radioplaysessionreplacebyid) | **POST** /RadioPlaySessions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPlaySessionReplaceOrCreate**](HRPRadioPlaySessionApi.md#radioplaysessionreplaceorcreate) | **POST** /RadioPlaySessions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpdateAll**](HRPRadioPlaySessionApi.md#radioplaysessionupdateall) | **POST** /RadioPlaySessions/update | Update instances of the model matched by {{where}} from the data source.
[**radioPlaySessionUpsertPatchRadioPlaySessions**](HRPRadioPlaySessionApi.md#radioplaysessionupsertpatchradioplaysessions) | **PATCH** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpsertPutRadioPlaySessions**](HRPRadioPlaySessionApi.md#radioplaysessionupsertputradioplaysessions) | **PUT** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpsertWithWhere**](HRPRadioPlaySessionApi.md#radioplaysessionupsertwithwhere) | **POST** /RadioPlaySessions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioPlaySessionCount**
```objc
-(NSNumber*) radioPlaySessionCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance radioPlaySessionCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionCreate**
```objc
-(NSNumber*) radioPlaySessionCreateWithData: (HRPRadioPlaySession*) data
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPlaySession* data = [[HRPRadioPlaySession alloc] init]; // Model instance data (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance radioPlaySessionCreateWithData:data
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPlaySession***](HRPRadioPlaySession*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream**
```objc
-(NSNumber*) radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Create a change stream.
[apiInstance radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream**
```objc
-(NSNumber*) radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Create a change stream.
[apiInstance radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionDeleteById**
```objc
-(NSNumber*) radioPlaySessionDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance radioPlaySessionDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionExistsGetRadioPlaySessionsidExists**
```objc
-(NSNumber*) radioPlaySessionExistsGetRadioPlaySessionsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioPlaySessionExistsGetRadioPlaySessionsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionExistsGetRadioPlaySessionsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionExistsHeadRadioPlaySessionsid**
```objc
-(NSNumber*) radioPlaySessionExistsHeadRadioPlaySessionsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioPlaySessionExistsHeadRadioPlaySessionsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionExistsHeadRadioPlaySessionsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionFind**
```objc
-(NSNumber*) radioPlaySessionFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioPlaySession>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance radioPlaySessionFindWithFilter:filter
          completionHandler: ^(NSArray<HRPRadioPlaySession>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPRadioPlaySession>***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionFindById**
```objc
-(NSNumber*) radioPlaySessionFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance radioPlaySessionFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionFindOne**
```objc
-(NSNumber*) radioPlaySessionFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance radioPlaySessionFindOneWithFilter:filter
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid**
```objc
-(NSNumber*) radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsidWithId: (NSString*) _id
    data: (HRPRadioPlaySession*) data
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPlaySession id
HRPRadioPlaySession* data = [[HRPRadioPlaySession alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsidWithId:_id
              data:data
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPlaySession id | 
 **data** | [**HRPRadioPlaySession***](HRPRadioPlaySession*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid**
```objc
-(NSNumber*) radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsidWithId: (NSString*) _id
    data: (HRPRadioPlaySession*) data
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPlaySession id
HRPRadioPlaySession* data = [[HRPRadioPlaySession alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsidWithId:_id
              data:data
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPlaySession id | 
 **data** | [**HRPRadioPlaySession***](HRPRadioPlaySession*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionReplaceById**
```objc
-(NSNumber*) radioPlaySessionReplaceByIdWithId: (NSString*) _id
    data: (HRPRadioPlaySession*) data
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPRadioPlaySession* data = [[HRPRadioPlaySession alloc] init]; // Model instance data (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance radioPlaySessionReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPRadioPlaySession***](HRPRadioPlaySession*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionReplaceOrCreate**
```objc
-(NSNumber*) radioPlaySessionReplaceOrCreateWithData: (HRPRadioPlaySession*) data
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPlaySession* data = [[HRPRadioPlaySession alloc] init]; // Model instance data (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance radioPlaySessionReplaceOrCreateWithData:data
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPlaySession***](HRPRadioPlaySession*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionUpdateAll**
```objc
-(NSNumber*) radioPlaySessionUpdateAllWithWhere: (NSString*) where
    data: (HRPRadioPlaySession*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioPlaySession* data = [[HRPRadioPlaySession alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance radioPlaySessionUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioPlaySession***](HRPRadioPlaySession*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionUpsertPatchRadioPlaySessions**
```objc
-(NSNumber*) radioPlaySessionUpsertPatchRadioPlaySessionsWithData: (HRPRadioPlaySession*) data
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPlaySession* data = [[HRPRadioPlaySession alloc] init]; // Model instance data (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioPlaySessionUpsertPatchRadioPlaySessionsWithData:data
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionUpsertPatchRadioPlaySessions: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPlaySession***](HRPRadioPlaySession*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionUpsertPutRadioPlaySessions**
```objc
-(NSNumber*) radioPlaySessionUpsertPutRadioPlaySessionsWithData: (HRPRadioPlaySession*) data
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPlaySession* data = [[HRPRadioPlaySession alloc] init]; // Model instance data (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioPlaySessionUpsertPutRadioPlaySessionsWithData:data
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionUpsertPutRadioPlaySessions: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPlaySession***](HRPRadioPlaySession*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPlaySessionUpsertWithWhere**
```objc
-(NSNumber*) radioPlaySessionUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPRadioPlaySession*) data
        completionHandler: (void (^)(HRPRadioPlaySession* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioPlaySession* data = [[HRPRadioPlaySession alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPlaySessionApi*apiInstance = [[HRPRadioPlaySessionApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance radioPlaySessionUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPRadioPlaySession* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPlaySessionApi->radioPlaySessionUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioPlaySession***](HRPRadioPlaySession*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioPlaySession***](HRPRadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

