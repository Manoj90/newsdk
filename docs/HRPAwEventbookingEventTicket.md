# HRPAwEventbookingEventTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**eventId** | **NSNumber*** |  | 
**price** | **NSString*** |  | [optional] 
**priceType** | **NSString*** |  | [optional] 
**sku** | **NSString*** |  | [optional] 
**qty** | **NSNumber*** |  | [optional] 
**codeprefix** | **NSString*** |  | [optional] 
**fee** | **NSString*** |  | 
**isPassOnFee** | **NSNumber*** |  | 
**chance** | **NSNumber*** |  | 
**attributes** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**tickets** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**awEventbookingTicket** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


