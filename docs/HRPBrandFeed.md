# HRPBrandFeed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand** | [**HRPBrand***](HRPBrand.md) |  | [optional] 
**message** | **NSString*** |  | [optional] 
**cover** | **NSString*** |  | [optional] 
**related** | [**HRPAnonymousModel8***](HRPAnonymousModel8.md) |  | [optional] 
**link** | **NSString*** |  | [optional] 
**actionCode** | **NSString*** |  | [optional] 
**privacyCode** | **NSString*** |  | [optional] 
**postedAt** | **NSDate*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


