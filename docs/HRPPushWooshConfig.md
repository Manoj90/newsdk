# HRPPushWooshConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appName** | **NSString*** |  | [optional] 
**appId** | **NSString*** |  | 
**appToken** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


