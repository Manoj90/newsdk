# HRPAwCollpurDealPurchases

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**dealId** | **NSNumber*** |  | 
**orderId** | **NSNumber*** |  | 
**orderItemId** | **NSNumber*** |  | 
**qtyPurchased** | **NSNumber*** |  | 
**qtyWithCoupons** | **NSNumber*** |  | 
**customerName** | **NSString*** |  | 
**customerId** | **NSNumber*** |  | 
**purchaseDateTime** | **NSDate*** |  | [optional] 
**shippingAmount** | **NSString*** |  | 
**qtyOrdered** | **NSString*** |  | 
**refundState** | **NSNumber*** |  | [optional] 
**isSuccessedFlag** | **NSNumber*** |  | [optional] 
**awCollpurCoupon** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


