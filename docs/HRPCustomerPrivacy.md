# HRPCustomerPrivacy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activity** | **NSString*** |  | [default to @"public"]
**notificationPush** | **NSNumber*** |  | [optional] [default to @1]
**notificationBeacon** | **NSNumber*** |  | [optional] [default to @1]
**notificationLocation** | **NSNumber*** |  | [optional] [default to @1]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


