# HRPAppColor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appPrimary** | **NSString*** |  | [default to @"#027fd3"]
**appSecondary** | **NSString*** |  | [optional] [default to @"#26ff00"]
**textPrimary** | **NSString*** |  | [optional] [default to @"#049975"]
**textSecondary** | **NSString*** |  | [optional] [default to @"#4e5eea"]
**radioNavBar** | **NSString*** |  | [optional] [default to @"#87878c"]
**radioPlayer** | **NSString*** |  | [optional] [default to @"#af2768"]
**radioProgressBar** | **NSString*** |  | [optional] [default to @"#ff00c9"]
**menuBackground** | **NSString*** |  | [optional] [default to @"#000000"]
**radioTextSecondary** | **NSString*** |  | [optional] [default to @"#000000"]
**radioTextPrimary** | **NSString*** |  | [optional] [default to @"#000000"]
**menuItem** | **NSString*** |  | [optional] [default to @"#000000"]
**appGradientPrimary** | **NSString*** |  | [optional] [default to @"#000000"]
**feedSegmentIndicator** | **NSString*** |  | [optional] [default to @"#ff0bc6"]
**radioPlayButton** | **NSString*** |  | [optional] [default to @"#000000"]
**radioPauseButton** | **NSString*** |  | [optional] [default to @"#000000"]
**radioPlayButtonBackground** | **NSString*** |  | [optional] [default to @"#000000"]
**radioPauseButtonBackground** | **NSString*** |  | [optional] [default to @"#000000"]
**radioPlayerItem** | **NSString*** |  | [optional] [default to @"#000000"]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


