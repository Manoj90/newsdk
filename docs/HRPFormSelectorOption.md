# HRPFormSelectorOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **NSString*** |  | 
**value** | **NSString*** |  | 
**format** | **NSString*** |  | [optional] [default to @"text"]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


