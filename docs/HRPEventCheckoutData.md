# HRPEventCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tickets** | [**NSArray&lt;HRPEventCheckoutTicketData&gt;***](HRPEventCheckoutTicketData.md) | Tickets to checkout | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


