# HRPMenuItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | [optional] 
**url** | **NSString*** |  | [optional] 
**image** | **NSString*** |  | [optional] 
**visibility** | **NSNumber*** |  | [optional] 
**sortOrder** | **NSNumber*** |  | [optional] 
**pageAction** | **NSNumber*** |  | [optional] 
**pageActionIcon** | **NSString*** |  | [optional] 
**pageActionLink** | **NSString*** |  | [optional] 
**styleCss** | **NSString*** |  | [optional] 
**featureRadioDontShowOnCamera** | **NSNumber*** |  | [optional] [default to @1]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


