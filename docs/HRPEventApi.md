# HRPEventApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**eventAttendees**](HRPEventApi.md#eventattendees) | **GET** /Events/{id}/attendees | 
[**eventCheckout**](HRPEventApi.md#eventcheckout) | **POST** /Events/{id}/checkout | 
[**eventFind**](HRPEventApi.md#eventfind) | **GET** /Events | Find all instances of the model matched by filter from the data source.
[**eventFindById**](HRPEventApi.md#eventfindbyid) | **GET** /Events/{id} | Find a model instance by {{id}} from the data source.
[**eventFindInLater**](HRPEventApi.md#eventfindinlater) | **GET** /Events/later | 
[**eventFindInThisMonth**](HRPEventApi.md#eventfindinthismonth) | **GET** /Events/thisMonth | 
[**eventFindOne**](HRPEventApi.md#eventfindone) | **GET** /Events/findOne | Find first instance of the model matched by filter from the data source.
[**eventRedeem**](HRPEventApi.md#eventredeem) | **POST** /Events/{id}/redeem | 
[**eventReplaceById**](HRPEventApi.md#eventreplacebyid) | **POST** /Events/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**eventReplaceOrCreate**](HRPEventApi.md#eventreplaceorcreate) | **POST** /Events/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**eventUpsertWithWhere**](HRPEventApi.md#eventupsertwithwhere) | **POST** /Events/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**eventVenues**](HRPEventApi.md#eventvenues) | **GET** /Events/{id}/venues | 


# **eventAttendees**
```objc
-(NSNumber*) eventAttendeesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPEventAttendee>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

[apiInstance eventAttendeesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPEventAttendee>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventAttendees: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPEventAttendee>***](HRPEventAttendee.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventCheckout**
```objc
-(NSNumber*) eventCheckoutWithId: (NSString*) _id
    data: (HRPEventCheckoutData*) data
        completionHandler: (void (^)(HRPEventCheckout* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPEventCheckoutData* data = [[HRPEventCheckoutData alloc] init]; //  (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

[apiInstance eventCheckoutWithId:_id
              data:data
          completionHandler: ^(HRPEventCheckout* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventCheckout: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPEventCheckoutData***](HRPEventCheckoutData*.md)|  | [optional] 

### Return type

[**HRPEventCheckout***](HRPEventCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventFind**
```objc
-(NSNumber*) eventFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPEvent>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance eventFindWithFilter:filter
          completionHandler: ^(NSArray<HRPEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPEvent>***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventFindById**
```objc
-(NSNumber*) eventFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPEvent* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance eventFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPEvent***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventFindInLater**
```objc
-(NSNumber*) eventFindInLaterWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPEvent>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

[apiInstance eventFindInLaterWithFilter:filter
          completionHandler: ^(NSArray<HRPEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventFindInLater: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPEvent>***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventFindInThisMonth**
```objc
-(NSNumber*) eventFindInThisMonthWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPEvent>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

[apiInstance eventFindInThisMonthWithFilter:filter
          completionHandler: ^(NSArray<HRPEvent>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventFindInThisMonth: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPEvent>***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventFindOne**
```objc
-(NSNumber*) eventFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPEvent* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance eventFindOneWithFilter:filter
          completionHandler: ^(HRPEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPEvent***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventRedeem**
```objc
-(NSNumber*) eventRedeemWithId: (NSString*) _id
    data: (HRPEventRedeemData*) data
        completionHandler: (void (^)(HRPEventPurchase* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPEventRedeemData* data = [[HRPEventRedeemData alloc] init]; //  (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

[apiInstance eventRedeemWithId:_id
              data:data
          completionHandler: ^(HRPEventPurchase* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventRedeem: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPEventRedeemData***](HRPEventRedeemData*.md)|  | [optional] 

### Return type

[**HRPEventPurchase***](HRPEventPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventReplaceById**
```objc
-(NSNumber*) eventReplaceByIdWithId: (NSString*) _id
    data: (HRPEvent*) data
        completionHandler: (void (^)(HRPEvent* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPEvent* data = [[HRPEvent alloc] init]; // Model instance data (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance eventReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPEvent***](HRPEvent*.md)| Model instance data | [optional] 

### Return type

[**HRPEvent***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventReplaceOrCreate**
```objc
-(NSNumber*) eventReplaceOrCreateWithData: (HRPEvent*) data
        completionHandler: (void (^)(HRPEvent* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPEvent* data = [[HRPEvent alloc] init]; // Model instance data (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance eventReplaceOrCreateWithData:data
          completionHandler: ^(HRPEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPEvent***](HRPEvent*.md)| Model instance data | [optional] 

### Return type

[**HRPEvent***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventUpsertWithWhere**
```objc
-(NSNumber*) eventUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPEvent*) data
        completionHandler: (void (^)(HRPEvent* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPEvent* data = [[HRPEvent alloc] init]; // An object of model property name/value pairs (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance eventUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPEvent***](HRPEvent*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPEvent***](HRPEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **eventVenues**
```objc
-(NSNumber*) eventVenuesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPVenue>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPEventApi*apiInstance = [[HRPEventApi alloc] init];

[apiInstance eventVenuesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPVenue>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPEventApi->eventVenues: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPVenue>***](HRPVenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

