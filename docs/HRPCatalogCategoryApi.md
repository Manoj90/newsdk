# HRPCatalogCategoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogCategoryCount**](HRPCatalogCategoryApi.md#catalogcategorycount) | **GET** /CatalogCategories/count | Count instances of the model matched by where from the data source.
[**catalogCategoryCreate**](HRPCatalogCategoryApi.md#catalogcategorycreate) | **POST** /CatalogCategories | Create a new instance of the model and persist it into the data source.
[**catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream**](HRPCatalogCategoryApi.md#catalogcategorycreatechangestreamgetcatalogcategorieschangestream) | **GET** /CatalogCategories/change-stream | Create a change stream.
[**catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream**](HRPCatalogCategoryApi.md#catalogcategorycreatechangestreampostcatalogcategorieschangestream) | **POST** /CatalogCategories/change-stream | Create a change stream.
[**catalogCategoryDeleteById**](HRPCatalogCategoryApi.md#catalogcategorydeletebyid) | **DELETE** /CatalogCategories/{id} | Delete a model instance by {{id}} from the data source.
[**catalogCategoryExistsGetCatalogCategoriesidExists**](HRPCatalogCategoryApi.md#catalogcategoryexistsgetcatalogcategoriesidexists) | **GET** /CatalogCategories/{id}/exists | Check whether a model instance exists in the data source.
[**catalogCategoryExistsHeadCatalogCategoriesid**](HRPCatalogCategoryApi.md#catalogcategoryexistsheadcatalogcategoriesid) | **HEAD** /CatalogCategories/{id} | Check whether a model instance exists in the data source.
[**catalogCategoryFind**](HRPCatalogCategoryApi.md#catalogcategoryfind) | **GET** /CatalogCategories | Find all instances of the model matched by filter from the data source.
[**catalogCategoryFindById**](HRPCatalogCategoryApi.md#catalogcategoryfindbyid) | **GET** /CatalogCategories/{id} | Find a model instance by {{id}} from the data source.
[**catalogCategoryFindOne**](HRPCatalogCategoryApi.md#catalogcategoryfindone) | **GET** /CatalogCategories/findOne | Find first instance of the model matched by filter from the data source.
[**catalogCategoryPrototypeCountCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypecountcatalogproducts) | **GET** /CatalogCategories/{id}/catalogProducts/count | Counts catalogProducts of CatalogCategory.
[**catalogCategoryPrototypeCreateCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypecreatecatalogproducts) | **POST** /CatalogCategories/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
[**catalogCategoryPrototypeDeleteCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypedeletecatalogproducts) | **DELETE** /CatalogCategories/{id}/catalogProducts | Deletes all catalogProducts of this model.
[**catalogCategoryPrototypeDestroyByIdCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypedestroybyidcatalogproducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
[**catalogCategoryPrototypeExistsCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypeexistscatalogproducts) | **HEAD** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
[**catalogCategoryPrototypeFindByIdCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypefindbyidcatalogproducts) | **GET** /CatalogCategories/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
[**catalogCategoryPrototypeGetCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypegetcatalogproducts) | **GET** /CatalogCategories/{id}/catalogProducts | Queries catalogProducts of CatalogCategory.
[**catalogCategoryPrototypeLinkCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypelinkcatalogproducts) | **PUT** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
[**catalogCategoryPrototypeUnlinkCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypeunlinkcatalogproducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
[**catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid**](HRPCatalogCategoryApi.md#catalogcategoryprototypeupdateattributespatchcatalogcategoriesid) | **PATCH** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid**](HRPCatalogCategoryApi.md#catalogcategoryprototypeupdateattributesputcatalogcategoriesid) | **PUT** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogCategoryPrototypeUpdateByIdCatalogProducts**](HRPCatalogCategoryApi.md#catalogcategoryprototypeupdatebyidcatalogproducts) | **PUT** /CatalogCategories/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
[**catalogCategoryReplaceById**](HRPCatalogCategoryApi.md#catalogcategoryreplacebyid) | **POST** /CatalogCategories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**catalogCategoryReplaceOrCreate**](HRPCatalogCategoryApi.md#catalogcategoryreplaceorcreate) | **POST** /CatalogCategories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**catalogCategoryUpdateAll**](HRPCatalogCategoryApi.md#catalogcategoryupdateall) | **POST** /CatalogCategories/update | Update instances of the model matched by {{where}} from the data source.
[**catalogCategoryUpsertPatchCatalogCategories**](HRPCatalogCategoryApi.md#catalogcategoryupsertpatchcatalogcategories) | **PATCH** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
[**catalogCategoryUpsertPutCatalogCategories**](HRPCatalogCategoryApi.md#catalogcategoryupsertputcatalogcategories) | **PUT** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
[**catalogCategoryUpsertWithWhere**](HRPCatalogCategoryApi.md#catalogcategoryupsertwithwhere) | **POST** /CatalogCategories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **catalogCategoryCount**
```objc
-(NSNumber*) catalogCategoryCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance catalogCategoryCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryCreate**
```objc
-(NSNumber*) catalogCategoryCreateWithData: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; // Model instance data (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance catalogCategoryCreateWithData:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream**
```objc
-(NSNumber*) catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Create a change stream.
[apiInstance catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream**
```objc
-(NSNumber*) catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Create a change stream.
[apiInstance catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryDeleteById**
```objc
-(NSNumber*) catalogCategoryDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance catalogCategoryDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryExistsGetCatalogCategoriesidExists**
```objc
-(NSNumber*) catalogCategoryExistsGetCatalogCategoriesidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance catalogCategoryExistsGetCatalogCategoriesidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryExistsGetCatalogCategoriesidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryExistsHeadCatalogCategoriesid**
```objc
-(NSNumber*) catalogCategoryExistsHeadCatalogCategoriesidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance catalogCategoryExistsHeadCatalogCategoriesidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryExistsHeadCatalogCategoriesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryFind**
```objc
-(NSNumber*) catalogCategoryFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCatalogCategory>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance catalogCategoryFindWithFilter:filter
          completionHandler: ^(NSArray<HRPCatalogCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPCatalogCategory>***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryFindById**
```objc
-(NSNumber*) catalogCategoryFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance catalogCategoryFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryFindOne**
```objc
-(NSNumber*) catalogCategoryFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance catalogCategoryFindOneWithFilter:filter
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeCountCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeCountCatalogProductsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts catalogProducts of CatalogCategory.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogCategory id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Counts catalogProducts of CatalogCategory.
[apiInstance catalogCategoryPrototypeCountCatalogProductsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeCountCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogCategory id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeCreateCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeCreateCatalogProductsWithId: (NSString*) _id
    data: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Creates a new instance in catalogProducts of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogCategory id
HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; //  (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Creates a new instance in catalogProducts of this model.
[apiInstance catalogCategoryPrototypeCreateCatalogProductsWithId:_id
              data:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeCreateCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogCategory id | 
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)|  | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeDeleteCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeDeleteCatalogProductsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all catalogProducts of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogCategory id

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Deletes all catalogProducts of this model.
[apiInstance catalogCategoryPrototypeDeleteCatalogProductsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeDeleteCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogCategory id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeDestroyByIdCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeDestroyByIdCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for catalogProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // CatalogCategory id

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Delete a related item by id for catalogProducts.
[apiInstance catalogCategoryPrototypeDestroyByIdCatalogProductsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeDestroyByIdCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| CatalogCategory id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeExistsCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeExistsCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSNumber* output, NSError* error)) handler;
```

Check the existence of catalogProducts relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // CatalogCategory id

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Check the existence of catalogProducts relation to an item by id.
[apiInstance catalogCategoryPrototypeExistsCatalogProductsWithFk:fk
              _id:_id
          completionHandler: ^(NSNumber* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeExistsCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| CatalogCategory id | 

### Return type

**NSNumber***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeFindByIdCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeFindByIdCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Find a related item by id for catalogProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // CatalogCategory id

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Find a related item by id for catalogProducts.
[apiInstance catalogCategoryPrototypeFindByIdCatalogProductsWithFk:fk
              _id:_id
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeFindByIdCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| CatalogCategory id | 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeGetCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeGetCatalogProductsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCatalogProduct>* output, NSError* error)) handler;
```

Queries catalogProducts of CatalogCategory.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogCategory id
NSString* filter = @"filter_example"; //  (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Queries catalogProducts of CatalogCategory.
[apiInstance catalogCategoryPrototypeGetCatalogProductsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCatalogProduct>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeGetCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogCategory id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPCatalogProduct>***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeLinkCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeLinkCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPCatalogCategoryProduct*) data
        completionHandler: (void (^)(HRPCatalogCategoryProduct* output, NSError* error)) handler;
```

Add a related item by id for catalogProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // CatalogCategory id
HRPCatalogCategoryProduct* data = [[HRPCatalogCategoryProduct alloc] init]; //  (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Add a related item by id for catalogProducts.
[apiInstance catalogCategoryPrototypeLinkCatalogProductsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPCatalogCategoryProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeLinkCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| CatalogCategory id | 
 **data** | [**HRPCatalogCategoryProduct***](HRPCatalogCategoryProduct*.md)|  | [optional] 

### Return type

[**HRPCatalogCategoryProduct***](HRPCatalogCategoryProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeUnlinkCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeUnlinkCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Remove the catalogProducts relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // CatalogCategory id

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Remove the catalogProducts relation to an item by id.
[apiInstance catalogCategoryPrototypeUnlinkCatalogProductsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeUnlinkCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| CatalogCategory id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid**
```objc
-(NSNumber*) catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesidWithId: (NSString*) _id
    data: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogCategory id
HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; // An object of model property name/value pairs (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesidWithId:_id
              data:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogCategory id | 
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid**
```objc
-(NSNumber*) catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesidWithId: (NSString*) _id
    data: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogCategory id
HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; // An object of model property name/value pairs (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesidWithId:_id
              data:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogCategory id | 
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryPrototypeUpdateByIdCatalogProducts**
```objc
-(NSNumber*) catalogCategoryPrototypeUpdateByIdCatalogProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Update a related item by id for catalogProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogProducts
NSString* _id = @"_id_example"; // CatalogCategory id
HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; //  (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Update a related item by id for catalogProducts.
[apiInstance catalogCategoryPrototypeUpdateByIdCatalogProductsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryPrototypeUpdateByIdCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogProducts | 
 **_id** | **NSString***| CatalogCategory id | 
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)|  | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryReplaceById**
```objc
-(NSNumber*) catalogCategoryReplaceByIdWithId: (NSString*) _id
    data: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; // Model instance data (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance catalogCategoryReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryReplaceOrCreate**
```objc
-(NSNumber*) catalogCategoryReplaceOrCreateWithData: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; // Model instance data (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance catalogCategoryReplaceOrCreateWithData:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryUpdateAll**
```objc
-(NSNumber*) catalogCategoryUpdateAllWithWhere: (NSString*) where
    data: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; // An object of model property name/value pairs (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance catalogCategoryUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryUpsertPatchCatalogCategories**
```objc
-(NSNumber*) catalogCategoryUpsertPatchCatalogCategoriesWithData: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; // Model instance data (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance catalogCategoryUpsertPatchCatalogCategoriesWithData:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryUpsertPatchCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryUpsertPutCatalogCategories**
```objc
-(NSNumber*) catalogCategoryUpsertPutCatalogCategoriesWithData: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; // Model instance data (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance catalogCategoryUpsertPutCatalogCategoriesWithData:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryUpsertPutCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogCategoryUpsertWithWhere**
```objc
-(NSNumber*) catalogCategoryUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; // An object of model property name/value pairs (optional)

HRPCatalogCategoryApi*apiInstance = [[HRPCatalogCategoryApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance catalogCategoryUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogCategoryApi->catalogCategoryUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

