# HRPCatalogProductApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogProductCount**](HRPCatalogProductApi.md#catalogproductcount) | **GET** /CatalogProducts/count | Count instances of the model matched by where from the data source.
[**catalogProductCreate**](HRPCatalogProductApi.md#catalogproductcreate) | **POST** /CatalogProducts | Create a new instance of the model and persist it into the data source.
[**catalogProductCreateChangeStreamGetCatalogProductsChangeStream**](HRPCatalogProductApi.md#catalogproductcreatechangestreamgetcatalogproductschangestream) | **GET** /CatalogProducts/change-stream | Create a change stream.
[**catalogProductCreateChangeStreamPostCatalogProductsChangeStream**](HRPCatalogProductApi.md#catalogproductcreatechangestreampostcatalogproductschangestream) | **POST** /CatalogProducts/change-stream | Create a change stream.
[**catalogProductDeleteById**](HRPCatalogProductApi.md#catalogproductdeletebyid) | **DELETE** /CatalogProducts/{id} | Delete a model instance by {{id}} from the data source.
[**catalogProductExistsGetCatalogProductsidExists**](HRPCatalogProductApi.md#catalogproductexistsgetcatalogproductsidexists) | **GET** /CatalogProducts/{id}/exists | Check whether a model instance exists in the data source.
[**catalogProductExistsHeadCatalogProductsid**](HRPCatalogProductApi.md#catalogproductexistsheadcatalogproductsid) | **HEAD** /CatalogProducts/{id} | Check whether a model instance exists in the data source.
[**catalogProductFind**](HRPCatalogProductApi.md#catalogproductfind) | **GET** /CatalogProducts | Find all instances of the model matched by filter from the data source.
[**catalogProductFindById**](HRPCatalogProductApi.md#catalogproductfindbyid) | **GET** /CatalogProducts/{id} | Find a model instance by {{id}} from the data source.
[**catalogProductFindOne**](HRPCatalogProductApi.md#catalogproductfindone) | **GET** /CatalogProducts/findOne | Find first instance of the model matched by filter from the data source.
[**catalogProductPrototypeCountCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypecountcatalogcategories) | **GET** /CatalogProducts/{id}/catalogCategories/count | Counts catalogCategories of CatalogProduct.
[**catalogProductPrototypeCountUdropshipVendorProducts**](HRPCatalogProductApi.md#catalogproductprototypecountudropshipvendorproducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of CatalogProduct.
[**catalogProductPrototypeCountUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypecountudropshipvendors) | **GET** /CatalogProducts/{id}/udropshipVendors/count | Counts udropshipVendors of CatalogProduct.
[**catalogProductPrototypeCreateAwCollpurDeal**](HRPCatalogProductApi.md#catalogproductprototypecreateawcollpurdeal) | **POST** /CatalogProducts/{id}/awCollpurDeal | Creates a new instance in awCollpurDeal of this model.
[**catalogProductPrototypeCreateCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypecreatecatalogcategories) | **POST** /CatalogProducts/{id}/catalogCategories | Creates a new instance in catalogCategories of this model.
[**catalogProductPrototypeCreateInventory**](HRPCatalogProductApi.md#catalogproductprototypecreateinventory) | **POST** /CatalogProducts/{id}/inventory | Creates a new instance in inventory of this model.
[**catalogProductPrototypeCreateProductEventCompetition**](HRPCatalogProductApi.md#catalogproductprototypecreateproducteventcompetition) | **POST** /CatalogProducts/{id}/productEventCompetition | Creates a new instance in productEventCompetition of this model.
[**catalogProductPrototypeCreateUdropshipVendorProducts**](HRPCatalogProductApi.md#catalogproductprototypecreateudropshipvendorproducts) | **POST** /CatalogProducts/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
[**catalogProductPrototypeCreateUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypecreateudropshipvendors) | **POST** /CatalogProducts/{id}/udropshipVendors | Creates a new instance in udropshipVendors of this model.
[**catalogProductPrototypeDeleteCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypedeletecatalogcategories) | **DELETE** /CatalogProducts/{id}/catalogCategories | Deletes all catalogCategories of this model.
[**catalogProductPrototypeDeleteUdropshipVendorProducts**](HRPCatalogProductApi.md#catalogproductprototypedeleteudropshipvendorproducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
[**catalogProductPrototypeDeleteUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypedeleteudropshipvendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors | Deletes all udropshipVendors of this model.
[**catalogProductPrototypeDestroyAwCollpurDeal**](HRPCatalogProductApi.md#catalogproductprototypedestroyawcollpurdeal) | **DELETE** /CatalogProducts/{id}/awCollpurDeal | Deletes awCollpurDeal of this model.
[**catalogProductPrototypeDestroyByIdCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypedestroybyidcatalogcategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/{fk} | Delete a related item by id for catalogCategories.
[**catalogProductPrototypeDestroyByIdUdropshipVendorProducts**](HRPCatalogProductApi.md#catalogproductprototypedestroybyidudropshipvendorproducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeDestroyByIdUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypedestroybyidudropshipvendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/{fk} | Delete a related item by id for udropshipVendors.
[**catalogProductPrototypeDestroyInventory**](HRPCatalogProductApi.md#catalogproductprototypedestroyinventory) | **DELETE** /CatalogProducts/{id}/inventory | Deletes inventory of this model.
[**catalogProductPrototypeDestroyProductEventCompetition**](HRPCatalogProductApi.md#catalogproductprototypedestroyproducteventcompetition) | **DELETE** /CatalogProducts/{id}/productEventCompetition | Deletes productEventCompetition of this model.
[**catalogProductPrototypeExistsCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypeexistscatalogcategories) | **HEAD** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Check the existence of catalogCategories relation to an item by id.
[**catalogProductPrototypeExistsUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypeexistsudropshipvendors) | **HEAD** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Check the existence of udropshipVendors relation to an item by id.
[**catalogProductPrototypeFindByIdCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypefindbyidcatalogcategories) | **GET** /CatalogProducts/{id}/catalogCategories/{fk} | Find a related item by id for catalogCategories.
[**catalogProductPrototypeFindByIdUdropshipVendorProducts**](HRPCatalogProductApi.md#catalogproductprototypefindbyidudropshipvendorproducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeFindByIdUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypefindbyidudropshipvendors) | **GET** /CatalogProducts/{id}/udropshipVendors/{fk} | Find a related item by id for udropshipVendors.
[**catalogProductPrototypeGetAwCollpurDeal**](HRPCatalogProductApi.md#catalogproductprototypegetawcollpurdeal) | **GET** /CatalogProducts/{id}/awCollpurDeal | Fetches hasOne relation awCollpurDeal.
[**catalogProductPrototypeGetCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypegetcatalogcategories) | **GET** /CatalogProducts/{id}/catalogCategories | Queries catalogCategories of CatalogProduct.
[**catalogProductPrototypeGetCheckoutAgreement**](HRPCatalogProductApi.md#catalogproductprototypegetcheckoutagreement) | **GET** /CatalogProducts/{id}/checkoutAgreement | Fetches belongsTo relation checkoutAgreement.
[**catalogProductPrototypeGetCreator**](HRPCatalogProductApi.md#catalogproductprototypegetcreator) | **GET** /CatalogProducts/{id}/creator | Fetches belongsTo relation creator.
[**catalogProductPrototypeGetInventory**](HRPCatalogProductApi.md#catalogproductprototypegetinventory) | **GET** /CatalogProducts/{id}/inventory | Fetches hasOne relation inventory.
[**catalogProductPrototypeGetProductEventCompetition**](HRPCatalogProductApi.md#catalogproductprototypegetproducteventcompetition) | **GET** /CatalogProducts/{id}/productEventCompetition | Fetches hasOne relation productEventCompetition.
[**catalogProductPrototypeGetUdropshipVendorProducts**](HRPCatalogProductApi.md#catalogproductprototypegetudropshipvendorproducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of CatalogProduct.
[**catalogProductPrototypeGetUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypegetudropshipvendors) | **GET** /CatalogProducts/{id}/udropshipVendors | Queries udropshipVendors of CatalogProduct.
[**catalogProductPrototypeLinkCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypelinkcatalogcategories) | **PUT** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Add a related item by id for catalogCategories.
[**catalogProductPrototypeLinkUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypelinkudropshipvendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Add a related item by id for udropshipVendors.
[**catalogProductPrototypeUnlinkCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypeunlinkcatalogcategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Remove the catalogCategories relation to an item by id.
[**catalogProductPrototypeUnlinkUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypeunlinkudropshipvendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Remove the udropshipVendors relation to an item by id.
[**catalogProductPrototypeUpdateAttributesPatchCatalogProductsid**](HRPCatalogProductApi.md#catalogproductprototypeupdateattributespatchcatalogproductsid) | **PATCH** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogProductPrototypeUpdateAttributesPutCatalogProductsid**](HRPCatalogProductApi.md#catalogproductprototypeupdateattributesputcatalogproductsid) | **PUT** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogProductPrototypeUpdateAwCollpurDeal**](HRPCatalogProductApi.md#catalogproductprototypeupdateawcollpurdeal) | **PUT** /CatalogProducts/{id}/awCollpurDeal | Update awCollpurDeal of this model.
[**catalogProductPrototypeUpdateByIdCatalogCategories**](HRPCatalogProductApi.md#catalogproductprototypeupdatebyidcatalogcategories) | **PUT** /CatalogProducts/{id}/catalogCategories/{fk} | Update a related item by id for catalogCategories.
[**catalogProductPrototypeUpdateByIdUdropshipVendorProducts**](HRPCatalogProductApi.md#catalogproductprototypeupdatebyidudropshipvendorproducts) | **PUT** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeUpdateByIdUdropshipVendors**](HRPCatalogProductApi.md#catalogproductprototypeupdatebyidudropshipvendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/{fk} | Update a related item by id for udropshipVendors.
[**catalogProductPrototypeUpdateInventory**](HRPCatalogProductApi.md#catalogproductprototypeupdateinventory) | **PUT** /CatalogProducts/{id}/inventory | Update inventory of this model.
[**catalogProductPrototypeUpdateProductEventCompetition**](HRPCatalogProductApi.md#catalogproductprototypeupdateproducteventcompetition) | **PUT** /CatalogProducts/{id}/productEventCompetition | Update productEventCompetition of this model.
[**catalogProductReplaceById**](HRPCatalogProductApi.md#catalogproductreplacebyid) | **POST** /CatalogProducts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**catalogProductReplaceOrCreate**](HRPCatalogProductApi.md#catalogproductreplaceorcreate) | **POST** /CatalogProducts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**catalogProductUpdateAll**](HRPCatalogProductApi.md#catalogproductupdateall) | **POST** /CatalogProducts/update | Update instances of the model matched by {{where}} from the data source.
[**catalogProductUpsertPatchCatalogProducts**](HRPCatalogProductApi.md#catalogproductupsertpatchcatalogproducts) | **PATCH** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
[**catalogProductUpsertPutCatalogProducts**](HRPCatalogProductApi.md#catalogproductupsertputcatalogproducts) | **PUT** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
[**catalogProductUpsertWithWhere**](HRPCatalogProductApi.md#catalogproductupsertwithwhere) | **POST** /CatalogProducts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **catalogProductCount**
```objc
-(NSNumber*) catalogProductCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance catalogProductCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductCreate**
```objc
-(NSNumber*) catalogProductCreateWithData: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; // Model instance data (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance catalogProductCreateWithData:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductCreateChangeStreamGetCatalogProductsChangeStream**
```objc
-(NSNumber*) catalogProductCreateChangeStreamGetCatalogProductsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Create a change stream.
[apiInstance catalogProductCreateChangeStreamGetCatalogProductsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductCreateChangeStreamGetCatalogProductsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductCreateChangeStreamPostCatalogProductsChangeStream**
```objc
-(NSNumber*) catalogProductCreateChangeStreamPostCatalogProductsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Create a change stream.
[apiInstance catalogProductCreateChangeStreamPostCatalogProductsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductCreateChangeStreamPostCatalogProductsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductDeleteById**
```objc
-(NSNumber*) catalogProductDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance catalogProductDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductExistsGetCatalogProductsidExists**
```objc
-(NSNumber*) catalogProductExistsGetCatalogProductsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance catalogProductExistsGetCatalogProductsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductExistsGetCatalogProductsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductExistsHeadCatalogProductsid**
```objc
-(NSNumber*) catalogProductExistsHeadCatalogProductsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance catalogProductExistsHeadCatalogProductsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductExistsHeadCatalogProductsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductFind**
```objc
-(NSNumber*) catalogProductFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCatalogProduct>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance catalogProductFindWithFilter:filter
          completionHandler: ^(NSArray<HRPCatalogProduct>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPCatalogProduct>***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductFindById**
```objc
-(NSNumber*) catalogProductFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance catalogProductFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductFindOne**
```objc
-(NSNumber*) catalogProductFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance catalogProductFindOneWithFilter:filter
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeCountCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeCountCatalogCategoriesWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts catalogCategories of CatalogProduct.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Counts catalogCategories of CatalogProduct.
[apiInstance catalogProductPrototypeCountCatalogCategoriesWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeCountCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeCountUdropshipVendorProducts**
```objc
-(NSNumber*) catalogProductPrototypeCountUdropshipVendorProductsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts udropshipVendorProducts of CatalogProduct.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Counts udropshipVendorProducts of CatalogProduct.
[apiInstance catalogProductPrototypeCountUdropshipVendorProductsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeCountUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeCountUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeCountUdropshipVendorsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts udropshipVendors of CatalogProduct.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Counts udropshipVendors of CatalogProduct.
[apiInstance catalogProductPrototypeCountUdropshipVendorsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeCountUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeCreateAwCollpurDeal**
```objc
-(NSNumber*) catalogProductPrototypeCreateAwCollpurDealWithId: (NSString*) _id
    data: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Creates a new instance in awCollpurDeal of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Creates a new instance in awCollpurDeal of this model.
[apiInstance catalogProductPrototypeCreateAwCollpurDealWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeCreateAwCollpurDeal: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)|  | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeCreateCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeCreateCatalogCategoriesWithId: (NSString*) _id
    data: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Creates a new instance in catalogCategories of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Creates a new instance in catalogCategories of this model.
[apiInstance catalogProductPrototypeCreateCatalogCategoriesWithId:_id
              data:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeCreateCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)|  | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeCreateInventory**
```objc
-(NSNumber*) catalogProductPrototypeCreateInventoryWithId: (NSString*) _id
    data: (HRPCatalogInventoryStockItem*) data
        completionHandler: (void (^)(HRPCatalogInventoryStockItem* output, NSError* error)) handler;
```

Creates a new instance in inventory of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPCatalogInventoryStockItem* data = [[HRPCatalogInventoryStockItem alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Creates a new instance in inventory of this model.
[apiInstance catalogProductPrototypeCreateInventoryWithId:_id
              data:data
          completionHandler: ^(HRPCatalogInventoryStockItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeCreateInventory: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPCatalogInventoryStockItem***](HRPCatalogInventoryStockItem*.md)|  | [optional] 

### Return type

[**HRPCatalogInventoryStockItem***](HRPCatalogInventoryStockItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeCreateProductEventCompetition**
```objc
-(NSNumber*) catalogProductPrototypeCreateProductEventCompetitionWithId: (NSString*) _id
    data: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Creates a new instance in productEventCompetition of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Creates a new instance in productEventCompetition of this model.
[apiInstance catalogProductPrototypeCreateProductEventCompetitionWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeCreateProductEventCompetition: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeCreateUdropshipVendorProducts**
```objc
-(NSNumber*) catalogProductPrototypeCreateUdropshipVendorProductsWithId: (NSString*) _id
    data: (HRPUdropshipVendorProduct*) data
        completionHandler: (void (^)(HRPUdropshipVendorProduct* output, NSError* error)) handler;
```

Creates a new instance in udropshipVendorProducts of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPUdropshipVendorProduct* data = [[HRPUdropshipVendorProduct alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Creates a new instance in udropshipVendorProducts of this model.
[apiInstance catalogProductPrototypeCreateUdropshipVendorProductsWithId:_id
              data:data
          completionHandler: ^(HRPUdropshipVendorProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeCreateUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeCreateUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeCreateUdropshipVendorsWithId: (NSString*) _id
    data: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Creates a new instance in udropshipVendors of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Creates a new instance in udropshipVendors of this model.
[apiInstance catalogProductPrototypeCreateUdropshipVendorsWithId:_id
              data:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeCreateUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeDeleteCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeDeleteCatalogCategoriesWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all catalogCategories of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Deletes all catalogCategories of this model.
[apiInstance catalogProductPrototypeDeleteCatalogCategoriesWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeDeleteCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeDeleteUdropshipVendorProducts**
```objc
-(NSNumber*) catalogProductPrototypeDeleteUdropshipVendorProductsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all udropshipVendorProducts of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Deletes all udropshipVendorProducts of this model.
[apiInstance catalogProductPrototypeDeleteUdropshipVendorProductsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeDeleteUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeDeleteUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeDeleteUdropshipVendorsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all udropshipVendors of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Deletes all udropshipVendors of this model.
[apiInstance catalogProductPrototypeDeleteUdropshipVendorsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeDeleteUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeDestroyAwCollpurDeal**
```objc
-(NSNumber*) catalogProductPrototypeDestroyAwCollpurDealWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes awCollpurDeal of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Deletes awCollpurDeal of this model.
[apiInstance catalogProductPrototypeDestroyAwCollpurDealWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeDestroyAwCollpurDeal: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeDestroyByIdCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeDestroyByIdCatalogCategoriesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for catalogCategories.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogCategories
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Delete a related item by id for catalogCategories.
[apiInstance catalogProductPrototypeDestroyByIdCatalogCategoriesWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeDestroyByIdCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogCategories | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeDestroyByIdUdropshipVendorProducts**
```objc
-(NSNumber*) catalogProductPrototypeDestroyByIdUdropshipVendorProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for udropshipVendorProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendorProducts
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Delete a related item by id for udropshipVendorProducts.
[apiInstance catalogProductPrototypeDestroyByIdUdropshipVendorProductsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeDestroyByIdUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendorProducts | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeDestroyByIdUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeDestroyByIdUdropshipVendorsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for udropshipVendors.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendors
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Delete a related item by id for udropshipVendors.
[apiInstance catalogProductPrototypeDestroyByIdUdropshipVendorsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeDestroyByIdUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendors | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeDestroyInventory**
```objc
-(NSNumber*) catalogProductPrototypeDestroyInventoryWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes inventory of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Deletes inventory of this model.
[apiInstance catalogProductPrototypeDestroyInventoryWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeDestroyInventory: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeDestroyProductEventCompetition**
```objc
-(NSNumber*) catalogProductPrototypeDestroyProductEventCompetitionWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes productEventCompetition of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Deletes productEventCompetition of this model.
[apiInstance catalogProductPrototypeDestroyProductEventCompetitionWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeDestroyProductEventCompetition: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeExistsCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeExistsCatalogCategoriesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSNumber* output, NSError* error)) handler;
```

Check the existence of catalogCategories relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogCategories
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Check the existence of catalogCategories relation to an item by id.
[apiInstance catalogProductPrototypeExistsCatalogCategoriesWithFk:fk
              _id:_id
          completionHandler: ^(NSNumber* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeExistsCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogCategories | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

**NSNumber***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeExistsUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeExistsUdropshipVendorsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSNumber* output, NSError* error)) handler;
```

Check the existence of udropshipVendors relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendors
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Check the existence of udropshipVendors relation to an item by id.
[apiInstance catalogProductPrototypeExistsUdropshipVendorsWithFk:fk
              _id:_id
          completionHandler: ^(NSNumber* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeExistsUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendors | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

**NSNumber***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeFindByIdCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeFindByIdCatalogCategoriesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Find a related item by id for catalogCategories.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogCategories
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Find a related item by id for catalogCategories.
[apiInstance catalogProductPrototypeFindByIdCatalogCategoriesWithFk:fk
              _id:_id
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeFindByIdCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogCategories | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeFindByIdUdropshipVendorProducts**
```objc
-(NSNumber*) catalogProductPrototypeFindByIdUdropshipVendorProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPUdropshipVendorProduct* output, NSError* error)) handler;
```

Find a related item by id for udropshipVendorProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendorProducts
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Find a related item by id for udropshipVendorProducts.
[apiInstance catalogProductPrototypeFindByIdUdropshipVendorProductsWithFk:fk
              _id:_id
          completionHandler: ^(HRPUdropshipVendorProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeFindByIdUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendorProducts | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

[**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeFindByIdUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeFindByIdUdropshipVendorsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Find a related item by id for udropshipVendors.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendors
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Find a related item by id for udropshipVendors.
[apiInstance catalogProductPrototypeFindByIdUdropshipVendorsWithFk:fk
              _id:_id
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeFindByIdUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendors | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeGetAwCollpurDeal**
```objc
-(NSNumber*) catalogProductPrototypeGetAwCollpurDealWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Fetches hasOne relation awCollpurDeal.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSNumber* refresh = @true; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Fetches hasOne relation awCollpurDeal.
[apiInstance catalogProductPrototypeGetAwCollpurDealWithId:_id
              refresh:refresh
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeGetAwCollpurDeal: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeGetCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeGetCatalogCategoriesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCatalogCategory>* output, NSError* error)) handler;
```

Queries catalogCategories of CatalogProduct.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSString* filter = @"filter_example"; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Queries catalogCategories of CatalogProduct.
[apiInstance catalogProductPrototypeGetCatalogCategoriesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCatalogCategory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeGetCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPCatalogCategory>***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeGetCheckoutAgreement**
```objc
-(NSNumber*) catalogProductPrototypeGetCheckoutAgreementWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPCheckoutAgreement* output, NSError* error)) handler;
```

Fetches belongsTo relation checkoutAgreement.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSNumber* refresh = @true; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Fetches belongsTo relation checkoutAgreement.
[apiInstance catalogProductPrototypeGetCheckoutAgreementWithId:_id
              refresh:refresh
          completionHandler: ^(HRPCheckoutAgreement* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeGetCheckoutAgreement: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPCheckoutAgreement***](HRPCheckoutAgreement.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeGetCreator**
```objc
-(NSNumber*) catalogProductPrototypeGetCreatorWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Fetches belongsTo relation creator.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSNumber* refresh = @true; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Fetches belongsTo relation creator.
[apiInstance catalogProductPrototypeGetCreatorWithId:_id
              refresh:refresh
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeGetCreator: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeGetInventory**
```objc
-(NSNumber*) catalogProductPrototypeGetInventoryWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPCatalogInventoryStockItem* output, NSError* error)) handler;
```

Fetches hasOne relation inventory.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSNumber* refresh = @true; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Fetches hasOne relation inventory.
[apiInstance catalogProductPrototypeGetInventoryWithId:_id
              refresh:refresh
          completionHandler: ^(HRPCatalogInventoryStockItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeGetInventory: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPCatalogInventoryStockItem***](HRPCatalogInventoryStockItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeGetProductEventCompetition**
```objc
-(NSNumber*) catalogProductPrototypeGetProductEventCompetitionWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Fetches hasOne relation productEventCompetition.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSNumber* refresh = @true; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Fetches hasOne relation productEventCompetition.
[apiInstance catalogProductPrototypeGetProductEventCompetitionWithId:_id
              refresh:refresh
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeGetProductEventCompetition: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeGetUdropshipVendorProducts**
```objc
-(NSNumber*) catalogProductPrototypeGetUdropshipVendorProductsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPUdropshipVendorProduct>* output, NSError* error)) handler;
```

Queries udropshipVendorProducts of CatalogProduct.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSString* filter = @"filter_example"; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Queries udropshipVendorProducts of CatalogProduct.
[apiInstance catalogProductPrototypeGetUdropshipVendorProductsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPUdropshipVendorProduct>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeGetUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPUdropshipVendorProduct>***](HRPUdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeGetUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeGetUdropshipVendorsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPUdropshipVendor>* output, NSError* error)) handler;
```

Queries udropshipVendors of CatalogProduct.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
NSString* filter = @"filter_example"; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Queries udropshipVendors of CatalogProduct.
[apiInstance catalogProductPrototypeGetUdropshipVendorsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPUdropshipVendor>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeGetUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPUdropshipVendor>***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeLinkCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeLinkCatalogCategoriesWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPCatalogCategoryProduct*) data
        completionHandler: (void (^)(HRPCatalogCategoryProduct* output, NSError* error)) handler;
```

Add a related item by id for catalogCategories.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogCategories
NSString* _id = @"_id_example"; // CatalogProduct id
HRPCatalogCategoryProduct* data = [[HRPCatalogCategoryProduct alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Add a related item by id for catalogCategories.
[apiInstance catalogProductPrototypeLinkCatalogCategoriesWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPCatalogCategoryProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeLinkCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogCategories | 
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPCatalogCategoryProduct***](HRPCatalogCategoryProduct*.md)|  | [optional] 

### Return type

[**HRPCatalogCategoryProduct***](HRPCatalogCategoryProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeLinkUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeLinkUdropshipVendorsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPUdropshipVendorProductAssoc*) data
        completionHandler: (void (^)(HRPUdropshipVendorProductAssoc* output, NSError* error)) handler;
```

Add a related item by id for udropshipVendors.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendors
NSString* _id = @"_id_example"; // CatalogProduct id
HRPUdropshipVendorProductAssoc* data = [[HRPUdropshipVendorProductAssoc alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Add a related item by id for udropshipVendors.
[apiInstance catalogProductPrototypeLinkUdropshipVendorsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPUdropshipVendorProductAssoc* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeLinkUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendors | 
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPUdropshipVendorProductAssoc***](HRPUdropshipVendorProductAssoc*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendorProductAssoc***](HRPUdropshipVendorProductAssoc.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUnlinkCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeUnlinkCatalogCategoriesWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Remove the catalogCategories relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogCategories
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Remove the catalogCategories relation to an item by id.
[apiInstance catalogProductPrototypeUnlinkCatalogCategoriesWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUnlinkCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogCategories | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUnlinkUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeUnlinkUdropshipVendorsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Remove the udropshipVendors relation to an item by id.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendors
NSString* _id = @"_id_example"; // CatalogProduct id

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Remove the udropshipVendors relation to an item by id.
[apiInstance catalogProductPrototypeUnlinkUdropshipVendorsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUnlinkUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendors | 
 **_id** | **NSString***| CatalogProduct id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUpdateAttributesPatchCatalogProductsid**
```objc
-(NSNumber*) catalogProductPrototypeUpdateAttributesPatchCatalogProductsidWithId: (NSString*) _id
    data: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; // An object of model property name/value pairs (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance catalogProductPrototypeUpdateAttributesPatchCatalogProductsidWithId:_id
              data:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUpdateAttributesPatchCatalogProductsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUpdateAttributesPutCatalogProductsid**
```objc
-(NSNumber*) catalogProductPrototypeUpdateAttributesPutCatalogProductsidWithId: (NSString*) _id
    data: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; // An object of model property name/value pairs (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance catalogProductPrototypeUpdateAttributesPutCatalogProductsidWithId:_id
              data:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUpdateAttributesPutCatalogProductsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUpdateAwCollpurDeal**
```objc
-(NSNumber*) catalogProductPrototypeUpdateAwCollpurDealWithId: (NSString*) _id
    data: (HRPAwCollpurDeal*) data
        completionHandler: (void (^)(HRPAwCollpurDeal* output, NSError* error)) handler;
```

Update awCollpurDeal of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPAwCollpurDeal* data = [[HRPAwCollpurDeal alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Update awCollpurDeal of this model.
[apiInstance catalogProductPrototypeUpdateAwCollpurDealWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurDeal* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUpdateAwCollpurDeal: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPAwCollpurDeal***](HRPAwCollpurDeal*.md)|  | [optional] 

### Return type

[**HRPAwCollpurDeal***](HRPAwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUpdateByIdCatalogCategories**
```objc
-(NSNumber*) catalogProductPrototypeUpdateByIdCatalogCategoriesWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPCatalogCategory*) data
        completionHandler: (void (^)(HRPCatalogCategory* output, NSError* error)) handler;
```

Update a related item by id for catalogCategories.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for catalogCategories
NSString* _id = @"_id_example"; // CatalogProduct id
HRPCatalogCategory* data = [[HRPCatalogCategory alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Update a related item by id for catalogCategories.
[apiInstance catalogProductPrototypeUpdateByIdCatalogCategoriesWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPCatalogCategory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUpdateByIdCatalogCategories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for catalogCategories | 
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPCatalogCategory***](HRPCatalogCategory*.md)|  | [optional] 

### Return type

[**HRPCatalogCategory***](HRPCatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUpdateByIdUdropshipVendorProducts**
```objc
-(NSNumber*) catalogProductPrototypeUpdateByIdUdropshipVendorProductsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPUdropshipVendorProduct*) data
        completionHandler: (void (^)(HRPUdropshipVendorProduct* output, NSError* error)) handler;
```

Update a related item by id for udropshipVendorProducts.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendorProducts
NSString* _id = @"_id_example"; // CatalogProduct id
HRPUdropshipVendorProduct* data = [[HRPUdropshipVendorProduct alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Update a related item by id for udropshipVendorProducts.
[apiInstance catalogProductPrototypeUpdateByIdUdropshipVendorProductsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPUdropshipVendorProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUpdateByIdUdropshipVendorProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendorProducts | 
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendorProduct***](HRPUdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUpdateByIdUdropshipVendors**
```objc
-(NSNumber*) catalogProductPrototypeUpdateByIdUdropshipVendorsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPUdropshipVendor*) data
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Update a related item by id for udropshipVendors.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for udropshipVendors
NSString* _id = @"_id_example"; // CatalogProduct id
HRPUdropshipVendor* data = [[HRPUdropshipVendor alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Update a related item by id for udropshipVendors.
[apiInstance catalogProductPrototypeUpdateByIdUdropshipVendorsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUpdateByIdUdropshipVendors: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for udropshipVendors | 
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPUdropshipVendor***](HRPUdropshipVendor*.md)|  | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUpdateInventory**
```objc
-(NSNumber*) catalogProductPrototypeUpdateInventoryWithId: (NSString*) _id
    data: (HRPCatalogInventoryStockItem*) data
        completionHandler: (void (^)(HRPCatalogInventoryStockItem* output, NSError* error)) handler;
```

Update inventory of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPCatalogInventoryStockItem* data = [[HRPCatalogInventoryStockItem alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Update inventory of this model.
[apiInstance catalogProductPrototypeUpdateInventoryWithId:_id
              data:data
          completionHandler: ^(HRPCatalogInventoryStockItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUpdateInventory: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPCatalogInventoryStockItem***](HRPCatalogInventoryStockItem*.md)|  | [optional] 

### Return type

[**HRPCatalogInventoryStockItem***](HRPCatalogInventoryStockItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductPrototypeUpdateProductEventCompetition**
```objc
-(NSNumber*) catalogProductPrototypeUpdateProductEventCompetitionWithId: (NSString*) _id
    data: (HRPAwEventbookingEvent*) data
        completionHandler: (void (^)(HRPAwEventbookingEvent* output, NSError* error)) handler;
```

Update productEventCompetition of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // CatalogProduct id
HRPAwEventbookingEvent* data = [[HRPAwEventbookingEvent alloc] init]; //  (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Update productEventCompetition of this model.
[apiInstance catalogProductPrototypeUpdateProductEventCompetitionWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingEvent* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductPrototypeUpdateProductEventCompetition: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| CatalogProduct id | 
 **data** | [**HRPAwEventbookingEvent***](HRPAwEventbookingEvent*.md)|  | [optional] 

### Return type

[**HRPAwEventbookingEvent***](HRPAwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductReplaceById**
```objc
-(NSNumber*) catalogProductReplaceByIdWithId: (NSString*) _id
    data: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; // Model instance data (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance catalogProductReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductReplaceOrCreate**
```objc
-(NSNumber*) catalogProductReplaceOrCreateWithData: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; // Model instance data (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance catalogProductReplaceOrCreateWithData:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductUpdateAll**
```objc
-(NSNumber*) catalogProductUpdateAllWithWhere: (NSString*) where
    data: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; // An object of model property name/value pairs (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance catalogProductUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductUpsertPatchCatalogProducts**
```objc
-(NSNumber*) catalogProductUpsertPatchCatalogProductsWithData: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; // Model instance data (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance catalogProductUpsertPatchCatalogProductsWithData:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductUpsertPatchCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductUpsertPutCatalogProducts**
```objc
-(NSNumber*) catalogProductUpsertPutCatalogProductsWithData: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; // Model instance data (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance catalogProductUpsertPutCatalogProductsWithData:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductUpsertPutCatalogProducts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)| Model instance data | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **catalogProductUpsertWithWhere**
```objc
-(NSNumber*) catalogProductUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPCatalogProduct*) data
        completionHandler: (void (^)(HRPCatalogProduct* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCatalogProduct* data = [[HRPCatalogProduct alloc] init]; // An object of model property name/value pairs (optional)

HRPCatalogProductApi*apiInstance = [[HRPCatalogProductApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance catalogProductUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPCatalogProduct* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCatalogProductApi->catalogProductUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCatalogProduct***](HRPCatalogProduct*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCatalogProduct***](HRPCatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

