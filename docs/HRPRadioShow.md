# HRPRadioShow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | 
**_description** | **NSString*** |  | [optional] 
**content** | **NSString*** |  | [optional] 
**contentType** | **NSString*** |  | [optional] [default to @"image"]
**contact** | [**HRPContact***](HRPContact.md) | Contacts for this show | [optional] 
**starts** | **NSDate*** | When the Show starts of being public | [optional] 
**ends** | **NSDate*** | When the Show ceases of being public | [optional] 
**type** | **NSString*** |  | [optional] 
**radioShowTimeCurrent** | [**HRPRadioShowTime***](HRPRadioShowTime.md) |  | [optional] 
**imgShow** | **NSString*** |  | [optional] 
**sponsorTrack** | **NSString*** | Url of the sponsor MP3 to be played | [optional] 
**_id** | **NSNumber*** |  | [optional] 
**radioStreamId** | **NSNumber*** |  | [optional] 
**playlistItemId** | **NSNumber*** |  | [optional] 
**radioPresenters** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**radioStream** | **NSObject*** |  | [optional] 
**radioShowTimes** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**playlistItem** | **NSObject*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


