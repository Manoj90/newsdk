# HRPStripeInvoiceItemApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeInvoiceItemCount**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemcount) | **GET** /StripeInvoiceItems/count | Count instances of the model matched by where from the data source.
[**stripeInvoiceItemCreate**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemcreate) | **POST** /StripeInvoiceItems | Create a new instance of the model and persist it into the data source.
[**stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemcreatechangestreamgetstripeinvoiceitemschangestream) | **GET** /StripeInvoiceItems/change-stream | Create a change stream.
[**stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemcreatechangestreampoststripeinvoiceitemschangestream) | **POST** /StripeInvoiceItems/change-stream | Create a change stream.
[**stripeInvoiceItemDeleteById**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemdeletebyid) | **DELETE** /StripeInvoiceItems/{id} | Delete a model instance by {{id}} from the data source.
[**stripeInvoiceItemExistsGetStripeInvoiceItemsidExists**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemexistsgetstripeinvoiceitemsidexists) | **GET** /StripeInvoiceItems/{id}/exists | Check whether a model instance exists in the data source.
[**stripeInvoiceItemExistsHeadStripeInvoiceItemsid**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemexistsheadstripeinvoiceitemsid) | **HEAD** /StripeInvoiceItems/{id} | Check whether a model instance exists in the data source.
[**stripeInvoiceItemFind**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemfind) | **GET** /StripeInvoiceItems | Find all instances of the model matched by filter from the data source.
[**stripeInvoiceItemFindById**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemfindbyid) | **GET** /StripeInvoiceItems/{id} | Find a model instance by {{id}} from the data source.
[**stripeInvoiceItemFindOne**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemfindone) | **GET** /StripeInvoiceItems/findOne | Find first instance of the model matched by filter from the data source.
[**stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemprototypeupdateattributespatchstripeinvoiceitemsid) | **PATCH** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemprototypeupdateattributesputstripeinvoiceitemsid) | **PUT** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemReplaceById**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemreplacebyid) | **POST** /StripeInvoiceItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemReplaceOrCreate**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemreplaceorcreate) | **POST** /StripeInvoiceItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpdateAll**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemupdateall) | **POST** /StripeInvoiceItems/update | Update instances of the model matched by {{where}} from the data source.
[**stripeInvoiceItemUpsertPatchStripeInvoiceItems**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemupsertpatchstripeinvoiceitems) | **PATCH** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpsertPutStripeInvoiceItems**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemupsertputstripeinvoiceitems) | **PUT** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpsertWithWhere**](HRPStripeInvoiceItemApi.md#stripeinvoiceitemupsertwithwhere) | **POST** /StripeInvoiceItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeInvoiceItemCount**
```objc
-(NSNumber*) stripeInvoiceItemCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance stripeInvoiceItemCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemCreate**
```objc
-(NSNumber*) stripeInvoiceItemCreateWithData: (HRPStripeInvoiceItem*) data
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeInvoiceItem* data = [[HRPStripeInvoiceItem alloc] init]; // Model instance data (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance stripeInvoiceItemCreateWithData:data
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeInvoiceItem***](HRPStripeInvoiceItem*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream**
```objc
-(NSNumber*) stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Create a change stream.
[apiInstance stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream**
```objc
-(NSNumber*) stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Create a change stream.
[apiInstance stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemDeleteById**
```objc
-(NSNumber*) stripeInvoiceItemDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance stripeInvoiceItemDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemExistsGetStripeInvoiceItemsidExists**
```objc
-(NSNumber*) stripeInvoiceItemExistsGetStripeInvoiceItemsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeInvoiceItemExistsGetStripeInvoiceItemsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemExistsGetStripeInvoiceItemsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemExistsHeadStripeInvoiceItemsid**
```objc
-(NSNumber*) stripeInvoiceItemExistsHeadStripeInvoiceItemsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeInvoiceItemExistsHeadStripeInvoiceItemsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemExistsHeadStripeInvoiceItemsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemFind**
```objc
-(NSNumber*) stripeInvoiceItemFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPStripeInvoiceItem>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance stripeInvoiceItemFindWithFilter:filter
          completionHandler: ^(NSArray<HRPStripeInvoiceItem>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPStripeInvoiceItem>***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemFindById**
```objc
-(NSNumber*) stripeInvoiceItemFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance stripeInvoiceItemFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemFindOne**
```objc
-(NSNumber*) stripeInvoiceItemFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance stripeInvoiceItemFindOneWithFilter:filter
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid**
```objc
-(NSNumber*) stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsidWithId: (NSString*) _id
    data: (HRPStripeInvoiceItem*) data
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeInvoiceItem id
HRPStripeInvoiceItem* data = [[HRPStripeInvoiceItem alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsidWithId:_id
              data:data
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeInvoiceItem id | 
 **data** | [**HRPStripeInvoiceItem***](HRPStripeInvoiceItem*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid**
```objc
-(NSNumber*) stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsidWithId: (NSString*) _id
    data: (HRPStripeInvoiceItem*) data
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeInvoiceItem id
HRPStripeInvoiceItem* data = [[HRPStripeInvoiceItem alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsidWithId:_id
              data:data
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeInvoiceItem id | 
 **data** | [**HRPStripeInvoiceItem***](HRPStripeInvoiceItem*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemReplaceById**
```objc
-(NSNumber*) stripeInvoiceItemReplaceByIdWithId: (NSString*) _id
    data: (HRPStripeInvoiceItem*) data
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPStripeInvoiceItem* data = [[HRPStripeInvoiceItem alloc] init]; // Model instance data (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance stripeInvoiceItemReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPStripeInvoiceItem***](HRPStripeInvoiceItem*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemReplaceOrCreate**
```objc
-(NSNumber*) stripeInvoiceItemReplaceOrCreateWithData: (HRPStripeInvoiceItem*) data
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeInvoiceItem* data = [[HRPStripeInvoiceItem alloc] init]; // Model instance data (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance stripeInvoiceItemReplaceOrCreateWithData:data
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeInvoiceItem***](HRPStripeInvoiceItem*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemUpdateAll**
```objc
-(NSNumber*) stripeInvoiceItemUpdateAllWithWhere: (NSString*) where
    data: (HRPStripeInvoiceItem*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeInvoiceItem* data = [[HRPStripeInvoiceItem alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance stripeInvoiceItemUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeInvoiceItem***](HRPStripeInvoiceItem*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemUpsertPatchStripeInvoiceItems**
```objc
-(NSNumber*) stripeInvoiceItemUpsertPatchStripeInvoiceItemsWithData: (HRPStripeInvoiceItem*) data
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeInvoiceItem* data = [[HRPStripeInvoiceItem alloc] init]; // Model instance data (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeInvoiceItemUpsertPatchStripeInvoiceItemsWithData:data
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemUpsertPatchStripeInvoiceItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeInvoiceItem***](HRPStripeInvoiceItem*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemUpsertPutStripeInvoiceItems**
```objc
-(NSNumber*) stripeInvoiceItemUpsertPutStripeInvoiceItemsWithData: (HRPStripeInvoiceItem*) data
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeInvoiceItem* data = [[HRPStripeInvoiceItem alloc] init]; // Model instance data (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeInvoiceItemUpsertPutStripeInvoiceItemsWithData:data
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemUpsertPutStripeInvoiceItems: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeInvoiceItem***](HRPStripeInvoiceItem*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeInvoiceItemUpsertWithWhere**
```objc
-(NSNumber*) stripeInvoiceItemUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPStripeInvoiceItem*) data
        completionHandler: (void (^)(HRPStripeInvoiceItem* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeInvoiceItem* data = [[HRPStripeInvoiceItem alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeInvoiceItemApi*apiInstance = [[HRPStripeInvoiceItemApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance stripeInvoiceItemUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPStripeInvoiceItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeInvoiceItemApi->stripeInvoiceItemUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeInvoiceItem***](HRPStripeInvoiceItem*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeInvoiceItem***](HRPStripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

