# HRPPlayerConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mute** | **NSNumber*** | Available on the following SDKs: JS, iOS | [optional] [default to @0]
**autostart** | **NSNumber*** | Available on the following SDKs: JS, iOS, Android | [optional] [default to @0]
**repeat** | **NSNumber*** | Available on the following SDKs: JS, iOS, Android | [optional] [default to @0]
**controls** | **NSNumber*** | Available on the following SDKs: JS, iOS, Android | [optional] [default to @1]
**visualPlaylist** | **NSNumber*** | Available on the following SDKs: JS (as visualplaylist) | [optional] [default to @1]
**displayTitle** | **NSNumber*** | Available on the following SDKs: JS (as displaytitle) | [optional] [default to @1]
**displayDescription** | **NSNumber*** | Available on the following SDKs: JS (as displaydescription) | [optional] [default to @1]
**stretching** | **NSString*** | Available on the following SDKs: JS, iOS, Android (as stretch) | [optional] [default to @"uniform"]
**hlshtml** | **NSNumber*** | Available on the following SDKs: JS | [optional] [default to @0]
**primary** | **NSString*** | Available on the following SDKs: JS | [optional] [default to @"html5"]
**flashPlayer** | **NSString*** | Available on the following SDKs: JS (as flashplayer) | [optional] [default to @"/"]
**baseSkinPath** | **NSString*** | Available on the following SDKs: JS (as base) | [optional] [default to @"/"]
**preload** | **NSString*** | Available on the following SDKs: JS | [optional] 
**playerAdConfig** | **NSObject*** | Available on the following SDKs: JS, iOS, Android | [optional] 
**playerCaptionConfig** | **NSObject*** | Available on the following SDKs: JS, iOS, Android | [optional] 
**skinName** | **NSString*** | Available on the following SDKs: JS (as skin.name), iOS, Android (as premiumSkin) | [optional] 
**skinCss** | **NSString*** | Available on the following SDKs: JS (as skin.url), iOS (as skinUrl), Android (as cssSkin) | [optional] 
**skinActive** | **NSString*** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.active), iOS | [optional] 
**skinInactive** | **NSString*** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.inactive), iOS | [optional] 
**skinBackground** | **NSString*** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.background), iOS | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


