# HRPCustomerBadge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer** | **NSNumber*** |  | [optional] [default to @0.0]
**event** | **NSNumber*** |  | [optional] [default to @0.0]
**dealAll** | **NSNumber*** |  | [optional] [default to @0.0]
**dealCoupon** | **NSNumber*** |  | [optional] [default to @0.0]
**dealSimple** | **NSNumber*** |  | [optional] [default to @0.0]
**dealGroup** | **NSNumber*** |  | [optional] [default to @0.0]
**competition** | **NSNumber*** |  | [optional] [default to @0.0]
**walletOfferAvailable** | **NSNumber*** |  | [optional] [default to @0.0]
**walletOfferNew** | **NSNumber*** |  | [optional] [default to @0.0]
**walletOfferExpiring** | **NSNumber*** |  | [optional] [default to @0.0]
**walletOfferExpired** | **NSNumber*** |  | [optional] [default to @0.0]
**notificationUnread** | **NSNumber*** |  | [optional] [default to @0.0]
**brandFeed** | **NSNumber*** |  | [optional] [default to @0.0]
**brand** | **NSNumber*** |  | [optional] [default to @0.0]
**brandActivity** | **NSNumber*** |  | [optional] [default to @0.0]
**all** | **NSNumber*** |  | [optional] [default to @0.0]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


