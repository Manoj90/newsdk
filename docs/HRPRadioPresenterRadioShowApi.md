# HRPRadioPresenterRadioShowApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPresenterRadioShowCount**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowcount) | **GET** /RadioPresenterRadioShows/count | Count instances of the model matched by where from the data source.
[**radioPresenterRadioShowCreate**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowcreate) | **POST** /RadioPresenterRadioShows | Create a new instance of the model and persist it into the data source.
[**radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowcreatechangestreamgetradiopresenterradioshowschangestream) | **GET** /RadioPresenterRadioShows/change-stream | Create a change stream.
[**radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowcreatechangestreampostradiopresenterradioshowschangestream) | **POST** /RadioPresenterRadioShows/change-stream | Create a change stream.
[**radioPresenterRadioShowDeleteById**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowdeletebyid) | **DELETE** /RadioPresenterRadioShows/{id} | Delete a model instance by {{id}} from the data source.
[**radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowexistsgetradiopresenterradioshowsidexists) | **GET** /RadioPresenterRadioShows/{id}/exists | Check whether a model instance exists in the data source.
[**radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowexistsheadradiopresenterradioshowsid) | **HEAD** /RadioPresenterRadioShows/{id} | Check whether a model instance exists in the data source.
[**radioPresenterRadioShowFind**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowfind) | **GET** /RadioPresenterRadioShows | Find all instances of the model matched by filter from the data source.
[**radioPresenterRadioShowFindById**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowfindbyid) | **GET** /RadioPresenterRadioShows/{id} | Find a model instance by {{id}} from the data source.
[**radioPresenterRadioShowFindOne**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowfindone) | **GET** /RadioPresenterRadioShows/findOne | Find first instance of the model matched by filter from the data source.
[**radioPresenterRadioShowPrototypeGetRadioPresenter**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowprototypegetradiopresenter) | **GET** /RadioPresenterRadioShows/{id}/radioPresenter | Fetches belongsTo relation radioPresenter.
[**radioPresenterRadioShowPrototypeGetRadioShow**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowprototypegetradioshow) | **GET** /RadioPresenterRadioShows/{id}/radioShow | Fetches belongsTo relation radioShow.
[**radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowprototypeupdateattributespatchradiopresenterradioshowsid) | **PATCH** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowprototypeupdateattributesputradiopresenterradioshowsid) | **PUT** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowReplaceById**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowreplacebyid) | **POST** /RadioPresenterRadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowReplaceOrCreate**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowreplaceorcreate) | **POST** /RadioPresenterRadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpdateAll**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowupdateall) | **POST** /RadioPresenterRadioShows/update | Update instances of the model matched by {{where}} from the data source.
[**radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowupsertpatchradiopresenterradioshows) | **PATCH** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpsertPutRadioPresenterRadioShows**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowupsertputradiopresenterradioshows) | **PUT** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpsertWithWhere**](HRPRadioPresenterRadioShowApi.md#radiopresenterradioshowupsertwithwhere) | **POST** /RadioPresenterRadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **radioPresenterRadioShowCount**
```objc
-(NSNumber*) radioPresenterRadioShowCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance radioPresenterRadioShowCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowCreate**
```objc
-(NSNumber*) radioPresenterRadioShowCreateWithData: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; // Model instance data (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance radioPresenterRadioShowCreateWithData:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream**
```objc
-(NSNumber*) radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Create a change stream.
[apiInstance radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream**
```objc
-(NSNumber*) radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Create a change stream.
[apiInstance radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowDeleteById**
```objc
-(NSNumber*) radioPresenterRadioShowDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance radioPresenterRadioShowDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists**
```objc
-(NSNumber*) radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid**
```objc
-(NSNumber*) radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowFind**
```objc
-(NSNumber*) radioPresenterRadioShowFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioPresenterRadioShow>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance radioPresenterRadioShowFindWithFilter:filter
          completionHandler: ^(NSArray<HRPRadioPresenterRadioShow>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPRadioPresenterRadioShow>***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowFindById**
```objc
-(NSNumber*) radioPresenterRadioShowFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance radioPresenterRadioShowFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowFindOne**
```objc
-(NSNumber*) radioPresenterRadioShowFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance radioPresenterRadioShowFindOneWithFilter:filter
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowPrototypeGetRadioPresenter**
```objc
-(NSNumber*) radioPresenterRadioShowPrototypeGetRadioPresenterWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPRadioPresenter* output, NSError* error)) handler;
```

Fetches belongsTo relation radioPresenter.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenterRadioShow id
NSNumber* refresh = @true; //  (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Fetches belongsTo relation radioPresenter.
[apiInstance radioPresenterRadioShowPrototypeGetRadioPresenterWithId:_id
              refresh:refresh
          completionHandler: ^(HRPRadioPresenter* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowPrototypeGetRadioPresenter: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenterRadioShow id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPRadioPresenter***](HRPRadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowPrototypeGetRadioShow**
```objc
-(NSNumber*) radioPresenterRadioShowPrototypeGetRadioShowWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPRadioShow* output, NSError* error)) handler;
```

Fetches belongsTo relation radioShow.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenterRadioShow id
NSNumber* refresh = @true; //  (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Fetches belongsTo relation radioShow.
[apiInstance radioPresenterRadioShowPrototypeGetRadioShowWithId:_id
              refresh:refresh
          completionHandler: ^(HRPRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowPrototypeGetRadioShow: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenterRadioShow id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPRadioShow***](HRPRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid**
```objc
-(NSNumber*) radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsidWithId: (NSString*) _id
    data: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenterRadioShow id
HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsidWithId:_id
              data:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenterRadioShow id | 
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid**
```objc
-(NSNumber*) radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsidWithId: (NSString*) _id
    data: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // RadioPresenterRadioShow id
HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsidWithId:_id
              data:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| RadioPresenterRadioShow id | 
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowReplaceById**
```objc
-(NSNumber*) radioPresenterRadioShowReplaceByIdWithId: (NSString*) _id
    data: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; // Model instance data (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance radioPresenterRadioShowReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowReplaceOrCreate**
```objc
-(NSNumber*) radioPresenterRadioShowReplaceOrCreateWithData: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; // Model instance data (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance radioPresenterRadioShowReplaceOrCreateWithData:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowUpdateAll**
```objc
-(NSNumber*) radioPresenterRadioShowUpdateAllWithWhere: (NSString*) where
    data: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance radioPresenterRadioShowUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows**
```objc
-(NSNumber*) radioPresenterRadioShowUpsertPatchRadioPresenterRadioShowsWithData: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; // Model instance data (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioPresenterRadioShowUpsertPatchRadioPresenterRadioShowsWithData:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowUpsertPutRadioPresenterRadioShows**
```objc
-(NSNumber*) radioPresenterRadioShowUpsertPutRadioPresenterRadioShowsWithData: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; // Model instance data (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance radioPresenterRadioShowUpsertPutRadioPresenterRadioShowsWithData:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowUpsertPutRadioPresenterRadioShows: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)| Model instance data | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **radioPresenterRadioShowUpsertWithWhere**
```objc
-(NSNumber*) radioPresenterRadioShowUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPRadioPresenterRadioShow*) data
        completionHandler: (void (^)(HRPRadioPresenterRadioShow* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPRadioPresenterRadioShow* data = [[HRPRadioPresenterRadioShow alloc] init]; // An object of model property name/value pairs (optional)

HRPRadioPresenterRadioShowApi*apiInstance = [[HRPRadioPresenterRadioShowApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance radioPresenterRadioShowUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPRadioPresenterRadioShow* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPRadioPresenterRadioShowApi->radioPresenterRadioShowUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPRadioPresenterRadioShow***](HRPRadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

