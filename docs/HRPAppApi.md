# HRPAppApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**appAnalyticTrack**](HRPAppApi.md#appanalytictrack) | **POST** /Apps/{id}/analytics/track | 
[**appCount**](HRPAppApi.md#appcount) | **GET** /Apps/count | Count instances of the model matched by where from the data source.
[**appCreateChangeStreamGetAppsChangeStream**](HRPAppApi.md#appcreatechangestreamgetappschangestream) | **GET** /Apps/change-stream | Create a change stream.
[**appCreateChangeStreamPostAppsChangeStream**](HRPAppApi.md#appcreatechangestreampostappschangestream) | **POST** /Apps/change-stream | Create a change stream.
[**appCreateWithAuthentication**](HRPAppApi.md#appcreatewithauthentication) | **POST** /Apps | Create a new instance of the model and persist it into the data source.
[**appDeleteById**](HRPAppApi.md#appdeletebyid) | **DELETE** /Apps/{id} | Delete a model instance by {{id}} from the data source.
[**appExistsGetAppsidExists**](HRPAppApi.md#appexistsgetappsidexists) | **GET** /Apps/{id}/exists | Check whether a model instance exists in the data source.
[**appExistsHeadAppsid**](HRPAppApi.md#appexistsheadappsid) | **HEAD** /Apps/{id} | Check whether a model instance exists in the data source.
[**appFind**](HRPAppApi.md#appfind) | **GET** /Apps | Find all instances of the model matched by filter from the data source.
[**appFindById**](HRPAppApi.md#appfindbyid) | **GET** /Apps/{id} | Find a model instance by {{id}} from the data source.
[**appFindByIdForVersion**](HRPAppApi.md#appfindbyidforversion) | **GET** /Apps/{id}/{appOs}/{appVersion} | 
[**appFindOne**](HRPAppApi.md#appfindone) | **GET** /Apps/findOne | Find first instance of the model matched by filter from the data source.
[**appPrototypeCountAppConfigs**](HRPAppApi.md#appprototypecountappconfigs) | **GET** /Apps/{id}/appConfigs/count | Counts appConfigs of App.
[**appPrototypeCountApps**](HRPAppApi.md#appprototypecountapps) | **GET** /Apps/{id}/apps/count | Counts apps of App.
[**appPrototypeCountPlaylists**](HRPAppApi.md#appprototypecountplaylists) | **GET** /Apps/{id}/playlists/count | Counts playlists of App.
[**appPrototypeCountRadioStreams**](HRPAppApi.md#appprototypecountradiostreams) | **GET** /Apps/{id}/radioStreams/count | Counts radioStreams of App.
[**appPrototypeCountRssFeedGroups**](HRPAppApi.md#appprototypecountrssfeedgroups) | **GET** /Apps/{id}/rssFeedGroups/count | Counts rssFeedGroups of App.
[**appPrototypeCountRssFeeds**](HRPAppApi.md#appprototypecountrssfeeds) | **GET** /Apps/{id}/rssFeeds/count | Counts rssFeeds of App.
[**appPrototypeCreateAppConfigs**](HRPAppApi.md#appprototypecreateappconfigs) | **POST** /Apps/{id}/appConfigs | Creates a new instance in appConfigs of this model.
[**appPrototypeCreateApps**](HRPAppApi.md#appprototypecreateapps) | **POST** /Apps/{id}/apps | Creates a new instance in apps of this model.
[**appPrototypeCreateCustomers**](HRPAppApi.md#appprototypecreatecustomers) | **POST** /Apps/{id}/customers | Creates a new instance in customers of this model.
[**appPrototypeCreatePlaylists**](HRPAppApi.md#appprototypecreateplaylists) | **POST** /Apps/{id}/playlists | Creates a new instance in playlists of this model.
[**appPrototypeCreateRadioStreams**](HRPAppApi.md#appprototypecreateradiostreams) | **POST** /Apps/{id}/radioStreams | Creates a new instance in radioStreams of this model.
[**appPrototypeCreateRssFeedGroups**](HRPAppApi.md#appprototypecreaterssfeedgroups) | **POST** /Apps/{id}/rssFeedGroups | Creates a new instance in rssFeedGroups of this model.
[**appPrototypeCreateRssFeeds**](HRPAppApi.md#appprototypecreaterssfeeds) | **POST** /Apps/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
[**appPrototypeDeleteAppConfigs**](HRPAppApi.md#appprototypedeleteappconfigs) | **DELETE** /Apps/{id}/appConfigs | Deletes all appConfigs of this model.
[**appPrototypeDeleteApps**](HRPAppApi.md#appprototypedeleteapps) | **DELETE** /Apps/{id}/apps | Deletes all apps of this model.
[**appPrototypeDeletePlaylists**](HRPAppApi.md#appprototypedeleteplaylists) | **DELETE** /Apps/{id}/playlists | Deletes all playlists of this model.
[**appPrototypeDeleteRadioStreams**](HRPAppApi.md#appprototypedeleteradiostreams) | **DELETE** /Apps/{id}/radioStreams | Deletes all radioStreams of this model.
[**appPrototypeDeleteRssFeedGroups**](HRPAppApi.md#appprototypedeleterssfeedgroups) | **DELETE** /Apps/{id}/rssFeedGroups | Deletes all rssFeedGroups of this model.
[**appPrototypeDeleteRssFeeds**](HRPAppApi.md#appprototypedeleterssfeeds) | **DELETE** /Apps/{id}/rssFeeds | Deletes all rssFeeds of this model.
[**appPrototypeDestroyByIdAppConfigs**](HRPAppApi.md#appprototypedestroybyidappconfigs) | **DELETE** /Apps/{id}/appConfigs/{fk} | Delete a related item by id for appConfigs.
[**appPrototypeDestroyByIdApps**](HRPAppApi.md#appprototypedestroybyidapps) | **DELETE** /Apps/{id}/apps/{fk} | Delete a related item by id for apps.
[**appPrototypeDestroyByIdPlaylists**](HRPAppApi.md#appprototypedestroybyidplaylists) | **DELETE** /Apps/{id}/playlists/{fk} | Delete a related item by id for playlists.
[**appPrototypeDestroyByIdRadioStreams**](HRPAppApi.md#appprototypedestroybyidradiostreams) | **DELETE** /Apps/{id}/radioStreams/{fk} | Delete a related item by id for radioStreams.
[**appPrototypeDestroyByIdRssFeedGroups**](HRPAppApi.md#appprototypedestroybyidrssfeedgroups) | **DELETE** /Apps/{id}/rssFeedGroups/{fk} | Delete a related item by id for rssFeedGroups.
[**appPrototypeDestroyByIdRssFeeds**](HRPAppApi.md#appprototypedestroybyidrssfeeds) | **DELETE** /Apps/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
[**appPrototypeFindByIdAppConfigs**](HRPAppApi.md#appprototypefindbyidappconfigs) | **GET** /Apps/{id}/appConfigs/{fk} | Find a related item by id for appConfigs.
[**appPrototypeFindByIdApps**](HRPAppApi.md#appprototypefindbyidapps) | **GET** /Apps/{id}/apps/{fk} | Find a related item by id for apps.
[**appPrototypeFindByIdCustomers**](HRPAppApi.md#appprototypefindbyidcustomers) | **GET** /Apps/{id}/customers/{fk} | Find a related item by id for customers.
[**appPrototypeFindByIdPlaylists**](HRPAppApi.md#appprototypefindbyidplaylists) | **GET** /Apps/{id}/playlists/{fk} | Find a related item by id for playlists.
[**appPrototypeFindByIdRadioStreams**](HRPAppApi.md#appprototypefindbyidradiostreams) | **GET** /Apps/{id}/radioStreams/{fk} | Find a related item by id for radioStreams.
[**appPrototypeFindByIdRssFeedGroups**](HRPAppApi.md#appprototypefindbyidrssfeedgroups) | **GET** /Apps/{id}/rssFeedGroups/{fk} | Find a related item by id for rssFeedGroups.
[**appPrototypeFindByIdRssFeeds**](HRPAppApi.md#appprototypefindbyidrssfeeds) | **GET** /Apps/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
[**appPrototypeGetAppConfigs**](HRPAppApi.md#appprototypegetappconfigs) | **GET** /Apps/{id}/appConfigs | Queries appConfigs of App.
[**appPrototypeGetApps**](HRPAppApi.md#appprototypegetapps) | **GET** /Apps/{id}/apps | Queries apps of App.
[**appPrototypeGetCustomers**](HRPAppApi.md#appprototypegetcustomers) | **GET** /Apps/{id}/customers | Queries customers of App.
[**appPrototypeGetParent**](HRPAppApi.md#appprototypegetparent) | **GET** /Apps/{id}/parent | Fetches belongsTo relation parent.
[**appPrototypeGetPlaylists**](HRPAppApi.md#appprototypegetplaylists) | **GET** /Apps/{id}/playlists | Queries playlists of App.
[**appPrototypeGetRadioStreams**](HRPAppApi.md#appprototypegetradiostreams) | **GET** /Apps/{id}/radioStreams | Queries radioStreams of App.
[**appPrototypeGetRssFeedGroups**](HRPAppApi.md#appprototypegetrssfeedgroups) | **GET** /Apps/{id}/rssFeedGroups | Queries rssFeedGroups of App.
[**appPrototypeGetRssFeeds**](HRPAppApi.md#appprototypegetrssfeeds) | **GET** /Apps/{id}/rssFeeds | Queries rssFeeds of App.
[**appPrototypeUpdateAttributesPatchAppsid**](HRPAppApi.md#appprototypeupdateattributespatchappsid) | **PATCH** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
[**appPrototypeUpdateAttributesPutAppsid**](HRPAppApi.md#appprototypeupdateattributesputappsid) | **PUT** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
[**appPrototypeUpdateByIdAppConfigs**](HRPAppApi.md#appprototypeupdatebyidappconfigs) | **PUT** /Apps/{id}/appConfigs/{fk} | Update a related item by id for appConfigs.
[**appPrototypeUpdateByIdApps**](HRPAppApi.md#appprototypeupdatebyidapps) | **PUT** /Apps/{id}/apps/{fk} | Update a related item by id for apps.
[**appPrototypeUpdateByIdCustomers**](HRPAppApi.md#appprototypeupdatebyidcustomers) | **PUT** /Apps/{id}/customers/{fk} | Update a related item by id for customers.
[**appPrototypeUpdateByIdPlaylists**](HRPAppApi.md#appprototypeupdatebyidplaylists) | **PUT** /Apps/{id}/playlists/{fk} | Update a related item by id for playlists.
[**appPrototypeUpdateByIdRadioStreams**](HRPAppApi.md#appprototypeupdatebyidradiostreams) | **PUT** /Apps/{id}/radioStreams/{fk} | Update a related item by id for radioStreams.
[**appPrototypeUpdateByIdRssFeedGroups**](HRPAppApi.md#appprototypeupdatebyidrssfeedgroups) | **PUT** /Apps/{id}/rssFeedGroups/{fk} | Update a related item by id for rssFeedGroups.
[**appPrototypeUpdateByIdRssFeeds**](HRPAppApi.md#appprototypeupdatebyidrssfeeds) | **PUT** /Apps/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
[**appReplaceById**](HRPAppApi.md#appreplacebyid) | **POST** /Apps/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**appReplaceOrCreate**](HRPAppApi.md#appreplaceorcreate) | **POST** /Apps/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**appUpdateAll**](HRPAppApi.md#appupdateall) | **POST** /Apps/update | Update instances of the model matched by {{where}} from the data source.
[**appUploadFile**](HRPAppApi.md#appuploadfile) | **POST** /Apps/{id}/file/{type} | 
[**appUpsertPatchApps**](HRPAppApi.md#appupsertpatchapps) | **PATCH** /Apps | Patch an existing model instance or insert a new one into the data source.
[**appUpsertPutApps**](HRPAppApi.md#appupsertputapps) | **PUT** /Apps | Patch an existing model instance or insert a new one into the data source.
[**appUpsertWithWhere**](HRPAppApi.md#appupsertwithwhere) | **POST** /Apps/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**appVerify**](HRPAppApi.md#appverify) | **GET** /Apps/verify | 


# **appAnalyticTrack**
```objc
-(NSNumber*) appAnalyticTrackWithId: (NSString*) _id
    data: (HRPAnalyticRequest*) data
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // 
HRPAnalyticRequest* data = [[HRPAnalyticRequest alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

[apiInstance appAnalyticTrackWithId:_id
              data:data
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appAnalyticTrack: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***|  | 
 **data** | [**HRPAnalyticRequest***](HRPAnalyticRequest*.md)|  | [optional] 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appCount**
```objc
-(NSNumber*) appCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance appCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appCreateChangeStreamGetAppsChangeStream**
```objc
-(NSNumber*) appCreateChangeStreamGetAppsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Create a change stream.
[apiInstance appCreateChangeStreamGetAppsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appCreateChangeStreamGetAppsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appCreateChangeStreamPostAppsChangeStream**
```objc
-(NSNumber*) appCreateChangeStreamPostAppsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Create a change stream.
[apiInstance appCreateChangeStreamPostAppsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appCreateChangeStreamPostAppsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appCreateWithAuthentication**
```objc
-(NSNumber*) appCreateWithAuthenticationWithData: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPApp* data = [[HRPApp alloc] init]; // Model instance data (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance appCreateWithAuthenticationWithData:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appCreateWithAuthentication: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPApp***](HRPApp*.md)| Model instance data | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appDeleteById**
```objc
-(NSNumber*) appDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance appDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appExistsGetAppsidExists**
```objc
-(NSNumber*) appExistsGetAppsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance appExistsGetAppsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appExistsGetAppsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appExistsHeadAppsid**
```objc
-(NSNumber*) appExistsHeadAppsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance appExistsHeadAppsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appExistsHeadAppsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appFind**
```objc
-(NSNumber*) appFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPApp>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance appFindWithFilter:filter
          completionHandler: ^(NSArray<HRPApp>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPApp>***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appFindById**
```objc
-(NSNumber*) appFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance appFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appFindByIdForVersion**
```objc
-(NSNumber*) appFindByIdForVersionWithId: (NSString*) _id
    appOs: (NSString*) appOs
    appVersion: (NSString*) appVersion
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // 
NSString* appOs = @"appOs_example"; // Either \"ios\" or \"android\"
NSString* appVersion = @"appVersion_example"; // e.g. 1.2.3

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

[apiInstance appFindByIdForVersionWithId:_id
              appOs:appOs
              appVersion:appVersion
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appFindByIdForVersion: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***|  | 
 **appOs** | **NSString***| Either \&quot;ios\&quot; or \&quot;android\&quot; | 
 **appVersion** | **NSString***| e.g. 1.2.3 | 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appFindOne**
```objc
-(NSNumber*) appFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance appFindOneWithFilter:filter
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCountAppConfigs**
```objc
-(NSNumber*) appPrototypeCountAppConfigsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts appConfigs of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Counts appConfigs of App.
[apiInstance appPrototypeCountAppConfigsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCountAppConfigs: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCountApps**
```objc
-(NSNumber*) appPrototypeCountAppsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts apps of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Counts apps of App.
[apiInstance appPrototypeCountAppsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCountApps: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCountPlaylists**
```objc
-(NSNumber*) appPrototypeCountPlaylistsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts playlists of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Counts playlists of App.
[apiInstance appPrototypeCountPlaylistsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCountPlaylists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCountRadioStreams**
```objc
-(NSNumber*) appPrototypeCountRadioStreamsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts radioStreams of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Counts radioStreams of App.
[apiInstance appPrototypeCountRadioStreamsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCountRadioStreams: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCountRssFeedGroups**
```objc
-(NSNumber*) appPrototypeCountRssFeedGroupsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts rssFeedGroups of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Counts rssFeedGroups of App.
[apiInstance appPrototypeCountRssFeedGroupsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCountRssFeedGroups: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCountRssFeeds**
```objc
-(NSNumber*) appPrototypeCountRssFeedsWithId: (NSString*) _id
    where: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Counts rssFeeds of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Counts rssFeeds of App.
[apiInstance appPrototypeCountRssFeedsWithId:_id
              where:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCountRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCreateAppConfigs**
```objc
-(NSNumber*) appPrototypeCreateAppConfigsWithId: (NSString*) _id
    data: (HRPAppConfig*) data
        completionHandler: (void (^)(HRPAppConfig* output, NSError* error)) handler;
```

Creates a new instance in appConfigs of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
HRPAppConfig* data = [[HRPAppConfig alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Creates a new instance in appConfigs of this model.
[apiInstance appPrototypeCreateAppConfigsWithId:_id
              data:data
          completionHandler: ^(HRPAppConfig* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCreateAppConfigs: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **data** | [**HRPAppConfig***](HRPAppConfig*.md)|  | [optional] 

### Return type

[**HRPAppConfig***](HRPAppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCreateApps**
```objc
-(NSNumber*) appPrototypeCreateAppsWithId: (NSString*) _id
    data: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Creates a new instance in apps of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
HRPApp* data = [[HRPApp alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Creates a new instance in apps of this model.
[apiInstance appPrototypeCreateAppsWithId:_id
              data:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCreateApps: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **data** | [**HRPApp***](HRPApp*.md)|  | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCreateCustomers**
```objc
-(NSNumber*) appPrototypeCreateCustomersWithId: (NSString*) _id
    data: (HRPCustomer*) data
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Creates a new instance in customers of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
HRPCustomer* data = [[HRPCustomer alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Creates a new instance in customers of this model.
[apiInstance appPrototypeCreateCustomersWithId:_id
              data:data
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCreateCustomers: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **data** | [**HRPCustomer***](HRPCustomer*.md)|  | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCreatePlaylists**
```objc
-(NSNumber*) appPrototypeCreatePlaylistsWithId: (NSString*) _id
    data: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Creates a new instance in playlists of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
HRPPlaylist* data = [[HRPPlaylist alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Creates a new instance in playlists of this model.
[apiInstance appPrototypeCreatePlaylistsWithId:_id
              data:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCreatePlaylists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)|  | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCreateRadioStreams**
```objc
-(NSNumber*) appPrototypeCreateRadioStreamsWithId: (NSString*) _id
    data: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Creates a new instance in radioStreams of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
HRPRadioStream* data = [[HRPRadioStream alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Creates a new instance in radioStreams of this model.
[apiInstance appPrototypeCreateRadioStreamsWithId:_id
              data:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCreateRadioStreams: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)|  | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCreateRssFeedGroups**
```objc
-(NSNumber*) appPrototypeCreateRssFeedGroupsWithId: (NSString*) _id
    data: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Creates a new instance in rssFeedGroups of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Creates a new instance in rssFeedGroups of this model.
[apiInstance appPrototypeCreateRssFeedGroupsWithId:_id
              data:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCreateRssFeedGroups: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)|  | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeCreateRssFeeds**
```objc
-(NSNumber*) appPrototypeCreateRssFeedsWithId: (NSString*) _id
    data: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Creates a new instance in rssFeeds of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
HRPRssFeed* data = [[HRPRssFeed alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Creates a new instance in rssFeeds of this model.
[apiInstance appPrototypeCreateRssFeedsWithId:_id
              data:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeCreateRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)|  | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDeleteAppConfigs**
```objc
-(NSNumber*) appPrototypeDeleteAppConfigsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all appConfigs of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Deletes all appConfigs of this model.
[apiInstance appPrototypeDeleteAppConfigsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDeleteAppConfigs: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDeleteApps**
```objc
-(NSNumber*) appPrototypeDeleteAppsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all apps of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Deletes all apps of this model.
[apiInstance appPrototypeDeleteAppsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDeleteApps: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDeletePlaylists**
```objc
-(NSNumber*) appPrototypeDeletePlaylistsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all playlists of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Deletes all playlists of this model.
[apiInstance appPrototypeDeletePlaylistsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDeletePlaylists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDeleteRadioStreams**
```objc
-(NSNumber*) appPrototypeDeleteRadioStreamsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all radioStreams of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Deletes all radioStreams of this model.
[apiInstance appPrototypeDeleteRadioStreamsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDeleteRadioStreams: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDeleteRssFeedGroups**
```objc
-(NSNumber*) appPrototypeDeleteRssFeedGroupsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all rssFeedGroups of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Deletes all rssFeedGroups of this model.
[apiInstance appPrototypeDeleteRssFeedGroupsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDeleteRssFeedGroups: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDeleteRssFeeds**
```objc
-(NSNumber*) appPrototypeDeleteRssFeedsWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes all rssFeeds of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Deletes all rssFeeds of this model.
[apiInstance appPrototypeDeleteRssFeedsWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDeleteRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDestroyByIdAppConfigs**
```objc
-(NSNumber*) appPrototypeDestroyByIdAppConfigsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for appConfigs.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for appConfigs
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Delete a related item by id for appConfigs.
[apiInstance appPrototypeDestroyByIdAppConfigsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDestroyByIdAppConfigs: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for appConfigs | 
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDestroyByIdApps**
```objc
-(NSNumber*) appPrototypeDestroyByIdAppsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for apps.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for apps
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Delete a related item by id for apps.
[apiInstance appPrototypeDestroyByIdAppsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDestroyByIdApps: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for apps | 
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDestroyByIdPlaylists**
```objc
-(NSNumber*) appPrototypeDestroyByIdPlaylistsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for playlists.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playlists
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Delete a related item by id for playlists.
[apiInstance appPrototypeDestroyByIdPlaylistsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDestroyByIdPlaylists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playlists | 
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDestroyByIdRadioStreams**
```objc
-(NSNumber*) appPrototypeDestroyByIdRadioStreamsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for radioStreams.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioStreams
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Delete a related item by id for radioStreams.
[apiInstance appPrototypeDestroyByIdRadioStreamsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDestroyByIdRadioStreams: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioStreams | 
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDestroyByIdRssFeedGroups**
```objc
-(NSNumber*) appPrototypeDestroyByIdRssFeedGroupsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for rssFeedGroups.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for rssFeedGroups
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Delete a related item by id for rssFeedGroups.
[apiInstance appPrototypeDestroyByIdRssFeedGroupsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDestroyByIdRssFeedGroups: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for rssFeedGroups | 
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeDestroyByIdRssFeeds**
```objc
-(NSNumber*) appPrototypeDestroyByIdRssFeedsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a related item by id for rssFeeds.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for rssFeeds
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Delete a related item by id for rssFeeds.
[apiInstance appPrototypeDestroyByIdRssFeedsWithFk:fk
              _id:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeDestroyByIdRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for rssFeeds | 
 **_id** | **NSString***| App id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeFindByIdAppConfigs**
```objc
-(NSNumber*) appPrototypeFindByIdAppConfigsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPAppConfig* output, NSError* error)) handler;
```

Find a related item by id for appConfigs.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for appConfigs
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find a related item by id for appConfigs.
[apiInstance appPrototypeFindByIdAppConfigsWithFk:fk
              _id:_id
          completionHandler: ^(HRPAppConfig* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeFindByIdAppConfigs: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for appConfigs | 
 **_id** | **NSString***| App id | 

### Return type

[**HRPAppConfig***](HRPAppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeFindByIdApps**
```objc
-(NSNumber*) appPrototypeFindByIdAppsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Find a related item by id for apps.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for apps
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find a related item by id for apps.
[apiInstance appPrototypeFindByIdAppsWithFk:fk
              _id:_id
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeFindByIdApps: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for apps | 
 **_id** | **NSString***| App id | 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeFindByIdCustomers**
```objc
-(NSNumber*) appPrototypeFindByIdCustomersWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Find a related item by id for customers.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for customers
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find a related item by id for customers.
[apiInstance appPrototypeFindByIdCustomersWithFk:fk
              _id:_id
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeFindByIdCustomers: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for customers | 
 **_id** | **NSString***| App id | 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeFindByIdPlaylists**
```objc
-(NSNumber*) appPrototypeFindByIdPlaylistsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Find a related item by id for playlists.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playlists
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find a related item by id for playlists.
[apiInstance appPrototypeFindByIdPlaylistsWithFk:fk
              _id:_id
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeFindByIdPlaylists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playlists | 
 **_id** | **NSString***| App id | 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeFindByIdRadioStreams**
```objc
-(NSNumber*) appPrototypeFindByIdRadioStreamsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Find a related item by id for radioStreams.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioStreams
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find a related item by id for radioStreams.
[apiInstance appPrototypeFindByIdRadioStreamsWithFk:fk
              _id:_id
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeFindByIdRadioStreams: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioStreams | 
 **_id** | **NSString***| App id | 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeFindByIdRssFeedGroups**
```objc
-(NSNumber*) appPrototypeFindByIdRssFeedGroupsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Find a related item by id for rssFeedGroups.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for rssFeedGroups
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find a related item by id for rssFeedGroups.
[apiInstance appPrototypeFindByIdRssFeedGroupsWithFk:fk
              _id:_id
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeFindByIdRssFeedGroups: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for rssFeedGroups | 
 **_id** | **NSString***| App id | 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeFindByIdRssFeeds**
```objc
-(NSNumber*) appPrototypeFindByIdRssFeedsWithFk: (NSString*) fk
    _id: (NSString*) _id
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Find a related item by id for rssFeeds.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for rssFeeds
NSString* _id = @"_id_example"; // App id

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Find a related item by id for rssFeeds.
[apiInstance appPrototypeFindByIdRssFeedsWithFk:fk
              _id:_id
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeFindByIdRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for rssFeeds | 
 **_id** | **NSString***| App id | 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeGetAppConfigs**
```objc
-(NSNumber*) appPrototypeGetAppConfigsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAppConfig>* output, NSError* error)) handler;
```

Queries appConfigs of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* filter = @"filter_example"; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Queries appConfigs of App.
[apiInstance appPrototypeGetAppConfigsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPAppConfig>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeGetAppConfigs: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPAppConfig>***](HRPAppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeGetApps**
```objc
-(NSNumber*) appPrototypeGetAppsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPApp>* output, NSError* error)) handler;
```

Queries apps of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* filter = @"filter_example"; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Queries apps of App.
[apiInstance appPrototypeGetAppsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPApp>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeGetApps: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPApp>***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeGetCustomers**
```objc
-(NSNumber*) appPrototypeGetCustomersWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCustomer>* output, NSError* error)) handler;
```

Queries customers of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* filter = @"filter_example"; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Queries customers of App.
[apiInstance appPrototypeGetCustomersWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCustomer>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeGetCustomers: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPCustomer>***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeGetParent**
```objc
-(NSNumber*) appPrototypeGetParentWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Fetches belongsTo relation parent.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSNumber* refresh = @true; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Fetches belongsTo relation parent.
[apiInstance appPrototypeGetParentWithId:_id
              refresh:refresh
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeGetParent: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeGetPlaylists**
```objc
-(NSNumber*) appPrototypeGetPlaylistsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPPlaylist>* output, NSError* error)) handler;
```

Queries playlists of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* filter = @"filter_example"; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Queries playlists of App.
[apiInstance appPrototypeGetPlaylistsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPPlaylist>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeGetPlaylists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPPlaylist>***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeGetRadioStreams**
```objc
-(NSNumber*) appPrototypeGetRadioStreamsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRadioStream>* output, NSError* error)) handler;
```

Queries radioStreams of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* filter = @"filter_example"; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Queries radioStreams of App.
[apiInstance appPrototypeGetRadioStreamsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPRadioStream>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeGetRadioStreams: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPRadioStream>***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeGetRssFeedGroups**
```objc
-(NSNumber*) appPrototypeGetRssFeedGroupsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRssFeedGroup>* output, NSError* error)) handler;
```

Queries rssFeedGroups of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* filter = @"filter_example"; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Queries rssFeedGroups of App.
[apiInstance appPrototypeGetRssFeedGroupsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPRssFeedGroup>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeGetRssFeedGroups: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPRssFeedGroup>***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeGetRssFeeds**
```objc
-(NSNumber*) appPrototypeGetRssFeedsWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPRssFeed>* output, NSError* error)) handler;
```

Queries rssFeeds of App.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
NSString* filter = @"filter_example"; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Queries rssFeeds of App.
[apiInstance appPrototypeGetRssFeedsWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPRssFeed>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeGetRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **filter** | **NSString***|  | [optional] 

### Return type

[**NSArray<HRPRssFeed>***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeUpdateAttributesPatchAppsid**
```objc
-(NSNumber*) appPrototypeUpdateAttributesPatchAppsidWithId: (NSString*) _id
    data: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
HRPApp* data = [[HRPApp alloc] init]; // An object of model property name/value pairs (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance appPrototypeUpdateAttributesPatchAppsidWithId:_id
              data:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeUpdateAttributesPatchAppsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **data** | [**HRPApp***](HRPApp*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeUpdateAttributesPutAppsid**
```objc
-(NSNumber*) appPrototypeUpdateAttributesPutAppsidWithId: (NSString*) _id
    data: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // App id
HRPApp* data = [[HRPApp alloc] init]; // An object of model property name/value pairs (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance appPrototypeUpdateAttributesPutAppsidWithId:_id
              data:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeUpdateAttributesPutAppsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| App id | 
 **data** | [**HRPApp***](HRPApp*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeUpdateByIdAppConfigs**
```objc
-(NSNumber*) appPrototypeUpdateByIdAppConfigsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPAppConfig*) data
        completionHandler: (void (^)(HRPAppConfig* output, NSError* error)) handler;
```

Update a related item by id for appConfigs.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for appConfigs
NSString* _id = @"_id_example"; // App id
HRPAppConfig* data = [[HRPAppConfig alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Update a related item by id for appConfigs.
[apiInstance appPrototypeUpdateByIdAppConfigsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPAppConfig* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeUpdateByIdAppConfigs: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for appConfigs | 
 **_id** | **NSString***| App id | 
 **data** | [**HRPAppConfig***](HRPAppConfig*.md)|  | [optional] 

### Return type

[**HRPAppConfig***](HRPAppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeUpdateByIdApps**
```objc
-(NSNumber*) appPrototypeUpdateByIdAppsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Update a related item by id for apps.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for apps
NSString* _id = @"_id_example"; // App id
HRPApp* data = [[HRPApp alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Update a related item by id for apps.
[apiInstance appPrototypeUpdateByIdAppsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeUpdateByIdApps: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for apps | 
 **_id** | **NSString***| App id | 
 **data** | [**HRPApp***](HRPApp*.md)|  | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeUpdateByIdCustomers**
```objc
-(NSNumber*) appPrototypeUpdateByIdCustomersWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPCustomer*) data
        completionHandler: (void (^)(HRPCustomer* output, NSError* error)) handler;
```

Update a related item by id for customers.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for customers
NSString* _id = @"_id_example"; // App id
HRPCustomer* data = [[HRPCustomer alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Update a related item by id for customers.
[apiInstance appPrototypeUpdateByIdCustomersWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPCustomer* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeUpdateByIdCustomers: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for customers | 
 **_id** | **NSString***| App id | 
 **data** | [**HRPCustomer***](HRPCustomer*.md)|  | [optional] 

### Return type

[**HRPCustomer***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeUpdateByIdPlaylists**
```objc
-(NSNumber*) appPrototypeUpdateByIdPlaylistsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPPlaylist*) data
        completionHandler: (void (^)(HRPPlaylist* output, NSError* error)) handler;
```

Update a related item by id for playlists.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for playlists
NSString* _id = @"_id_example"; // App id
HRPPlaylist* data = [[HRPPlaylist alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Update a related item by id for playlists.
[apiInstance appPrototypeUpdateByIdPlaylistsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPPlaylist* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeUpdateByIdPlaylists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for playlists | 
 **_id** | **NSString***| App id | 
 **data** | [**HRPPlaylist***](HRPPlaylist*.md)|  | [optional] 

### Return type

[**HRPPlaylist***](HRPPlaylist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeUpdateByIdRadioStreams**
```objc
-(NSNumber*) appPrototypeUpdateByIdRadioStreamsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRadioStream*) data
        completionHandler: (void (^)(HRPRadioStream* output, NSError* error)) handler;
```

Update a related item by id for radioStreams.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for radioStreams
NSString* _id = @"_id_example"; // App id
HRPRadioStream* data = [[HRPRadioStream alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Update a related item by id for radioStreams.
[apiInstance appPrototypeUpdateByIdRadioStreamsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRadioStream* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeUpdateByIdRadioStreams: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for radioStreams | 
 **_id** | **NSString***| App id | 
 **data** | [**HRPRadioStream***](HRPRadioStream*.md)|  | [optional] 

### Return type

[**HRPRadioStream***](HRPRadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeUpdateByIdRssFeedGroups**
```objc
-(NSNumber*) appPrototypeUpdateByIdRssFeedGroupsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRssFeedGroup*) data
        completionHandler: (void (^)(HRPRssFeedGroup* output, NSError* error)) handler;
```

Update a related item by id for rssFeedGroups.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for rssFeedGroups
NSString* _id = @"_id_example"; // App id
HRPRssFeedGroup* data = [[HRPRssFeedGroup alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Update a related item by id for rssFeedGroups.
[apiInstance appPrototypeUpdateByIdRssFeedGroupsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRssFeedGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeUpdateByIdRssFeedGroups: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for rssFeedGroups | 
 **_id** | **NSString***| App id | 
 **data** | [**HRPRssFeedGroup***](HRPRssFeedGroup*.md)|  | [optional] 

### Return type

[**HRPRssFeedGroup***](HRPRssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appPrototypeUpdateByIdRssFeeds**
```objc
-(NSNumber*) appPrototypeUpdateByIdRssFeedsWithFk: (NSString*) fk
    _id: (NSString*) _id
    data: (HRPRssFeed*) data
        completionHandler: (void (^)(HRPRssFeed* output, NSError* error)) handler;
```

Update a related item by id for rssFeeds.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* fk = @"fk_example"; // Foreign key for rssFeeds
NSString* _id = @"_id_example"; // App id
HRPRssFeed* data = [[HRPRssFeed alloc] init]; //  (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Update a related item by id for rssFeeds.
[apiInstance appPrototypeUpdateByIdRssFeedsWithFk:fk
              _id:_id
              data:data
          completionHandler: ^(HRPRssFeed* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appPrototypeUpdateByIdRssFeeds: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **NSString***| Foreign key for rssFeeds | 
 **_id** | **NSString***| App id | 
 **data** | [**HRPRssFeed***](HRPRssFeed*.md)|  | [optional] 

### Return type

[**HRPRssFeed***](HRPRssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appReplaceById**
```objc
-(NSNumber*) appReplaceByIdWithId: (NSString*) _id
    data: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPApp* data = [[HRPApp alloc] init]; // Model instance data (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance appReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPApp***](HRPApp*.md)| Model instance data | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appReplaceOrCreate**
```objc
-(NSNumber*) appReplaceOrCreateWithData: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPApp* data = [[HRPApp alloc] init]; // Model instance data (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance appReplaceOrCreateWithData:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPApp***](HRPApp*.md)| Model instance data | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appUpdateAll**
```objc
-(NSNumber*) appUpdateAllWithWhere: (NSString*) where
    data: (HRPApp*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPApp* data = [[HRPApp alloc] init]; // An object of model property name/value pairs (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance appUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPApp***](HRPApp*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appUploadFile**
```objc
-(NSNumber*) appUploadFileWithId: (NSString*) _id
    type: (NSString*) type
    data: (HRPMagentoFileUpload*) data
        completionHandler: (void (^)(HRPMagentoFileUpload* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Application Id
NSString* type = @"type_example"; // Corresponds to the type of file being uploaded (androidGoogleServices, androidKey, androidKeystore, androidApk, iOSGoogleServices, iOSPushNotificationPrivateKey, iOSPushNotificationCertificate, iOSPushNotificationPem, imgAppLogo, imgSplashScreen, imgMenuBackground, imgBrandPlaceholder, imgNavbarLogo,imgSponsorMenu, imgSponsorSplash, imgMenuItem, imgRadioStream, imgRadioShow, imgRadioPresenter).
HRPMagentoFileUpload* data = [[HRPMagentoFileUpload alloc] init]; // Corresponds to the file you're uploading formatted as an object. (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

[apiInstance appUploadFileWithId:_id
              type:type
              data:data
          completionHandler: ^(HRPMagentoFileUpload* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appUploadFile: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Application Id | 
 **type** | **NSString***| Corresponds to the type of file being uploaded (androidGoogleServices, androidKey, androidKeystore, androidApk, iOSGoogleServices, iOSPushNotificationPrivateKey, iOSPushNotificationCertificate, iOSPushNotificationPem, imgAppLogo, imgSplashScreen, imgMenuBackground, imgBrandPlaceholder, imgNavbarLogo,imgSponsorMenu, imgSponsorSplash, imgMenuItem, imgRadioStream, imgRadioShow, imgRadioPresenter). | 
 **data** | [**HRPMagentoFileUpload***](HRPMagentoFileUpload*.md)| Corresponds to the file you&#39;re uploading formatted as an object. | [optional] 

### Return type

[**HRPMagentoFileUpload***](HRPMagentoFileUpload.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appUpsertPatchApps**
```objc
-(NSNumber*) appUpsertPatchAppsWithData: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPApp* data = [[HRPApp alloc] init]; // Model instance data (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance appUpsertPatchAppsWithData:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appUpsertPatchApps: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPApp***](HRPApp*.md)| Model instance data | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appUpsertPutApps**
```objc
-(NSNumber*) appUpsertPutAppsWithData: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPApp* data = [[HRPApp alloc] init]; // Model instance data (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance appUpsertPutAppsWithData:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appUpsertPutApps: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPApp***](HRPApp*.md)| Model instance data | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appUpsertWithWhere**
```objc
-(NSNumber*) appUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPApp*) data
        completionHandler: (void (^)(HRPApp* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPApp* data = [[HRPApp alloc] init]; // An object of model property name/value pairs (optional)

HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance appUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPApp* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPApp***](HRPApp*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPApp***](HRPApp.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **appVerify**
```objc
-(NSNumber*) appVerifyWithCompletionHandler: 
        (void (^)(NSObject* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];



HRPAppApi*apiInstance = [[HRPAppApi alloc] init];

[apiInstance appVerifyWithCompletionHandler: 
          ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAppApi->appVerify: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

