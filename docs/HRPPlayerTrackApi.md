# HRPPlayerTrackApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playerTrackCount**](HRPPlayerTrackApi.md#playertrackcount) | **GET** /PlayerTracks/count | Count instances of the model matched by where from the data source.
[**playerTrackCreate**](HRPPlayerTrackApi.md#playertrackcreate) | **POST** /PlayerTracks | Create a new instance of the model and persist it into the data source.
[**playerTrackCreateChangeStreamGetPlayerTracksChangeStream**](HRPPlayerTrackApi.md#playertrackcreatechangestreamgetplayertrackschangestream) | **GET** /PlayerTracks/change-stream | Create a change stream.
[**playerTrackCreateChangeStreamPostPlayerTracksChangeStream**](HRPPlayerTrackApi.md#playertrackcreatechangestreampostplayertrackschangestream) | **POST** /PlayerTracks/change-stream | Create a change stream.
[**playerTrackDeleteById**](HRPPlayerTrackApi.md#playertrackdeletebyid) | **DELETE** /PlayerTracks/{id} | Delete a model instance by {{id}} from the data source.
[**playerTrackExistsGetPlayerTracksidExists**](HRPPlayerTrackApi.md#playertrackexistsgetplayertracksidexists) | **GET** /PlayerTracks/{id}/exists | Check whether a model instance exists in the data source.
[**playerTrackExistsHeadPlayerTracksid**](HRPPlayerTrackApi.md#playertrackexistsheadplayertracksid) | **HEAD** /PlayerTracks/{id} | Check whether a model instance exists in the data source.
[**playerTrackFind**](HRPPlayerTrackApi.md#playertrackfind) | **GET** /PlayerTracks | Find all instances of the model matched by filter from the data source.
[**playerTrackFindById**](HRPPlayerTrackApi.md#playertrackfindbyid) | **GET** /PlayerTracks/{id} | Find a model instance by {{id}} from the data source.
[**playerTrackFindOne**](HRPPlayerTrackApi.md#playertrackfindone) | **GET** /PlayerTracks/findOne | Find first instance of the model matched by filter from the data source.
[**playerTrackPrototypeGetPlaylistItem**](HRPPlayerTrackApi.md#playertrackprototypegetplaylistitem) | **GET** /PlayerTracks/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**playerTrackPrototypeUpdateAttributesPatchPlayerTracksid**](HRPPlayerTrackApi.md#playertrackprototypeupdateattributespatchplayertracksid) | **PATCH** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerTrackPrototypeUpdateAttributesPutPlayerTracksid**](HRPPlayerTrackApi.md#playertrackprototypeupdateattributesputplayertracksid) | **PUT** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerTrackReplaceById**](HRPPlayerTrackApi.md#playertrackreplacebyid) | **POST** /PlayerTracks/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playerTrackReplaceOrCreate**](HRPPlayerTrackApi.md#playertrackreplaceorcreate) | **POST** /PlayerTracks/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playerTrackUpdateAll**](HRPPlayerTrackApi.md#playertrackupdateall) | **POST** /PlayerTracks/update | Update instances of the model matched by {{where}} from the data source.
[**playerTrackUpsertPatchPlayerTracks**](HRPPlayerTrackApi.md#playertrackupsertpatchplayertracks) | **PATCH** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
[**playerTrackUpsertPutPlayerTracks**](HRPPlayerTrackApi.md#playertrackupsertputplayertracks) | **PUT** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
[**playerTrackUpsertWithWhere**](HRPPlayerTrackApi.md#playertrackupsertwithwhere) | **POST** /PlayerTracks/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **playerTrackCount**
```objc
-(NSNumber*) playerTrackCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance playerTrackCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackCreate**
```objc
-(NSNumber*) playerTrackCreateWithData: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; // Model instance data (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance playerTrackCreateWithData:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackCreateChangeStreamGetPlayerTracksChangeStream**
```objc
-(NSNumber*) playerTrackCreateChangeStreamGetPlayerTracksChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Create a change stream.
[apiInstance playerTrackCreateChangeStreamGetPlayerTracksChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackCreateChangeStreamGetPlayerTracksChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackCreateChangeStreamPostPlayerTracksChangeStream**
```objc
-(NSNumber*) playerTrackCreateChangeStreamPostPlayerTracksChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Create a change stream.
[apiInstance playerTrackCreateChangeStreamPostPlayerTracksChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackCreateChangeStreamPostPlayerTracksChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackDeleteById**
```objc
-(NSNumber*) playerTrackDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance playerTrackDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackExistsGetPlayerTracksidExists**
```objc
-(NSNumber*) playerTrackExistsGetPlayerTracksidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance playerTrackExistsGetPlayerTracksidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackExistsGetPlayerTracksidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackExistsHeadPlayerTracksid**
```objc
-(NSNumber*) playerTrackExistsHeadPlayerTracksidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance playerTrackExistsHeadPlayerTracksidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackExistsHeadPlayerTracksid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackFind**
```objc
-(NSNumber*) playerTrackFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPPlayerTrack>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance playerTrackFindWithFilter:filter
          completionHandler: ^(NSArray<HRPPlayerTrack>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPPlayerTrack>***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackFindById**
```objc
-(NSNumber*) playerTrackFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance playerTrackFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackFindOne**
```objc
-(NSNumber*) playerTrackFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance playerTrackFindOneWithFilter:filter
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackPrototypeGetPlaylistItem**
```objc
-(NSNumber*) playerTrackPrototypeGetPlaylistItemWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPPlaylistItem* output, NSError* error)) handler;
```

Fetches belongsTo relation playlistItem.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlayerTrack id
NSNumber* refresh = @true; //  (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Fetches belongsTo relation playlistItem.
[apiInstance playerTrackPrototypeGetPlaylistItemWithId:_id
              refresh:refresh
          completionHandler: ^(HRPPlaylistItem* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackPrototypeGetPlaylistItem: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlayerTrack id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPPlaylistItem***](HRPPlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackPrototypeUpdateAttributesPatchPlayerTracksid**
```objc
-(NSNumber*) playerTrackPrototypeUpdateAttributesPatchPlayerTracksidWithId: (NSString*) _id
    data: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlayerTrack id
HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; // An object of model property name/value pairs (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance playerTrackPrototypeUpdateAttributesPatchPlayerTracksidWithId:_id
              data:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackPrototypeUpdateAttributesPatchPlayerTracksid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlayerTrack id | 
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackPrototypeUpdateAttributesPutPlayerTracksid**
```objc
-(NSNumber*) playerTrackPrototypeUpdateAttributesPutPlayerTracksidWithId: (NSString*) _id
    data: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // PlayerTrack id
HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; // An object of model property name/value pairs (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance playerTrackPrototypeUpdateAttributesPutPlayerTracksidWithId:_id
              data:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackPrototypeUpdateAttributesPutPlayerTracksid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| PlayerTrack id | 
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackReplaceById**
```objc
-(NSNumber*) playerTrackReplaceByIdWithId: (NSString*) _id
    data: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; // Model instance data (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance playerTrackReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackReplaceOrCreate**
```objc
-(NSNumber*) playerTrackReplaceOrCreateWithData: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; // Model instance data (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance playerTrackReplaceOrCreateWithData:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackUpdateAll**
```objc
-(NSNumber*) playerTrackUpdateAllWithWhere: (NSString*) where
    data: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; // An object of model property name/value pairs (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance playerTrackUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackUpsertPatchPlayerTracks**
```objc
-(NSNumber*) playerTrackUpsertPatchPlayerTracksWithData: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; // Model instance data (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance playerTrackUpsertPatchPlayerTracksWithData:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackUpsertPatchPlayerTracks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackUpsertPutPlayerTracks**
```objc
-(NSNumber*) playerTrackUpsertPutPlayerTracksWithData: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; // Model instance data (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance playerTrackUpsertPutPlayerTracksWithData:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackUpsertPutPlayerTracks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)| Model instance data | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **playerTrackUpsertWithWhere**
```objc
-(NSNumber*) playerTrackUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPPlayerTrack*) data
        completionHandler: (void (^)(HRPPlayerTrack* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPPlayerTrack* data = [[HRPPlayerTrack alloc] init]; // An object of model property name/value pairs (optional)

HRPPlayerTrackApi*apiInstance = [[HRPPlayerTrackApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance playerTrackUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPPlayerTrack* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPPlayerTrackApi->playerTrackUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPPlayerTrack***](HRPPlayerTrack*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPPlayerTrack***](HRPPlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

