# HRPNotificationFrom

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**HRPCustomer***](HRPCustomer.md) |  | [optional] 
**brand** | [**HRPBrand***](HRPBrand.md) |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


