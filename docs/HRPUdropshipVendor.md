# HRPUdropshipVendor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**vendorName** | **NSString*** |  | 
**vendorAttn** | **NSString*** |  | 
**email** | **NSString*** |  | 
**street** | **NSString*** |  | 
**city** | **NSString*** |  | 
**zip** | **NSString*** |  | [optional] 
**countryId** | **NSString*** |  | 
**regionId** | **NSNumber*** |  | [optional] 
**region** | **NSString*** |  | [optional] 
**telephone** | **NSString*** |  | [optional] 
**fax** | **NSString*** |  | [optional] 
**status** | **NSNumber*** |  | 
**password** | **NSString*** |  | [optional] 
**passwordHash** | **NSString*** |  | [optional] 
**passwordEnc** | **NSString*** |  | [optional] 
**carrierCode** | **NSString*** |  | [optional] 
**notifyNewOrder** | **NSNumber*** |  | 
**labelType** | **NSString*** |  | 
**testMode** | **NSNumber*** |  | 
**handlingFee** | **NSString*** |  | 
**upsShipperNumber** | **NSString*** |  | [optional] 
**customDataCombined** | **NSString*** |  | [optional] 
**customVarsCombined** | **NSString*** |  | [optional] 
**emailTemplate** | **NSNumber*** |  | [optional] 
**urlKey** | **NSString*** |  | [optional] 
**randomHash** | **NSString*** |  | [optional] 
**createdAt** | **NSDate*** |  | [optional] 
**notifyLowstock** | **NSNumber*** |  | [optional] 
**notifyLowstockQty** | **NSString*** |  | [optional] 
**useHandlingFee** | **NSNumber*** |  | [optional] 
**useRatesFallback** | **NSNumber*** |  | [optional] 
**allowShippingExtraCharge** | **NSNumber*** |  | [optional] 
**defaultShippingExtraChargeSuffix** | **NSString*** |  | [optional] 
**defaultShippingExtraChargeType** | **NSString*** |  | [optional] 
**defaultShippingExtraCharge** | **NSString*** |  | [optional] 
**isExtraChargeShippingDefault** | **NSNumber*** |  | [optional] 
**defaultShippingId** | **NSNumber*** |  | [optional] 
**billingUseShipping** | **NSNumber*** |  | 
**billingEmail** | **NSString*** |  | 
**billingTelephone** | **NSString*** |  | [optional] 
**billingFax** | **NSString*** |  | [optional] 
**billingVendorAttn** | **NSString*** |  | 
**billingStreet** | **NSString*** |  | 
**billingCity** | **NSString*** |  | 
**billingZip** | **NSString*** |  | [optional] 
**billingCountryId** | **NSString*** |  | 
**billingRegionId** | **NSNumber*** |  | [optional] 
**billingRegion** | **NSString*** |  | [optional] 
**subdomainLevel** | **NSNumber*** |  | 
**updateStoreBaseUrl** | **NSNumber*** |  | 
**confirmation** | **NSString*** |  | [optional] 
**confirmationSent** | **NSNumber*** |  | [optional] 
**rejectReason** | **NSString*** |  | [optional] 
**backorderByAvailability** | **NSNumber*** |  | [optional] 
**useReservedQty** | **NSNumber*** |  | [optional] 
**tiercomRates** | **NSString*** |  | [optional] 
**tiercomFixedRule** | **NSString*** |  | [optional] 
**tiercomFixedRates** | **NSString*** |  | [optional] 
**tiercomFixedCalcType** | **NSString*** |  | [optional] 
**tiershipRates** | **NSString*** |  | [optional] 
**tiershipSimpleRates** | **NSString*** |  | [optional] 
**tiershipUseV2Rates** | **NSNumber*** |  | [optional] 
**vacationMode** | **NSNumber*** |  | [optional] 
**vacationEnd** | **NSDate*** |  | [optional] 
**vacationMessage** | **NSString*** |  | [optional] 
**udmemberLimitProducts** | **NSNumber*** |  | [optional] 
**udmemberProfileId** | **NSNumber*** |  | [optional] 
**udmemberProfileRefid** | **NSString*** |  | [optional] 
**udmemberMembershipCode** | **NSString*** |  | [optional] 
**udmemberMembershipTitle** | **NSString*** |  | [optional] 
**udmemberBillingType** | **NSString*** |  | [optional] 
**udmemberHistory** | **NSString*** |  | [optional] 
**udmemberProfileSyncOff** | **NSNumber*** |  | [optional] 
**udmemberAllowMicrosite** | **NSNumber*** |  | [optional] 
**udprodTemplateSku** | **NSString*** |  | [optional] 
**vendorTaxClass** | **NSString*** |  | [optional] 
**catalogProducts** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**vendorLocations** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**config** | **NSObject*** |  | [optional] 
**partners** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**udropshipVendorProducts** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**harpoonHpublicApplicationpartners** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**harpoonHpublicv12VendorAppCategories** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


