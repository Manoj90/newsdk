# HRPEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basePrice** | **NSNumber*** |  | [optional] [default to @0.0]
**attendees** | [**NSArray&lt;HRPEventAttendee&gt;***](HRPEventAttendee.md) |  | [optional] 
**attendeeCount** | **NSNumber*** |  | [optional] [default to @0.0]
**isGoing** | **NSNumber*** |  | [optional] [default to @0]
**tickets** | [**NSArray&lt;HRPEventTicket&gt;***](HRPEventTicket.md) |  | [optional] 
**termsConditions** | **NSString*** |  | [optional] 
**facebook** | [**HRPFacebookEvent***](HRPFacebookEvent.md) |  | [optional] 
**name** | **NSString*** |  | [optional] 
**_description** | **NSString*** |  | [optional] 
**cover** | **NSString*** |  | [optional] 
**campaignType** | [**HRPCategory***](HRPCategory.md) |  | [optional] 
**category** | [**HRPCategory***](HRPCategory.md) |  | [optional] 
**topic** | [**HRPCategory***](HRPCategory.md) |  | [optional] 
**alias** | **NSString*** |  | [optional] 
**from** | **NSDate*** |  | [optional] 
**to** | **NSDate*** |  | [optional] 
**baseCurrency** | **NSString*** |  | [optional] [default to @"EUR"]
**priceText** | **NSString*** |  | [optional] 
**bannerText** | **NSString*** |  | [optional] 
**checkoutLink** | **NSString*** |  | [optional] 
**nearestVenue** | [**HRPVenue***](HRPVenue.md) |  | [optional] 
**actionText** | **NSString*** |  | [optional] [default to @"Sold Out"]
**status** | **NSString*** |  | [optional] [default to @"soldOut"]
**collectionNotes** | **NSString*** |  | [optional] 
**locationLink** | **NSString*** |  | [optional] 
**altLink** | **NSString*** |  | [optional] 
**redemptionType** | **NSString*** |  | [optional] 
**brand** | [**HRPBrand***](HRPBrand.md) |  | [optional] 
**closestPurchase** | [**HRPOfferClosestPurchase***](HRPOfferClosestPurchase.md) |  | [optional] 
**isFeatured** | **NSNumber*** |  | [optional] [default to @0]
**qtyPerOrder** | **NSNumber*** |  | [optional] [default to @1.0]
**shareLink** | **NSString*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


