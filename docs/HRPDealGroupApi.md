# HRPDealGroupApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dealGroupCheckout**](HRPDealGroupApi.md#dealgroupcheckout) | **POST** /DealGroups/{id}/checkout | 
[**dealGroupFind**](HRPDealGroupApi.md#dealgroupfind) | **GET** /DealGroups | Find all instances of the model matched by filter from the data source.
[**dealGroupFindById**](HRPDealGroupApi.md#dealgroupfindbyid) | **GET** /DealGroups/{id} | Find a model instance by {{id}} from the data source.
[**dealGroupFindOne**](HRPDealGroupApi.md#dealgroupfindone) | **GET** /DealGroups/findOne | Find first instance of the model matched by filter from the data source.
[**dealGroupRedeem**](HRPDealGroupApi.md#dealgroupredeem) | **POST** /DealGroups/{id}/redeem | 
[**dealGroupReplaceById**](HRPDealGroupApi.md#dealgroupreplacebyid) | **POST** /DealGroups/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**dealGroupReplaceOrCreate**](HRPDealGroupApi.md#dealgroupreplaceorcreate) | **POST** /DealGroups/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**dealGroupUpsertWithWhere**](HRPDealGroupApi.md#dealgroupupsertwithwhere) | **POST** /DealGroups/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**dealGroupVenues**](HRPDealGroupApi.md#dealgroupvenues) | **GET** /DealGroups/{id}/venues | 


# **dealGroupCheckout**
```objc
-(NSNumber*) dealGroupCheckoutWithId: (NSString*) _id
    data: (HRPDealGroupCheckoutData*) data
        completionHandler: (void (^)(HRPDealGroupCheckout* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPDealGroupCheckoutData* data = [[HRPDealGroupCheckoutData alloc] init]; //  (optional)

HRPDealGroupApi*apiInstance = [[HRPDealGroupApi alloc] init];

[apiInstance dealGroupCheckoutWithId:_id
              data:data
          completionHandler: ^(HRPDealGroupCheckout* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealGroupApi->dealGroupCheckout: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPDealGroupCheckoutData***](HRPDealGroupCheckoutData*.md)|  | [optional] 

### Return type

[**HRPDealGroupCheckout***](HRPDealGroupCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealGroupFind**
```objc
-(NSNumber*) dealGroupFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPDealGroup>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPDealGroupApi*apiInstance = [[HRPDealGroupApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance dealGroupFindWithFilter:filter
          completionHandler: ^(NSArray<HRPDealGroup>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealGroupApi->dealGroupFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPDealGroup>***](HRPDealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealGroupFindById**
```objc
-(NSNumber*) dealGroupFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPDealGroup* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPDealGroupApi*apiInstance = [[HRPDealGroupApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance dealGroupFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPDealGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealGroupApi->dealGroupFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPDealGroup***](HRPDealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealGroupFindOne**
```objc
-(NSNumber*) dealGroupFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPDealGroup* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPDealGroupApi*apiInstance = [[HRPDealGroupApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance dealGroupFindOneWithFilter:filter
          completionHandler: ^(HRPDealGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealGroupApi->dealGroupFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPDealGroup***](HRPDealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealGroupRedeem**
```objc
-(NSNumber*) dealGroupRedeemWithId: (NSString*) _id
    data: (HRPDealGroupRedeemData*) data
        completionHandler: (void (^)(HRPDealGroupPurchase* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPDealGroupRedeemData* data = [[HRPDealGroupRedeemData alloc] init]; //  (optional)

HRPDealGroupApi*apiInstance = [[HRPDealGroupApi alloc] init];

[apiInstance dealGroupRedeemWithId:_id
              data:data
          completionHandler: ^(HRPDealGroupPurchase* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealGroupApi->dealGroupRedeem: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPDealGroupRedeemData***](HRPDealGroupRedeemData*.md)|  | [optional] 

### Return type

[**HRPDealGroupPurchase***](HRPDealGroupPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealGroupReplaceById**
```objc
-(NSNumber*) dealGroupReplaceByIdWithId: (NSString*) _id
    data: (HRPDealGroup*) data
        completionHandler: (void (^)(HRPDealGroup* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPDealGroup* data = [[HRPDealGroup alloc] init]; // Model instance data (optional)

HRPDealGroupApi*apiInstance = [[HRPDealGroupApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance dealGroupReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPDealGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealGroupApi->dealGroupReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPDealGroup***](HRPDealGroup*.md)| Model instance data | [optional] 

### Return type

[**HRPDealGroup***](HRPDealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealGroupReplaceOrCreate**
```objc
-(NSNumber*) dealGroupReplaceOrCreateWithData: (HRPDealGroup*) data
        completionHandler: (void (^)(HRPDealGroup* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPDealGroup* data = [[HRPDealGroup alloc] init]; // Model instance data (optional)

HRPDealGroupApi*apiInstance = [[HRPDealGroupApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance dealGroupReplaceOrCreateWithData:data
          completionHandler: ^(HRPDealGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealGroupApi->dealGroupReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPDealGroup***](HRPDealGroup*.md)| Model instance data | [optional] 

### Return type

[**HRPDealGroup***](HRPDealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealGroupUpsertWithWhere**
```objc
-(NSNumber*) dealGroupUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPDealGroup*) data
        completionHandler: (void (^)(HRPDealGroup* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPDealGroup* data = [[HRPDealGroup alloc] init]; // An object of model property name/value pairs (optional)

HRPDealGroupApi*apiInstance = [[HRPDealGroupApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance dealGroupUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPDealGroup* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealGroupApi->dealGroupUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPDealGroup***](HRPDealGroup*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPDealGroup***](HRPDealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **dealGroupVenues**
```objc
-(NSNumber*) dealGroupVenuesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPVenue>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPDealGroupApi*apiInstance = [[HRPDealGroupApi alloc] init];

[apiInstance dealGroupVenuesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPVenue>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPDealGroupApi->dealGroupVenues: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPVenue>***](HRPVenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

