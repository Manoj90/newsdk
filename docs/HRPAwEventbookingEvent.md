# HRPAwEventbookingEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSNumber*** |  | 
**productId** | **NSNumber*** |  | [optional] 
**isEnabled** | **NSNumber*** |  | [optional] 
**eventStartDate** | **NSDate*** |  | [optional] 
**eventEndDate** | **NSDate*** |  | [optional] 
**dayCountBeforeSendReminderLetter** | **NSNumber*** |  | [optional] 
**isReminderSend** | **NSNumber*** |  | 
**isTermsEnabled** | **NSNumber*** |  | [optional] 
**generatePdfTickets** | **NSNumber*** |  | 
**redeemRoles** | **NSString*** |  | [optional] 
**location** | **NSString*** |  | [optional] 
**tickets** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**attributes** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 
**purchasedTickets** | **NSArray&lt;NSObject*&gt;*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


