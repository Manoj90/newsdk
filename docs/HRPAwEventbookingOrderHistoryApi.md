# HRPAwEventbookingOrderHistoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingOrderHistoryCount**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistorycount) | **GET** /AwEventbookingOrderHistories/count | Count instances of the model matched by where from the data source.
[**awEventbookingOrderHistoryCreate**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistorycreate) | **POST** /AwEventbookingOrderHistories | Create a new instance of the model and persist it into the data source.
[**awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistorycreatechangestreamgetaweventbookingorderhistorieschangestream) | **GET** /AwEventbookingOrderHistories/change-stream | Create a change stream.
[**awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistorycreatechangestreampostaweventbookingorderhistorieschangestream) | **POST** /AwEventbookingOrderHistories/change-stream | Create a change stream.
[**awEventbookingOrderHistoryDeleteById**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistorydeletebyid) | **DELETE** /AwEventbookingOrderHistories/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryexistsgetaweventbookingorderhistoriesidexists) | **GET** /AwEventbookingOrderHistories/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryexistsheadaweventbookingorderhistoriesid) | **HEAD** /AwEventbookingOrderHistories/{id} | Check whether a model instance exists in the data source.
[**awEventbookingOrderHistoryFind**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryfind) | **GET** /AwEventbookingOrderHistories | Find all instances of the model matched by filter from the data source.
[**awEventbookingOrderHistoryFindById**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryfindbyid) | **GET** /AwEventbookingOrderHistories/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingOrderHistoryFindOne**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryfindone) | **GET** /AwEventbookingOrderHistories/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryprototypeupdateattributespatchaweventbookingorderhistoriesid) | **PATCH** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryprototypeupdateattributesputaweventbookingorderhistoriesid) | **PUT** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryReplaceById**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryreplacebyid) | **POST** /AwEventbookingOrderHistories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryReplaceOrCreate**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryreplaceorcreate) | **POST** /AwEventbookingOrderHistories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpdateAll**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryupdateall) | **POST** /AwEventbookingOrderHistories/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryupsertpatchaweventbookingorderhistories) | **PATCH** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryupsertputaweventbookingorderhistories) | **PUT** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpsertWithWhere**](HRPAwEventbookingOrderHistoryApi.md#aweventbookingorderhistoryupsertwithwhere) | **POST** /AwEventbookingOrderHistories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awEventbookingOrderHistoryCount**
```objc
-(NSNumber*) awEventbookingOrderHistoryCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance awEventbookingOrderHistoryCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryCreate**
```objc
-(NSNumber*) awEventbookingOrderHistoryCreateWithData: (HRPAwEventbookingOrderHistory*) data
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingOrderHistory* data = [[HRPAwEventbookingOrderHistory alloc] init]; // Model instance data (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance awEventbookingOrderHistoryCreateWithData:data
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream**
```objc
-(NSNumber*) awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Create a change stream.
[apiInstance awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream**
```objc
-(NSNumber*) awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Create a change stream.
[apiInstance awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryDeleteById**
```objc
-(NSNumber*) awEventbookingOrderHistoryDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance awEventbookingOrderHistoryDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists**
```objc
-(NSNumber*) awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid**
```objc
-(NSNumber*) awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryFind**
```objc
-(NSNumber*) awEventbookingOrderHistoryFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingOrderHistory>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance awEventbookingOrderHistoryFindWithFilter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingOrderHistory>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPAwEventbookingOrderHistory>***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryFindById**
```objc
-(NSNumber*) awEventbookingOrderHistoryFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance awEventbookingOrderHistoryFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryFindOne**
```objc
-(NSNumber*) awEventbookingOrderHistoryFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance awEventbookingOrderHistoryFindOneWithFilter:filter
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid**
```objc
-(NSNumber*) awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesidWithId: (NSString*) _id
    data: (HRPAwEventbookingOrderHistory*) data
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingOrderHistory id
HRPAwEventbookingOrderHistory* data = [[HRPAwEventbookingOrderHistory alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesidWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingOrderHistory id | 
 **data** | [**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid**
```objc
-(NSNumber*) awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesidWithId: (NSString*) _id
    data: (HRPAwEventbookingOrderHistory*) data
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingOrderHistory id
HRPAwEventbookingOrderHistory* data = [[HRPAwEventbookingOrderHistory alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesidWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingOrderHistory id | 
 **data** | [**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryReplaceById**
```objc
-(NSNumber*) awEventbookingOrderHistoryReplaceByIdWithId: (NSString*) _id
    data: (HRPAwEventbookingOrderHistory*) data
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPAwEventbookingOrderHistory* data = [[HRPAwEventbookingOrderHistory alloc] init]; // Model instance data (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingOrderHistoryReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryReplaceOrCreate**
```objc
-(NSNumber*) awEventbookingOrderHistoryReplaceOrCreateWithData: (HRPAwEventbookingOrderHistory*) data
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingOrderHistory* data = [[HRPAwEventbookingOrderHistory alloc] init]; // Model instance data (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingOrderHistoryReplaceOrCreateWithData:data
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryUpdateAll**
```objc
-(NSNumber*) awEventbookingOrderHistoryUpdateAllWithWhere: (NSString*) where
    data: (HRPAwEventbookingOrderHistory*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwEventbookingOrderHistory* data = [[HRPAwEventbookingOrderHistory alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance awEventbookingOrderHistoryUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories**
```objc
-(NSNumber*) awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistoriesWithData: (HRPAwEventbookingOrderHistory*) data
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingOrderHistory* data = [[HRPAwEventbookingOrderHistory alloc] init]; // Model instance data (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistoriesWithData:data
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories**
```objc
-(NSNumber*) awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistoriesWithData: (HRPAwEventbookingOrderHistory*) data
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingOrderHistory* data = [[HRPAwEventbookingOrderHistory alloc] init]; // Model instance data (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistoriesWithData:data
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingOrderHistoryUpsertWithWhere**
```objc
-(NSNumber*) awEventbookingOrderHistoryUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPAwEventbookingOrderHistory*) data
        completionHandler: (void (^)(HRPAwEventbookingOrderHistory* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwEventbookingOrderHistory* data = [[HRPAwEventbookingOrderHistory alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingOrderHistoryApi*apiInstance = [[HRPAwEventbookingOrderHistoryApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance awEventbookingOrderHistoryUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPAwEventbookingOrderHistory* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingOrderHistoryApi->awEventbookingOrderHistoryUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingOrderHistory***](HRPAwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

