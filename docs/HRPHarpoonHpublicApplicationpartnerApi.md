# HRPHarpoonHpublicApplicationpartnerApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**harpoonHpublicApplicationpartnerCount**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnercount) | **GET** /HarpoonHpublicApplicationpartners/count | Count instances of the model matched by where from the data source.
[**harpoonHpublicApplicationpartnerCreate**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnercreate) | **POST** /HarpoonHpublicApplicationpartners | Create a new instance of the model and persist it into the data source.
[**harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnercreatechangestreamgetharpoonhpublicapplicationpartnerschangestream) | **GET** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
[**harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnercreatechangestreampostharpoonhpublicapplicationpartnerschangestream) | **POST** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
[**harpoonHpublicApplicationpartnerDeleteById**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerdeletebyid) | **DELETE** /HarpoonHpublicApplicationpartners/{id} | Delete a model instance by {{id}} from the data source.
[**harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerexistsgetharpoonhpublicapplicationpartnersidexists) | **GET** /HarpoonHpublicApplicationpartners/{id}/exists | Check whether a model instance exists in the data source.
[**harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerexistsheadharpoonhpublicapplicationpartnersid) | **HEAD** /HarpoonHpublicApplicationpartners/{id} | Check whether a model instance exists in the data source.
[**harpoonHpublicApplicationpartnerFind**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerfind) | **GET** /HarpoonHpublicApplicationpartners | Find all instances of the model matched by filter from the data source.
[**harpoonHpublicApplicationpartnerFindById**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerfindbyid) | **GET** /HarpoonHpublicApplicationpartners/{id} | Find a model instance by {{id}} from the data source.
[**harpoonHpublicApplicationpartnerFindOne**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerfindone) | **GET** /HarpoonHpublicApplicationpartners/findOne | Find first instance of the model matched by filter from the data source.
[**harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerprototypegetudropshipvendor) | **GET** /HarpoonHpublicApplicationpartners/{id}/udropshipVendor | Fetches belongsTo relation udropshipVendor.
[**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerprototypeupdateattributespatchharpoonhpublicapplicationpartnersid) | **PATCH** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerprototypeupdateattributesputharpoonhpublicapplicationpartnersid) | **PUT** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerReplaceById**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerreplacebyid) | **POST** /HarpoonHpublicApplicationpartners/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerReplaceOrCreate**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerreplaceorcreate) | **POST** /HarpoonHpublicApplicationpartners/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpdateAll**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerupdateall) | **POST** /HarpoonHpublicApplicationpartners/update | Update instances of the model matched by {{where}} from the data source.
[**harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerupsertpatchharpoonhpublicapplicationpartners) | **PATCH** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerupsertputharpoonhpublicapplicationpartners) | **PUT** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpsertWithWhere**](HRPHarpoonHpublicApplicationpartnerApi.md#harpoonhpublicapplicationpartnerupsertwithwhere) | **POST** /HarpoonHpublicApplicationpartners/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **harpoonHpublicApplicationpartnerCount**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance harpoonHpublicApplicationpartnerCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerCreate**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerCreateWithData: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; // Model instance data (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance harpoonHpublicApplicationpartnerCreateWithData:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)| Model instance data | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Create a change stream.
[apiInstance harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Create a change stream.
[apiInstance harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerDeleteById**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance harpoonHpublicApplicationpartnerDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerFind**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPHarpoonHpublicApplicationpartner>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance harpoonHpublicApplicationpartnerFindWithFilter:filter
          completionHandler: ^(NSArray<HRPHarpoonHpublicApplicationpartner>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPHarpoonHpublicApplicationpartner>***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerFindById**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance harpoonHpublicApplicationpartnerFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerFindOne**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance harpoonHpublicApplicationpartnerFindOneWithFilter:filter
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendorWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPUdropshipVendor* output, NSError* error)) handler;
```

Fetches belongsTo relation udropshipVendor.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // HarpoonHpublicApplicationpartner id
NSNumber* refresh = @true; //  (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Fetches belongsTo relation udropshipVendor.
[apiInstance harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendorWithId:_id
              refresh:refresh
          completionHandler: ^(HRPUdropshipVendor* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| HarpoonHpublicApplicationpartner id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPUdropshipVendor***](HRPUdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersidWithId: (NSString*) _id
    data: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // HarpoonHpublicApplicationpartner id
HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; // An object of model property name/value pairs (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersidWithId:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| HarpoonHpublicApplicationpartner id | 
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersidWithId: (NSString*) _id
    data: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // HarpoonHpublicApplicationpartner id
HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; // An object of model property name/value pairs (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersidWithId:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| HarpoonHpublicApplicationpartner id | 
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerReplaceById**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerReplaceByIdWithId: (NSString*) _id
    data: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; // Model instance data (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance harpoonHpublicApplicationpartnerReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)| Model instance data | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerReplaceOrCreate**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerReplaceOrCreateWithData: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; // Model instance data (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance harpoonHpublicApplicationpartnerReplaceOrCreateWithData:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)| Model instance data | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerUpdateAll**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerUpdateAllWithWhere: (NSString*) where
    data: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; // An object of model property name/value pairs (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance harpoonHpublicApplicationpartnerUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartnersWithData: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; // Model instance data (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartnersWithData:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)| Model instance data | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartnersWithData: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; // Model instance data (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartnersWithData:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)| Model instance data | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **harpoonHpublicApplicationpartnerUpsertWithWhere**
```objc
-(NSNumber*) harpoonHpublicApplicationpartnerUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPHarpoonHpublicApplicationpartner*) data
        completionHandler: (void (^)(HRPHarpoonHpublicApplicationpartner* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPHarpoonHpublicApplicationpartner* data = [[HRPHarpoonHpublicApplicationpartner alloc] init]; // An object of model property name/value pairs (optional)

HRPHarpoonHpublicApplicationpartnerApi*apiInstance = [[HRPHarpoonHpublicApplicationpartnerApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance harpoonHpublicApplicationpartnerUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPHarpoonHpublicApplicationpartner* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPHarpoonHpublicApplicationpartnerApi->harpoonHpublicApplicationpartnerUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPHarpoonHpublicApplicationpartner***](HRPHarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

