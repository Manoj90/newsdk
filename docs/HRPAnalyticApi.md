# HRPAnalyticApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**analyticReplaceById**](HRPAnalyticApi.md#analyticreplacebyid) | **POST** /Analytics/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**analyticReplaceOrCreate**](HRPAnalyticApi.md#analyticreplaceorcreate) | **POST** /Analytics/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**analyticTrack**](HRPAnalyticApi.md#analytictrack) | **POST** /Analytics/track | 
[**analyticUpsertWithWhere**](HRPAnalyticApi.md#analyticupsertwithwhere) | **POST** /Analytics/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **analyticReplaceById**
```objc
-(NSNumber*) analyticReplaceByIdWithId: (NSString*) _id
    data: (HRPAnalytic*) data
        completionHandler: (void (^)(HRPAnalytic* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPAnalytic* data = [[HRPAnalytic alloc] init]; // Model instance data (optional)

HRPAnalyticApi*apiInstance = [[HRPAnalyticApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance analyticReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPAnalytic* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAnalyticApi->analyticReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPAnalytic***](HRPAnalytic*.md)| Model instance data | [optional] 

### Return type

[**HRPAnalytic***](HRPAnalytic.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **analyticReplaceOrCreate**
```objc
-(NSNumber*) analyticReplaceOrCreateWithData: (HRPAnalytic*) data
        completionHandler: (void (^)(HRPAnalytic* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAnalytic* data = [[HRPAnalytic alloc] init]; // Model instance data (optional)

HRPAnalyticApi*apiInstance = [[HRPAnalyticApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance analyticReplaceOrCreateWithData:data
          completionHandler: ^(HRPAnalytic* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAnalyticApi->analyticReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAnalytic***](HRPAnalytic*.md)| Model instance data | [optional] 

### Return type

[**HRPAnalytic***](HRPAnalytic.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **analyticTrack**
```objc
-(NSNumber*) analyticTrackWithData: (HRPAnalyticRequest*) data
        completionHandler: (void (^)(HRPInlineResponse200* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAnalyticRequest* data = [[HRPAnalyticRequest alloc] init]; //  (optional)

HRPAnalyticApi*apiInstance = [[HRPAnalyticApi alloc] init];

[apiInstance analyticTrackWithData:data
          completionHandler: ^(HRPInlineResponse200* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAnalyticApi->analyticTrack: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAnalyticRequest***](HRPAnalyticRequest*.md)|  | [optional] 

### Return type

[**HRPInlineResponse200***](HRPInlineResponse200.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **analyticUpsertWithWhere**
```objc
-(NSNumber*) analyticUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPAnalytic*) data
        completionHandler: (void (^)(HRPAnalytic* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAnalytic* data = [[HRPAnalytic alloc] init]; // An object of model property name/value pairs (optional)

HRPAnalyticApi*apiInstance = [[HRPAnalyticApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance analyticUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPAnalytic* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAnalyticApi->analyticUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAnalytic***](HRPAnalytic*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAnalytic***](HRPAnalytic.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

