# HRPCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_id** | **NSString*** |  | [optional] 
**name** | **NSString*** |  | 
**type** | **NSString*** |  | [optional] 
**funding** | **NSString*** |  | [optional] 
**lastDigits** | **NSString*** |  | [optional] 
**exMonth** | **NSString*** |  | 
**exYear** | **NSString*** |  | 
**cardholderName** | **NSString*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


