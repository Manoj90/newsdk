# HRPLoginExternal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **NSString*** |  | 
**version** | **NSString*** |  | [optional] 
**requestUrl** | **NSString*** |  | 
**authorizeUrl** | **NSString*** |  | 
**accessUrl** | **NSString*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


