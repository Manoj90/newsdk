# HRPEventTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | [optional] 
**price** | **NSNumber*** |  | [optional] [default to @0.0]
**qtyBought** | **NSNumber*** |  | [optional] [default to @0.0]
**qtyTotal** | **NSNumber*** |  | [optional] [default to @0.0]
**qtyLeft** | **NSNumber*** |  | [optional] [default to @0.0]
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


