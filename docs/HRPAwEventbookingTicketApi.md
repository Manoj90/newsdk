# HRPAwEventbookingTicketApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingTicketCount**](HRPAwEventbookingTicketApi.md#aweventbookingticketcount) | **GET** /AwEventbookingTickets/count | Count instances of the model matched by where from the data source.
[**awEventbookingTicketCreate**](HRPAwEventbookingTicketApi.md#aweventbookingticketcreate) | **POST** /AwEventbookingTickets | Create a new instance of the model and persist it into the data source.
[**awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream**](HRPAwEventbookingTicketApi.md#aweventbookingticketcreatechangestreamgetaweventbookingticketschangestream) | **GET** /AwEventbookingTickets/change-stream | Create a change stream.
[**awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream**](HRPAwEventbookingTicketApi.md#aweventbookingticketcreatechangestreampostaweventbookingticketschangestream) | **POST** /AwEventbookingTickets/change-stream | Create a change stream.
[**awEventbookingTicketDeleteById**](HRPAwEventbookingTicketApi.md#aweventbookingticketdeletebyid) | **DELETE** /AwEventbookingTickets/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingTicketExistsGetAwEventbookingTicketsidExists**](HRPAwEventbookingTicketApi.md#aweventbookingticketexistsgetaweventbookingticketsidexists) | **GET** /AwEventbookingTickets/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingTicketExistsHeadAwEventbookingTicketsid**](HRPAwEventbookingTicketApi.md#aweventbookingticketexistsheadaweventbookingticketsid) | **HEAD** /AwEventbookingTickets/{id} | Check whether a model instance exists in the data source.
[**awEventbookingTicketFind**](HRPAwEventbookingTicketApi.md#aweventbookingticketfind) | **GET** /AwEventbookingTickets | Find all instances of the model matched by filter from the data source.
[**awEventbookingTicketFindById**](HRPAwEventbookingTicketApi.md#aweventbookingticketfindbyid) | **GET** /AwEventbookingTickets/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingTicketFindOne**](HRPAwEventbookingTicketApi.md#aweventbookingticketfindone) | **GET** /AwEventbookingTickets/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid**](HRPAwEventbookingTicketApi.md#aweventbookingticketprototypeupdateattributespatchaweventbookingticketsid) | **PATCH** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid**](HRPAwEventbookingTicketApi.md#aweventbookingticketprototypeupdateattributesputaweventbookingticketsid) | **PUT** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingTicketReplaceById**](HRPAwEventbookingTicketApi.md#aweventbookingticketreplacebyid) | **POST** /AwEventbookingTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingTicketReplaceOrCreate**](HRPAwEventbookingTicketApi.md#aweventbookingticketreplaceorcreate) | **POST** /AwEventbookingTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpdateAll**](HRPAwEventbookingTicketApi.md#aweventbookingticketupdateall) | **POST** /AwEventbookingTickets/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingTicketUpsertPatchAwEventbookingTickets**](HRPAwEventbookingTicketApi.md#aweventbookingticketupsertpatchaweventbookingtickets) | **PATCH** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpsertPutAwEventbookingTickets**](HRPAwEventbookingTicketApi.md#aweventbookingticketupsertputaweventbookingtickets) | **PUT** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpsertWithWhere**](HRPAwEventbookingTicketApi.md#aweventbookingticketupsertwithwhere) | **POST** /AwEventbookingTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awEventbookingTicketCount**
```objc
-(NSNumber*) awEventbookingTicketCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance awEventbookingTicketCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketCreate**
```objc
-(NSNumber*) awEventbookingTicketCreateWithData: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance awEventbookingTicketCreateWithData:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream**
```objc
-(NSNumber*) awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Create a change stream.
[apiInstance awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream**
```objc
-(NSNumber*) awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Create a change stream.
[apiInstance awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketDeleteById**
```objc
-(NSNumber*) awEventbookingTicketDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance awEventbookingTicketDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketExistsGetAwEventbookingTicketsidExists**
```objc
-(NSNumber*) awEventbookingTicketExistsGetAwEventbookingTicketsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awEventbookingTicketExistsGetAwEventbookingTicketsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketExistsGetAwEventbookingTicketsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketExistsHeadAwEventbookingTicketsid**
```objc
-(NSNumber*) awEventbookingTicketExistsHeadAwEventbookingTicketsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awEventbookingTicketExistsHeadAwEventbookingTicketsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketExistsHeadAwEventbookingTicketsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketFind**
```objc
-(NSNumber*) awEventbookingTicketFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwEventbookingTicket>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance awEventbookingTicketFindWithFilter:filter
          completionHandler: ^(NSArray<HRPAwEventbookingTicket>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPAwEventbookingTicket>***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketFindById**
```objc
-(NSNumber*) awEventbookingTicketFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance awEventbookingTicketFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketFindOne**
```objc
-(NSNumber*) awEventbookingTicketFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance awEventbookingTicketFindOneWithFilter:filter
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid**
```objc
-(NSNumber*) awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsidWithId: (NSString*) _id
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingTicket id
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsidWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingTicket id | 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid**
```objc
-(NSNumber*) awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsidWithId: (NSString*) _id
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwEventbookingTicket id
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsidWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwEventbookingTicket id | 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketReplaceById**
```objc
-(NSNumber*) awEventbookingTicketReplaceByIdWithId: (NSString*) _id
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance awEventbookingTicketReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketReplaceOrCreate**
```objc
-(NSNumber*) awEventbookingTicketReplaceOrCreateWithData: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingTicketReplaceOrCreateWithData:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketUpdateAll**
```objc
-(NSNumber*) awEventbookingTicketUpdateAllWithWhere: (NSString*) where
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance awEventbookingTicketUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketUpsertPatchAwEventbookingTickets**
```objc
-(NSNumber*) awEventbookingTicketUpsertPatchAwEventbookingTicketsWithData: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingTicketUpsertPatchAwEventbookingTicketsWithData:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketUpsertPatchAwEventbookingTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketUpsertPutAwEventbookingTickets**
```objc
-(NSNumber*) awEventbookingTicketUpsertPutAwEventbookingTicketsWithData: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; // Model instance data (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awEventbookingTicketUpsertPutAwEventbookingTicketsWithData:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketUpsertPutAwEventbookingTickets: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)| Model instance data | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awEventbookingTicketUpsertWithWhere**
```objc
-(NSNumber*) awEventbookingTicketUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPAwEventbookingTicket*) data
        completionHandler: (void (^)(HRPAwEventbookingTicket* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwEventbookingTicket* data = [[HRPAwEventbookingTicket alloc] init]; // An object of model property name/value pairs (optional)

HRPAwEventbookingTicketApi*apiInstance = [[HRPAwEventbookingTicketApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance awEventbookingTicketUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPAwEventbookingTicket* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwEventbookingTicketApi->awEventbookingTicketUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwEventbookingTicket***](HRPAwEventbookingTicket*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwEventbookingTicket***](HRPAwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

