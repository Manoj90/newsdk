# HRPStripePlanApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripePlanCount**](HRPStripePlanApi.md#stripeplancount) | **GET** /StripePlans/count | Count instances of the model matched by where from the data source.
[**stripePlanCreate**](HRPStripePlanApi.md#stripeplancreate) | **POST** /StripePlans | Create a new instance of the model and persist it into the data source.
[**stripePlanCreateChangeStreamGetStripePlansChangeStream**](HRPStripePlanApi.md#stripeplancreatechangestreamgetstripeplanschangestream) | **GET** /StripePlans/change-stream | Create a change stream.
[**stripePlanCreateChangeStreamPostStripePlansChangeStream**](HRPStripePlanApi.md#stripeplancreatechangestreampoststripeplanschangestream) | **POST** /StripePlans/change-stream | Create a change stream.
[**stripePlanDeleteById**](HRPStripePlanApi.md#stripeplandeletebyid) | **DELETE** /StripePlans/{id} | Delete a model instance by {{id}} from the data source.
[**stripePlanExistsGetStripePlansidExists**](HRPStripePlanApi.md#stripeplanexistsgetstripeplansidexists) | **GET** /StripePlans/{id}/exists | Check whether a model instance exists in the data source.
[**stripePlanExistsHeadStripePlansid**](HRPStripePlanApi.md#stripeplanexistsheadstripeplansid) | **HEAD** /StripePlans/{id} | Check whether a model instance exists in the data source.
[**stripePlanFind**](HRPStripePlanApi.md#stripeplanfind) | **GET** /StripePlans | Find all instances of the model matched by filter from the data source.
[**stripePlanFindById**](HRPStripePlanApi.md#stripeplanfindbyid) | **GET** /StripePlans/{id} | Find a model instance by {{id}} from the data source.
[**stripePlanFindOne**](HRPStripePlanApi.md#stripeplanfindone) | **GET** /StripePlans/findOne | Find first instance of the model matched by filter from the data source.
[**stripePlanPrototypeUpdateAttributesPatchStripePlansid**](HRPStripePlanApi.md#stripeplanprototypeupdateattributespatchstripeplansid) | **PATCH** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripePlanPrototypeUpdateAttributesPutStripePlansid**](HRPStripePlanApi.md#stripeplanprototypeupdateattributesputstripeplansid) | **PUT** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripePlanReplaceById**](HRPStripePlanApi.md#stripeplanreplacebyid) | **POST** /StripePlans/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripePlanReplaceOrCreate**](HRPStripePlanApi.md#stripeplanreplaceorcreate) | **POST** /StripePlans/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripePlanUpdateAll**](HRPStripePlanApi.md#stripeplanupdateall) | **POST** /StripePlans/update | Update instances of the model matched by {{where}} from the data source.
[**stripePlanUpsertPatchStripePlans**](HRPStripePlanApi.md#stripeplanupsertpatchstripeplans) | **PATCH** /StripePlans | Patch an existing model instance or insert a new one into the data source.
[**stripePlanUpsertPutStripePlans**](HRPStripePlanApi.md#stripeplanupsertputstripeplans) | **PUT** /StripePlans | Patch an existing model instance or insert a new one into the data source.
[**stripePlanUpsertWithWhere**](HRPStripePlanApi.md#stripeplanupsertwithwhere) | **POST** /StripePlans/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripePlanCount**
```objc
-(NSNumber*) stripePlanCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance stripePlanCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanCreate**
```objc
-(NSNumber*) stripePlanCreateWithData: (HRPStripePlan*) data
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripePlan* data = [[HRPStripePlan alloc] init]; // Model instance data (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance stripePlanCreateWithData:data
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripePlan***](HRPStripePlan*.md)| Model instance data | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanCreateChangeStreamGetStripePlansChangeStream**
```objc
-(NSNumber*) stripePlanCreateChangeStreamGetStripePlansChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Create a change stream.
[apiInstance stripePlanCreateChangeStreamGetStripePlansChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanCreateChangeStreamGetStripePlansChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanCreateChangeStreamPostStripePlansChangeStream**
```objc
-(NSNumber*) stripePlanCreateChangeStreamPostStripePlansChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Create a change stream.
[apiInstance stripePlanCreateChangeStreamPostStripePlansChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanCreateChangeStreamPostStripePlansChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanDeleteById**
```objc
-(NSNumber*) stripePlanDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance stripePlanDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanExistsGetStripePlansidExists**
```objc
-(NSNumber*) stripePlanExistsGetStripePlansidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripePlanExistsGetStripePlansidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanExistsGetStripePlansidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanExistsHeadStripePlansid**
```objc
-(NSNumber*) stripePlanExistsHeadStripePlansidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripePlanExistsHeadStripePlansidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanExistsHeadStripePlansid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanFind**
```objc
-(NSNumber*) stripePlanFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPStripePlan>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance stripePlanFindWithFilter:filter
          completionHandler: ^(NSArray<HRPStripePlan>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPStripePlan>***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanFindById**
```objc
-(NSNumber*) stripePlanFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance stripePlanFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanFindOne**
```objc
-(NSNumber*) stripePlanFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance stripePlanFindOneWithFilter:filter
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanPrototypeUpdateAttributesPatchStripePlansid**
```objc
-(NSNumber*) stripePlanPrototypeUpdateAttributesPatchStripePlansidWithId: (NSString*) _id
    data: (HRPStripePlan*) data
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripePlan id
HRPStripePlan* data = [[HRPStripePlan alloc] init]; // An object of model property name/value pairs (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripePlanPrototypeUpdateAttributesPatchStripePlansidWithId:_id
              data:data
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanPrototypeUpdateAttributesPatchStripePlansid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripePlan id | 
 **data** | [**HRPStripePlan***](HRPStripePlan*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanPrototypeUpdateAttributesPutStripePlansid**
```objc
-(NSNumber*) stripePlanPrototypeUpdateAttributesPutStripePlansidWithId: (NSString*) _id
    data: (HRPStripePlan*) data
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripePlan id
HRPStripePlan* data = [[HRPStripePlan alloc] init]; // An object of model property name/value pairs (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripePlanPrototypeUpdateAttributesPutStripePlansidWithId:_id
              data:data
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanPrototypeUpdateAttributesPutStripePlansid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripePlan id | 
 **data** | [**HRPStripePlan***](HRPStripePlan*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanReplaceById**
```objc
-(NSNumber*) stripePlanReplaceByIdWithId: (NSString*) _id
    data: (HRPStripePlan*) data
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPStripePlan* data = [[HRPStripePlan alloc] init]; // Model instance data (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance stripePlanReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPStripePlan***](HRPStripePlan*.md)| Model instance data | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanReplaceOrCreate**
```objc
-(NSNumber*) stripePlanReplaceOrCreateWithData: (HRPStripePlan*) data
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripePlan* data = [[HRPStripePlan alloc] init]; // Model instance data (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance stripePlanReplaceOrCreateWithData:data
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripePlan***](HRPStripePlan*.md)| Model instance data | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanUpdateAll**
```objc
-(NSNumber*) stripePlanUpdateAllWithWhere: (NSString*) where
    data: (HRPStripePlan*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripePlan* data = [[HRPStripePlan alloc] init]; // An object of model property name/value pairs (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance stripePlanUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripePlan***](HRPStripePlan*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanUpsertPatchStripePlans**
```objc
-(NSNumber*) stripePlanUpsertPatchStripePlansWithData: (HRPStripePlan*) data
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripePlan* data = [[HRPStripePlan alloc] init]; // Model instance data (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripePlanUpsertPatchStripePlansWithData:data
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanUpsertPatchStripePlans: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripePlan***](HRPStripePlan*.md)| Model instance data | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanUpsertPutStripePlans**
```objc
-(NSNumber*) stripePlanUpsertPutStripePlansWithData: (HRPStripePlan*) data
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripePlan* data = [[HRPStripePlan alloc] init]; // Model instance data (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripePlanUpsertPutStripePlansWithData:data
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanUpsertPutStripePlans: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripePlan***](HRPStripePlan*.md)| Model instance data | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripePlanUpsertWithWhere**
```objc
-(NSNumber*) stripePlanUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPStripePlan*) data
        completionHandler: (void (^)(HRPStripePlan* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripePlan* data = [[HRPStripePlan alloc] init]; // An object of model property name/value pairs (optional)

HRPStripePlanApi*apiInstance = [[HRPStripePlanApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance stripePlanUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPStripePlan* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripePlanApi->stripePlanUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripePlan***](HRPStripePlan*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripePlan***](HRPStripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

