# HRPStripeDiscountApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeDiscountCount**](HRPStripeDiscountApi.md#stripediscountcount) | **GET** /StripeDiscounts/count | Count instances of the model matched by where from the data source.
[**stripeDiscountCreate**](HRPStripeDiscountApi.md#stripediscountcreate) | **POST** /StripeDiscounts | Create a new instance of the model and persist it into the data source.
[**stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream**](HRPStripeDiscountApi.md#stripediscountcreatechangestreamgetstripediscountschangestream) | **GET** /StripeDiscounts/change-stream | Create a change stream.
[**stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream**](HRPStripeDiscountApi.md#stripediscountcreatechangestreampoststripediscountschangestream) | **POST** /StripeDiscounts/change-stream | Create a change stream.
[**stripeDiscountDeleteById**](HRPStripeDiscountApi.md#stripediscountdeletebyid) | **DELETE** /StripeDiscounts/{id} | Delete a model instance by {{id}} from the data source.
[**stripeDiscountExistsGetStripeDiscountsidExists**](HRPStripeDiscountApi.md#stripediscountexistsgetstripediscountsidexists) | **GET** /StripeDiscounts/{id}/exists | Check whether a model instance exists in the data source.
[**stripeDiscountExistsHeadStripeDiscountsid**](HRPStripeDiscountApi.md#stripediscountexistsheadstripediscountsid) | **HEAD** /StripeDiscounts/{id} | Check whether a model instance exists in the data source.
[**stripeDiscountFind**](HRPStripeDiscountApi.md#stripediscountfind) | **GET** /StripeDiscounts | Find all instances of the model matched by filter from the data source.
[**stripeDiscountFindById**](HRPStripeDiscountApi.md#stripediscountfindbyid) | **GET** /StripeDiscounts/{id} | Find a model instance by {{id}} from the data source.
[**stripeDiscountFindOne**](HRPStripeDiscountApi.md#stripediscountfindone) | **GET** /StripeDiscounts/findOne | Find first instance of the model matched by filter from the data source.
[**stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid**](HRPStripeDiscountApi.md#stripediscountprototypeupdateattributespatchstripediscountsid) | **PATCH** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid**](HRPStripeDiscountApi.md#stripediscountprototypeupdateattributesputstripediscountsid) | **PUT** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeDiscountReplaceById**](HRPStripeDiscountApi.md#stripediscountreplacebyid) | **POST** /StripeDiscounts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeDiscountReplaceOrCreate**](HRPStripeDiscountApi.md#stripediscountreplaceorcreate) | **POST** /StripeDiscounts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeDiscountUpdateAll**](HRPStripeDiscountApi.md#stripediscountupdateall) | **POST** /StripeDiscounts/update | Update instances of the model matched by {{where}} from the data source.
[**stripeDiscountUpsertPatchStripeDiscounts**](HRPStripeDiscountApi.md#stripediscountupsertpatchstripediscounts) | **PATCH** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
[**stripeDiscountUpsertPutStripeDiscounts**](HRPStripeDiscountApi.md#stripediscountupsertputstripediscounts) | **PUT** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
[**stripeDiscountUpsertWithWhere**](HRPStripeDiscountApi.md#stripediscountupsertwithwhere) | **POST** /StripeDiscounts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeDiscountCount**
```objc
-(NSNumber*) stripeDiscountCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance stripeDiscountCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountCreate**
```objc
-(NSNumber*) stripeDiscountCreateWithData: (HRPStripeDiscount*) data
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeDiscount* data = [[HRPStripeDiscount alloc] init]; // Model instance data (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance stripeDiscountCreateWithData:data
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeDiscount***](HRPStripeDiscount*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream**
```objc
-(NSNumber*) stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Create a change stream.
[apiInstance stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream**
```objc
-(NSNumber*) stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Create a change stream.
[apiInstance stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountDeleteById**
```objc
-(NSNumber*) stripeDiscountDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance stripeDiscountDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountExistsGetStripeDiscountsidExists**
```objc
-(NSNumber*) stripeDiscountExistsGetStripeDiscountsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeDiscountExistsGetStripeDiscountsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountExistsGetStripeDiscountsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountExistsHeadStripeDiscountsid**
```objc
-(NSNumber*) stripeDiscountExistsHeadStripeDiscountsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeDiscountExistsHeadStripeDiscountsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountExistsHeadStripeDiscountsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountFind**
```objc
-(NSNumber*) stripeDiscountFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPStripeDiscount>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance stripeDiscountFindWithFilter:filter
          completionHandler: ^(NSArray<HRPStripeDiscount>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPStripeDiscount>***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountFindById**
```objc
-(NSNumber*) stripeDiscountFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance stripeDiscountFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountFindOne**
```objc
-(NSNumber*) stripeDiscountFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance stripeDiscountFindOneWithFilter:filter
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid**
```objc
-(NSNumber*) stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsidWithId: (NSString*) _id
    data: (HRPStripeDiscount*) data
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeDiscount id
HRPStripeDiscount* data = [[HRPStripeDiscount alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsidWithId:_id
              data:data
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeDiscount id | 
 **data** | [**HRPStripeDiscount***](HRPStripeDiscount*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid**
```objc
-(NSNumber*) stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsidWithId: (NSString*) _id
    data: (HRPStripeDiscount*) data
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeDiscount id
HRPStripeDiscount* data = [[HRPStripeDiscount alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsidWithId:_id
              data:data
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeDiscount id | 
 **data** | [**HRPStripeDiscount***](HRPStripeDiscount*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountReplaceById**
```objc
-(NSNumber*) stripeDiscountReplaceByIdWithId: (NSString*) _id
    data: (HRPStripeDiscount*) data
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPStripeDiscount* data = [[HRPStripeDiscount alloc] init]; // Model instance data (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance stripeDiscountReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPStripeDiscount***](HRPStripeDiscount*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountReplaceOrCreate**
```objc
-(NSNumber*) stripeDiscountReplaceOrCreateWithData: (HRPStripeDiscount*) data
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeDiscount* data = [[HRPStripeDiscount alloc] init]; // Model instance data (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance stripeDiscountReplaceOrCreateWithData:data
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeDiscount***](HRPStripeDiscount*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountUpdateAll**
```objc
-(NSNumber*) stripeDiscountUpdateAllWithWhere: (NSString*) where
    data: (HRPStripeDiscount*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeDiscount* data = [[HRPStripeDiscount alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance stripeDiscountUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeDiscount***](HRPStripeDiscount*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountUpsertPatchStripeDiscounts**
```objc
-(NSNumber*) stripeDiscountUpsertPatchStripeDiscountsWithData: (HRPStripeDiscount*) data
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeDiscount* data = [[HRPStripeDiscount alloc] init]; // Model instance data (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeDiscountUpsertPatchStripeDiscountsWithData:data
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountUpsertPatchStripeDiscounts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeDiscount***](HRPStripeDiscount*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountUpsertPutStripeDiscounts**
```objc
-(NSNumber*) stripeDiscountUpsertPutStripeDiscountsWithData: (HRPStripeDiscount*) data
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeDiscount* data = [[HRPStripeDiscount alloc] init]; // Model instance data (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeDiscountUpsertPutStripeDiscountsWithData:data
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountUpsertPutStripeDiscounts: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeDiscount***](HRPStripeDiscount*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeDiscountUpsertWithWhere**
```objc
-(NSNumber*) stripeDiscountUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPStripeDiscount*) data
        completionHandler: (void (^)(HRPStripeDiscount* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeDiscount* data = [[HRPStripeDiscount alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeDiscountApi*apiInstance = [[HRPStripeDiscountApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance stripeDiscountUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPStripeDiscount* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeDiscountApi->stripeDiscountUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeDiscount***](HRPStripeDiscount*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeDiscount***](HRPStripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

