# HRPCompetitionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**competitionAttendees**](HRPCompetitionApi.md#competitionattendees) | **GET** /Competitions/{id}/attendees | 
[**competitionCheckout**](HRPCompetitionApi.md#competitioncheckout) | **POST** /Competitions/{id}/checkout | 
[**competitionFind**](HRPCompetitionApi.md#competitionfind) | **GET** /Competitions | Find all instances of the model matched by filter from the data source.
[**competitionFindById**](HRPCompetitionApi.md#competitionfindbyid) | **GET** /Competitions/{id} | Find a model instance by {{id}} from the data source.
[**competitionFindOne**](HRPCompetitionApi.md#competitionfindone) | **GET** /Competitions/findOne | Find first instance of the model matched by filter from the data source.
[**competitionRedeem**](HRPCompetitionApi.md#competitionredeem) | **POST** /Competitions/{id}/redeem | 
[**competitionReplaceById**](HRPCompetitionApi.md#competitionreplacebyid) | **POST** /Competitions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**competitionReplaceOrCreate**](HRPCompetitionApi.md#competitionreplaceorcreate) | **POST** /Competitions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**competitionUpsertWithWhere**](HRPCompetitionApi.md#competitionupsertwithwhere) | **POST** /Competitions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**competitionValidateAnswer**](HRPCompetitionApi.md#competitionvalidateanswer) | **POST** /Competitions/{id}/validateAnswer | 
[**competitionVenues**](HRPCompetitionApi.md#competitionvenues) | **GET** /Competitions/{id}/venues | 


# **competitionAttendees**
```objc
-(NSNumber*) competitionAttendeesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCustomer>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

[apiInstance competitionAttendeesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPCustomer>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionAttendees: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPCustomer>***](HRPCustomer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionCheckout**
```objc
-(NSNumber*) competitionCheckoutWithId: (NSString*) _id
    data: (HRPCompetitionCheckoutData*) data
        completionHandler: (void (^)(HRPCompetitionCheckout* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCompetitionCheckoutData* data = [[HRPCompetitionCheckoutData alloc] init]; //  (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

[apiInstance competitionCheckoutWithId:_id
              data:data
          completionHandler: ^(HRPCompetitionCheckout* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionCheckout: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCompetitionCheckoutData***](HRPCompetitionCheckoutData*.md)|  | [optional] 

### Return type

[**HRPCompetitionCheckout***](HRPCompetitionCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionFind**
```objc
-(NSNumber*) competitionFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPCompetition>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance competitionFindWithFilter:filter
          completionHandler: ^(NSArray<HRPCompetition>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPCompetition>***](HRPCompetition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionFindById**
```objc
-(NSNumber*) competitionFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPCompetition* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance competitionFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPCompetition* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPCompetition***](HRPCompetition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionFindOne**
```objc
-(NSNumber*) competitionFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPCompetition* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance competitionFindOneWithFilter:filter
          completionHandler: ^(HRPCompetition* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPCompetition***](HRPCompetition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionRedeem**
```objc
-(NSNumber*) competitionRedeemWithId: (NSString*) _id
    data: (HRPCompetitionRedeemData*) data
        completionHandler: (void (^)(HRPCompetitionPurchase* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCompetitionRedeemData* data = [[HRPCompetitionRedeemData alloc] init]; //  (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

[apiInstance competitionRedeemWithId:_id
              data:data
          completionHandler: ^(HRPCompetitionPurchase* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionRedeem: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCompetitionRedeemData***](HRPCompetitionRedeemData*.md)|  | [optional] 

### Return type

[**HRPCompetitionPurchase***](HRPCompetitionPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionReplaceById**
```objc
-(NSNumber*) competitionReplaceByIdWithId: (NSString*) _id
    data: (HRPCompetition*) data
        completionHandler: (void (^)(HRPCompetition* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCompetition* data = [[HRPCompetition alloc] init]; // Model instance data (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance competitionReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPCompetition* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPCompetition***](HRPCompetition*.md)| Model instance data | [optional] 

### Return type

[**HRPCompetition***](HRPCompetition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionReplaceOrCreate**
```objc
-(NSNumber*) competitionReplaceOrCreateWithData: (HRPCompetition*) data
        completionHandler: (void (^)(HRPCompetition* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPCompetition* data = [[HRPCompetition alloc] init]; // Model instance data (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance competitionReplaceOrCreateWithData:data
          completionHandler: ^(HRPCompetition* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPCompetition***](HRPCompetition*.md)| Model instance data | [optional] 

### Return type

[**HRPCompetition***](HRPCompetition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionUpsertWithWhere**
```objc
-(NSNumber*) competitionUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPCompetition*) data
        completionHandler: (void (^)(HRPCompetition* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPCompetition* data = [[HRPCompetition alloc] init]; // An object of model property name/value pairs (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance competitionUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPCompetition* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPCompetition***](HRPCompetition*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPCompetition***](HRPCompetition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionValidateAnswer**
```objc
-(NSNumber*) competitionValidateAnswerWithId: (NSString*) _id
    answer: (HRPCompetitionAnswerData*) answer
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPCompetitionAnswerData* answer = [[HRPCompetitionAnswerData alloc] init]; // Competition Answer

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

[apiInstance competitionValidateAnswerWithId:_id
              answer:answer
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionValidateAnswer: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **answer** | [**HRPCompetitionAnswerData***](HRPCompetitionAnswerData*.md)| Competition Answer | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **competitionVenues**
```objc
-(NSNumber*) competitionVenuesWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPVenue>* output, NSError* error)) handler;
```



### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPCompetitionApi*apiInstance = [[HRPCompetitionApi alloc] init];

[apiInstance competitionVenuesWithId:_id
              filter:filter
          completionHandler: ^(NSArray<HRPVenue>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPCompetitionApi->competitionVenues: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**NSArray<HRPVenue>***](HRPVenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

