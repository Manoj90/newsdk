# HRPCustomerCardCreateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **NSString*** |  | 
**number** | **NSString*** |  | 
**cvc** | **NSString*** |  | 
**exMonth** | **NSString*** |  | 
**exYear** | **NSString*** |  | 
**cardholderName** | **NSString*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


