# HRPCustomerPasswordUpdateCredentials

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currentPassword** | **NSString*** | Current Customer Passwrod | 
**varNewPassword** | **NSString*** | New Customer Password | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


