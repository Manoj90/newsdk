# HRPStripeSubscriptionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeSubscriptionCount**](HRPStripeSubscriptionApi.md#stripesubscriptioncount) | **GET** /StripeSubscriptions/count | Count instances of the model matched by where from the data source.
[**stripeSubscriptionCreate**](HRPStripeSubscriptionApi.md#stripesubscriptioncreate) | **POST** /StripeSubscriptions | Create a new instance of the model and persist it into the data source.
[**stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream**](HRPStripeSubscriptionApi.md#stripesubscriptioncreatechangestreamgetstripesubscriptionschangestream) | **GET** /StripeSubscriptions/change-stream | Create a change stream.
[**stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream**](HRPStripeSubscriptionApi.md#stripesubscriptioncreatechangestreampoststripesubscriptionschangestream) | **POST** /StripeSubscriptions/change-stream | Create a change stream.
[**stripeSubscriptionDeleteById**](HRPStripeSubscriptionApi.md#stripesubscriptiondeletebyid) | **DELETE** /StripeSubscriptions/{id} | Delete a model instance by {{id}} from the data source.
[**stripeSubscriptionExistsGetStripeSubscriptionsidExists**](HRPStripeSubscriptionApi.md#stripesubscriptionexistsgetstripesubscriptionsidexists) | **GET** /StripeSubscriptions/{id}/exists | Check whether a model instance exists in the data source.
[**stripeSubscriptionExistsHeadStripeSubscriptionsid**](HRPStripeSubscriptionApi.md#stripesubscriptionexistsheadstripesubscriptionsid) | **HEAD** /StripeSubscriptions/{id} | Check whether a model instance exists in the data source.
[**stripeSubscriptionFind**](HRPStripeSubscriptionApi.md#stripesubscriptionfind) | **GET** /StripeSubscriptions | Find all instances of the model matched by filter from the data source.
[**stripeSubscriptionFindById**](HRPStripeSubscriptionApi.md#stripesubscriptionfindbyid) | **GET** /StripeSubscriptions/{id} | Find a model instance by {{id}} from the data source.
[**stripeSubscriptionFindOne**](HRPStripeSubscriptionApi.md#stripesubscriptionfindone) | **GET** /StripeSubscriptions/findOne | Find first instance of the model matched by filter from the data source.
[**stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid**](HRPStripeSubscriptionApi.md#stripesubscriptionprototypeupdateattributespatchstripesubscriptionsid) | **PATCH** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid**](HRPStripeSubscriptionApi.md#stripesubscriptionprototypeupdateattributesputstripesubscriptionsid) | **PUT** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeSubscriptionReplaceById**](HRPStripeSubscriptionApi.md#stripesubscriptionreplacebyid) | **POST** /StripeSubscriptions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeSubscriptionReplaceOrCreate**](HRPStripeSubscriptionApi.md#stripesubscriptionreplaceorcreate) | **POST** /StripeSubscriptions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpdateAll**](HRPStripeSubscriptionApi.md#stripesubscriptionupdateall) | **POST** /StripeSubscriptions/update | Update instances of the model matched by {{where}} from the data source.
[**stripeSubscriptionUpsertPatchStripeSubscriptions**](HRPStripeSubscriptionApi.md#stripesubscriptionupsertpatchstripesubscriptions) | **PATCH** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpsertPutStripeSubscriptions**](HRPStripeSubscriptionApi.md#stripesubscriptionupsertputstripesubscriptions) | **PUT** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpsertWithWhere**](HRPStripeSubscriptionApi.md#stripesubscriptionupsertwithwhere) | **POST** /StripeSubscriptions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **stripeSubscriptionCount**
```objc
-(NSNumber*) stripeSubscriptionCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance stripeSubscriptionCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionCreate**
```objc
-(NSNumber*) stripeSubscriptionCreateWithData: (HRPStripeSubscription*) data
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeSubscription* data = [[HRPStripeSubscription alloc] init]; // Model instance data (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance stripeSubscriptionCreateWithData:data
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeSubscription***](HRPStripeSubscription*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream**
```objc
-(NSNumber*) stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Create a change stream.
[apiInstance stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream**
```objc
-(NSNumber*) stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Create a change stream.
[apiInstance stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionDeleteById**
```objc
-(NSNumber*) stripeSubscriptionDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance stripeSubscriptionDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionExistsGetStripeSubscriptionsidExists**
```objc
-(NSNumber*) stripeSubscriptionExistsGetStripeSubscriptionsidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeSubscriptionExistsGetStripeSubscriptionsidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionExistsGetStripeSubscriptionsidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionExistsHeadStripeSubscriptionsid**
```objc
-(NSNumber*) stripeSubscriptionExistsHeadStripeSubscriptionsidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance stripeSubscriptionExistsHeadStripeSubscriptionsidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionExistsHeadStripeSubscriptionsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionFind**
```objc
-(NSNumber*) stripeSubscriptionFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPStripeSubscription>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance stripeSubscriptionFindWithFilter:filter
          completionHandler: ^(NSArray<HRPStripeSubscription>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPStripeSubscription>***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionFindById**
```objc
-(NSNumber*) stripeSubscriptionFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance stripeSubscriptionFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionFindOne**
```objc
-(NSNumber*) stripeSubscriptionFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance stripeSubscriptionFindOneWithFilter:filter
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid**
```objc
-(NSNumber*) stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsidWithId: (NSString*) _id
    data: (HRPStripeSubscription*) data
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeSubscription id
HRPStripeSubscription* data = [[HRPStripeSubscription alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsidWithId:_id
              data:data
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeSubscription id | 
 **data** | [**HRPStripeSubscription***](HRPStripeSubscription*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid**
```objc
-(NSNumber*) stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsidWithId: (NSString*) _id
    data: (HRPStripeSubscription*) data
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // StripeSubscription id
HRPStripeSubscription* data = [[HRPStripeSubscription alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsidWithId:_id
              data:data
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| StripeSubscription id | 
 **data** | [**HRPStripeSubscription***](HRPStripeSubscription*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionReplaceById**
```objc
-(NSNumber*) stripeSubscriptionReplaceByIdWithId: (NSString*) _id
    data: (HRPStripeSubscription*) data
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPStripeSubscription* data = [[HRPStripeSubscription alloc] init]; // Model instance data (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance stripeSubscriptionReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPStripeSubscription***](HRPStripeSubscription*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionReplaceOrCreate**
```objc
-(NSNumber*) stripeSubscriptionReplaceOrCreateWithData: (HRPStripeSubscription*) data
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeSubscription* data = [[HRPStripeSubscription alloc] init]; // Model instance data (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance stripeSubscriptionReplaceOrCreateWithData:data
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeSubscription***](HRPStripeSubscription*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionUpdateAll**
```objc
-(NSNumber*) stripeSubscriptionUpdateAllWithWhere: (NSString*) where
    data: (HRPStripeSubscription*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeSubscription* data = [[HRPStripeSubscription alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance stripeSubscriptionUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeSubscription***](HRPStripeSubscription*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionUpsertPatchStripeSubscriptions**
```objc
-(NSNumber*) stripeSubscriptionUpsertPatchStripeSubscriptionsWithData: (HRPStripeSubscription*) data
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeSubscription* data = [[HRPStripeSubscription alloc] init]; // Model instance data (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeSubscriptionUpsertPatchStripeSubscriptionsWithData:data
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionUpsertPatchStripeSubscriptions: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeSubscription***](HRPStripeSubscription*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionUpsertPutStripeSubscriptions**
```objc
-(NSNumber*) stripeSubscriptionUpsertPutStripeSubscriptionsWithData: (HRPStripeSubscription*) data
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPStripeSubscription* data = [[HRPStripeSubscription alloc] init]; // Model instance data (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance stripeSubscriptionUpsertPutStripeSubscriptionsWithData:data
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionUpsertPutStripeSubscriptions: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPStripeSubscription***](HRPStripeSubscription*.md)| Model instance data | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeSubscriptionUpsertWithWhere**
```objc
-(NSNumber*) stripeSubscriptionUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPStripeSubscription*) data
        completionHandler: (void (^)(HRPStripeSubscription* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPStripeSubscription* data = [[HRPStripeSubscription alloc] init]; // An object of model property name/value pairs (optional)

HRPStripeSubscriptionApi*apiInstance = [[HRPStripeSubscriptionApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance stripeSubscriptionUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPStripeSubscription* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPStripeSubscriptionApi->stripeSubscriptionUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPStripeSubscription***](HRPStripeSubscription*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPStripeSubscription***](HRPStripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

