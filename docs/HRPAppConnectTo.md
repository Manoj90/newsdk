# HRPAppConnectTo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **NSString*** |  | 
**heading** | **NSString*** |  | 
**_description** | **NSString*** |  | 
**link** | **NSString*** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


