# HRPAwCollpurDealPurchasesApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurDealPurchasesCount**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasescount) | **GET** /AwCollpurDealPurchases/count | Count instances of the model matched by where from the data source.
[**awCollpurDealPurchasesCreate**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasescreate) | **POST** /AwCollpurDealPurchases | Create a new instance of the model and persist it into the data source.
[**awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasescreatechangestreamgetawcollpurdealpurchaseschangestream) | **GET** /AwCollpurDealPurchases/change-stream | Create a change stream.
[**awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasescreatechangestreampostawcollpurdealpurchaseschangestream) | **POST** /AwCollpurDealPurchases/change-stream | Create a change stream.
[**awCollpurDealPurchasesDeleteById**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesdeletebyid) | **DELETE** /AwCollpurDealPurchases/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesexistsgetawcollpurdealpurchasesidexists) | **GET** /AwCollpurDealPurchases/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesexistsheadawcollpurdealpurchasesid) | **HEAD** /AwCollpurDealPurchases/{id} | Check whether a model instance exists in the data source.
[**awCollpurDealPurchasesFind**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesfind) | **GET** /AwCollpurDealPurchases | Find all instances of the model matched by filter from the data source.
[**awCollpurDealPurchasesFindById**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesfindbyid) | **GET** /AwCollpurDealPurchases/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurDealPurchasesFindOne**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesfindone) | **GET** /AwCollpurDealPurchases/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesprototypecreateawcollpurcoupon) | **POST** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Creates a new instance in awCollpurCoupon of this model.
[**awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesprototypedestroyawcollpurcoupon) | **DELETE** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Deletes awCollpurCoupon of this model.
[**awCollpurDealPurchasesPrototypeGetAwCollpurCoupon**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesprototypegetawcollpurcoupon) | **GET** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Fetches hasOne relation awCollpurCoupon.
[**awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesprototypeupdateattributespatchawcollpurdealpurchasesid) | **PATCH** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesprototypeupdateattributesputawcollpurdealpurchasesid) | **PUT** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesprototypeupdateawcollpurcoupon) | **PUT** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Update awCollpurCoupon of this model.
[**awCollpurDealPurchasesReplaceById**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesreplacebyid) | **POST** /AwCollpurDealPurchases/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesReplaceOrCreate**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesreplaceorcreate) | **POST** /AwCollpurDealPurchases/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpdateAll**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesupdateall) | **POST** /AwCollpurDealPurchases/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesupsertpatchawcollpurdealpurchases) | **PATCH** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesupsertputawcollpurdealpurchases) | **PUT** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpsertWithWhere**](HRPAwCollpurDealPurchasesApi.md#awcollpurdealpurchasesupsertwithwhere) | **POST** /AwCollpurDealPurchases/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


# **awCollpurDealPurchasesCount**
```objc
-(NSNumber*) awCollpurDealPurchasesCountWithWhere: (NSString*) where
        completionHandler: (void (^)(HRPInlineResponse2001* output, NSError* error)) handler;
```

Count instances of the model matched by where from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Count instances of the model matched by where from the data source.
[apiInstance awCollpurDealPurchasesCountWithWhere:where
          completionHandler: ^(HRPInlineResponse2001* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesCount: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 

### Return type

[**HRPInlineResponse2001***](HRPInlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesCreate**
```objc
-(NSNumber*) awCollpurDealPurchasesCreateWithData: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Create a new instance of the model and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; // Model instance data (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Create a new instance of the model and persist it into the data source.
[apiInstance awCollpurDealPurchasesCreateWithData:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream**
```objc
-(NSNumber*) awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Create a change stream.
[apiInstance awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream**
```objc
-(NSNumber*) awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStreamWithOptions: (NSString*) options
        completionHandler: (void (^)(NSURL* output, NSError* error)) handler;
```

Create a change stream.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* options = @"options_example"; //  (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Create a change stream.
[apiInstance awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStreamWithOptions:options
          completionHandler: ^(NSURL* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **NSString***|  | [optional] 

### Return type

**NSURL***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesDeleteById**
```objc
-(NSNumber*) awCollpurDealPurchasesDeleteByIdWithId: (NSString*) _id
        completionHandler: (void (^)(NSObject* output, NSError* error)) handler;
```

Delete a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Delete a model instance by {{id}} from the data source.
[apiInstance awCollpurDealPurchasesDeleteByIdWithId:_id
          completionHandler: ^(NSObject* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesDeleteById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

**NSObject***

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists**
```objc
-(NSNumber*) awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExistsWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExistsWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid**
```objc
-(NSNumber*) awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesidWithId: (NSString*) _id
        completionHandler: (void (^)(HRPInlineResponse2003* output, NSError* error)) handler;
```

Check whether a model instance exists in the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Check whether a model instance exists in the data source.
[apiInstance awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesidWithId:_id
          completionHandler: ^(HRPInlineResponse2003* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 

### Return type

[**HRPInlineResponse2003***](HRPInlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesFind**
```objc
-(NSNumber*) awCollpurDealPurchasesFindWithFilter: (NSString*) filter
        completionHandler: (void (^)(NSArray<HRPAwCollpurDealPurchases>* output, NSError* error)) handler;
```

Find all instances of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Find all instances of the model matched by filter from the data source.
[apiInstance awCollpurDealPurchasesFindWithFilter:filter
          completionHandler: ^(NSArray<HRPAwCollpurDealPurchases>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesFind: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**NSArray<HRPAwCollpurDealPurchases>***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesFindById**
```objc
-(NSNumber*) awCollpurDealPurchasesFindByIdWithId: (NSString*) _id
    filter: (NSString*) filter
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Find a model instance by {{id}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
NSString* filter = @"filter_example"; // Filter defining fields and include (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Find a model instance by {{id}} from the data source.
[apiInstance awCollpurDealPurchasesFindByIdWithId:_id
              filter:filter
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesFindById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **filter** | **NSString***| Filter defining fields and include | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesFindOne**
```objc
-(NSNumber*) awCollpurDealPurchasesFindOneWithFilter: (NSString*) filter
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Find first instance of the model matched by filter from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* filter = @"filter_example"; // Filter defining fields, where, include, order, offset, and limit (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Find first instance of the model matched by filter from the data source.
[apiInstance awCollpurDealPurchasesFindOneWithFilter:filter
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesFindOne: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **NSString***| Filter defining fields, where, include, order, offset, and limit | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon**
```objc
-(NSNumber*) awCollpurDealPurchasesPrototypeCreateAwCollpurCouponWithId: (NSString*) _id
    data: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Creates a new instance in awCollpurCoupon of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDealPurchases id
HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; //  (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Creates a new instance in awCollpurCoupon of this model.
[apiInstance awCollpurDealPurchasesPrototypeCreateAwCollpurCouponWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDealPurchases id | 
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)|  | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon**
```objc
-(NSNumber*) awCollpurDealPurchasesPrototypeDestroyAwCollpurCouponWithId: (NSString*) _id
        completionHandler: (void (^)(NSError* error)) handler;
```

Deletes awCollpurCoupon of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDealPurchases id

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Deletes awCollpurCoupon of this model.
[apiInstance awCollpurDealPurchasesPrototypeDestroyAwCollpurCouponWithId:_id
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDealPurchases id | 

### Return type

void (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesPrototypeGetAwCollpurCoupon**
```objc
-(NSNumber*) awCollpurDealPurchasesPrototypeGetAwCollpurCouponWithId: (NSString*) _id
    refresh: (NSNumber*) refresh
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Fetches hasOne relation awCollpurCoupon.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDealPurchases id
NSNumber* refresh = @true; //  (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Fetches hasOne relation awCollpurCoupon.
[apiInstance awCollpurDealPurchasesPrototypeGetAwCollpurCouponWithId:_id
              refresh:refresh
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeGetAwCollpurCoupon: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDealPurchases id | 
 **refresh** | **NSNumber***|  | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid**
```objc
-(NSNumber*) awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesidWithId: (NSString*) _id
    data: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDealPurchases id
HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesidWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDealPurchases id | 
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid**
```objc
-(NSNumber*) awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesidWithId: (NSString*) _id
    data: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Patch attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDealPurchases id
HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Patch attributes for a model instance and persist it into the data source.
[apiInstance awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesidWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDealPurchases id | 
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon**
```objc
-(NSNumber*) awCollpurDealPurchasesPrototypeUpdateAwCollpurCouponWithId: (NSString*) _id
    data: (HRPAwCollpurCoupon*) data
        completionHandler: (void (^)(HRPAwCollpurCoupon* output, NSError* error)) handler;
```

Update awCollpurCoupon of this model.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // AwCollpurDealPurchases id
HRPAwCollpurCoupon* data = [[HRPAwCollpurCoupon alloc] init]; //  (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Update awCollpurCoupon of this model.
[apiInstance awCollpurDealPurchasesPrototypeUpdateAwCollpurCouponWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurCoupon* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| AwCollpurDealPurchases id | 
 **data** | [**HRPAwCollpurCoupon***](HRPAwCollpurCoupon*.md)|  | [optional] 

### Return type

[**HRPAwCollpurCoupon***](HRPAwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesReplaceById**
```objc
-(NSNumber*) awCollpurDealPurchasesReplaceByIdWithId: (NSString*) _id
    data: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Replace attributes for a model instance and persist it into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* _id = @"_id_example"; // Model id
HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; // Model instance data (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Replace attributes for a model instance and persist it into the data source.
[apiInstance awCollpurDealPurchasesReplaceByIdWithId:_id
              data:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesReplaceById: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **_id** | **NSString***| Model id | 
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesReplaceOrCreate**
```objc
-(NSNumber*) awCollpurDealPurchasesReplaceOrCreateWithData: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Replace an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; // Model instance data (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Replace an existing model instance or insert a new one into the data source.
[apiInstance awCollpurDealPurchasesReplaceOrCreateWithData:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesReplaceOrCreate: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesUpdateAll**
```objc
-(NSNumber*) awCollpurDealPurchasesUpdateAllWithWhere: (NSString*) where
    data: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPInlineResponse2002* output, NSError* error)) handler;
```

Update instances of the model matched by {{where}} from the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Update instances of the model matched by {{where}} from the data source.
[apiInstance awCollpurDealPurchasesUpdateAllWithWhere:where
              data:data
          completionHandler: ^(HRPInlineResponse2002* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesUpdateAll: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPInlineResponse2002***](HRPInlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases**
```objc
-(NSNumber*) awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchasesWithData: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; // Model instance data (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchasesWithData:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases**
```objc
-(NSNumber*) awCollpurDealPurchasesUpsertPutAwCollpurDealPurchasesWithData: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Patch an existing model instance or insert a new one into the data source.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; // Model instance data (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Patch an existing model instance or insert a new one into the data source.
[apiInstance awCollpurDealPurchasesUpsertPutAwCollpurDealPurchasesWithData:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)| Model instance data | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **awCollpurDealPurchasesUpsertWithWhere**
```objc
-(NSNumber*) awCollpurDealPurchasesUpsertWithWhereWithWhere: (NSString*) where
    data: (HRPAwCollpurDealPurchases*) data
        completionHandler: (void (^)(HRPAwCollpurDealPurchases* output, NSError* error)) handler;
```

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example 
```objc
HRPConfiguration *apiConfig = [HRPConfiguration sharedConfig];

// Configure API key authorization: (authentication scheme: access_token)
[apiConfig setApiKey:@"YOUR_API_KEY" forApiKeyIdentifier:@"access_token"];
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//[apiConfig setApiKeyPrefix:@"Bearer" forApiKeyIdentifier:@"access_token"];


NSString* where = @"where_example"; // Criteria to match model instances (optional)
HRPAwCollpurDealPurchases* data = [[HRPAwCollpurDealPurchases alloc] init]; // An object of model property name/value pairs (optional)

HRPAwCollpurDealPurchasesApi*apiInstance = [[HRPAwCollpurDealPurchasesApi alloc] init];

// Update an existing model instance or insert a new one into the data source based on the where criteria.
[apiInstance awCollpurDealPurchasesUpsertWithWhereWithWhere:where
              data:data
          completionHandler: ^(HRPAwCollpurDealPurchases* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling HRPAwCollpurDealPurchasesApi->awCollpurDealPurchasesUpsertWithWhere: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **NSString***| Criteria to match model instances | [optional] 
 **data** | [**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases*.md)| An object of model property name/value pairs | [optional] 

### Return type

[**HRPAwCollpurDealPurchases***](HRPAwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

