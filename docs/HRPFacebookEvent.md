# HRPFacebookEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**node** | **NSString*** |  | [optional] 
**category** | **NSString*** |  | [optional] 
**updatedTime** | **NSString*** |  | [optional] 
**maybeCount** | **NSNumber*** |  | [optional] [default to @0.0]
**noreplyCount** | **NSNumber*** |  | [optional] [default to @0.0]
**attendingCount** | **NSNumber*** |  | [optional] [default to @0.0]
**place** | **NSObject*** |  | [optional] 
**_id** | **NSNumber*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


