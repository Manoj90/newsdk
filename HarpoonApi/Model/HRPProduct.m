#import "HRPProduct.h"

@implementation HRPProduct

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.price = @0.0;
    self.baseCurrency = @"EUR";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"name": @"name", @"description": @"_description", @"cover": @"cover", @"price": @"price", @"base_currency": @"baseCurrency", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"name", @"_description", @"cover", @"price", @"baseCurrency", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
