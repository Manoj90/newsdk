#import "HRPTritonConfig.h"

@implementation HRPTritonConfig

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"stationId": @"stationId", @"stationName": @"stationName", @"mp3Mount": @"mp3Mount", @"aacMount": @"aacMount" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"stationName", @"mp3Mount", @"aacMount"];
  return [optionalProperties containsObject:propertyName];
}

@end
