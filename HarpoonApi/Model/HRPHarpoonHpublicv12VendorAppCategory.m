#import "HRPHarpoonHpublicv12VendorAppCategory.h"

@implementation HRPHarpoonHpublicv12VendorAppCategory

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.isPrimary = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"appId": @"appId", @"vendorId": @"vendorId", @"categoryId": @"categoryId", @"isPrimary": @"isPrimary", @"createdAt": @"createdAt", @"updatedAt": @"updatedAt" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"isPrimary", @"createdAt", @"updatedAt"];
  return [optionalProperties containsObject:propertyName];
}

@end
