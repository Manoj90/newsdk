#import "HRPAwEventbookingEventTicket.h"

@implementation HRPAwEventbookingEventTicket

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"eventId": @"eventId", @"price": @"price", @"priceType": @"priceType", @"sku": @"sku", @"qty": @"qty", @"codeprefix": @"codeprefix", @"fee": @"fee", @"isPassOnFee": @"isPassOnFee", @"chance": @"chance", @"attributes": @"attributes", @"tickets": @"tickets", @"awEventbookingTicket": @"awEventbookingTicket" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"price", @"priceType", @"sku", @"qty", @"codeprefix", @"attributes", @"tickets", @"awEventbookingTicket"];
  return [optionalProperties containsObject:propertyName];
}

@end
