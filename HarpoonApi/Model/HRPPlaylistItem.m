#import "HRPPlaylistItem.h"

@implementation HRPPlaylistItem

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.order = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"title": @"title", @"shortDescription": @"shortDescription", @"image": @"image", @"file": @"file", @"type": @"type", @"mediaType": @"mediaType", @"mediaId": @"mediaId", @"order": @"order", @"id": @"_id", @"playlistId": @"playlistId", @"playlist": @"playlist", @"playerSources": @"playerSources", @"playerTracks": @"playerTracks", @"radioShows": @"radioShows" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"title", @"shortDescription", @"image", @"file", @"type", @"mediaType", @"mediaId", @"order", @"_id", @"playlistId", @"playlist", @"playerSources", @"playerTracks", @"radioShows"];
  return [optionalProperties containsObject:propertyName];
}

@end
