#import "HRPRssTags.h"

@implementation HRPRssTags

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.imgTumbnail = @"<media>";
    self.imgMain = @"media";
    self.link = @"link";
    self.postDateTime = @"pubDate";
    self._description = @"content:encoded";
    self.category = @"category";
    self.title = @"title";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"imgTumbnail": @"imgTumbnail", @"imgMain": @"imgMain", @"link": @"link", @"postDateTime": @"postDateTime", @"description": @"_description", @"category": @"category", @"title": @"title", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"imgTumbnail", @"imgMain", @"link", @"postDateTime", @"_description", @"category", @"title", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
