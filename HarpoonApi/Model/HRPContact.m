#import "HRPContact.h"

@implementation HRPContact

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"email": @"email", @"website": @"website", @"phone": @"phone", @"address": @"address", @"twitter": @"twitter", @"facebook": @"facebook", @"whatsapp": @"whatsapp", @"snapchat": @"snapchat", @"googlePlus": @"googlePlus", @"linkedIn": @"linkedIn", @"hashtag": @"hashtag", @"text": @"text", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"email", @"website", @"phone", @"address", @"twitter", @"facebook", @"whatsapp", @"snapchat", @"googlePlus", @"linkedIn", @"hashtag", @"text", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
