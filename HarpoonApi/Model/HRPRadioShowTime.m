#import "HRPRadioShowTime.h"

@implementation HRPRadioShowTime

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.startTime = @"0000";
    self.endTime = @"2359";
    self.weekday = @1.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"startTime": @"startTime", @"endTime": @"endTime", @"weekday": @"weekday", @"id": @"_id", @"radioShowId": @"radioShowId", @"radioShow": @"radioShow" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"startTime", @"endTime", @"weekday", @"_id", @"radioShowId", @"radioShow"];
  return [optionalProperties containsObject:propertyName];
}

@end
