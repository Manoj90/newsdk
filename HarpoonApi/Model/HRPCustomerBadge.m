#import "HRPCustomerBadge.h"

@implementation HRPCustomerBadge

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.offer = @0.0;
    self.event = @0.0;
    self.dealAll = @0.0;
    self.dealCoupon = @0.0;
    self.dealSimple = @0.0;
    self.dealGroup = @0.0;
    self.competition = @0.0;
    self.walletOfferAvailable = @0.0;
    self.walletOfferNew = @0.0;
    self.walletOfferExpiring = @0.0;
    self.walletOfferExpired = @0.0;
    self.notificationUnread = @0.0;
    self.brandFeed = @0.0;
    self.brand = @0.0;
    self.brandActivity = @0.0;
    self.all = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"offer": @"offer", @"event": @"event", @"dealAll": @"dealAll", @"dealCoupon": @"dealCoupon", @"dealSimple": @"dealSimple", @"dealGroup": @"dealGroup", @"competition": @"competition", @"walletOfferAvailable": @"walletOfferAvailable", @"walletOfferNew": @"walletOfferNew", @"walletOfferExpiring": @"walletOfferExpiring", @"walletOfferExpired": @"walletOfferExpired", @"notificationUnread": @"notificationUnread", @"brandFeed": @"brandFeed", @"brand": @"brand", @"brandActivity": @"brandActivity", @"all": @"all", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"offer", @"event", @"dealAll", @"dealCoupon", @"dealSimple", @"dealGroup", @"competition", @"walletOfferAvailable", @"walletOfferNew", @"walletOfferExpiring", @"walletOfferExpired", @"notificationUnread", @"brandFeed", @"brand", @"brandActivity", @"all", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
