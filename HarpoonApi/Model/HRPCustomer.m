#import "HRPCustomer.h"

@implementation HRPCustomer

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.gender = @"other";
    self.followingCount = @0.0;
    self.followerCount = @0.0;
    self.isFollowed = @0;
    self.isFollower = @0;
    self.notificationCount = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"email": @"email", @"firstName": @"firstName", @"lastName": @"lastName", @"dob": @"dob", @"gender": @"gender", @"password": @"password", @"profilePicture": @"profilePicture", @"cover": @"cover", @"profilePictureUpload": @"profilePictureUpload", @"coverUpload": @"coverUpload", @"metadata": @"metadata", @"connection": @"connection", @"privacy": @"privacy", @"followingCount": @"followingCount", @"followerCount": @"followerCount", @"isFollowed": @"isFollowed", @"isFollower": @"isFollower", @"notificationCount": @"notificationCount", @"badge": @"badge", @"authorizationCode": @"authorizationCode", @"id": @"_id", @"appId": @"appId" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"dob", @"gender", @"profilePicture", @"cover", @"profilePictureUpload", @"coverUpload", @"metadata", @"connection", @"privacy", @"followingCount", @"followerCount", @"isFollowed", @"isFollower", @"notificationCount", @"badge", @"authorizationCode", @"_id", @"appId"];
  return [optionalProperties containsObject:propertyName];
}

@end
