#import "HRPHarpoonHpublicApplicationpartner.h"

@implementation HRPHarpoonHpublicApplicationpartner

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"applicationId": @"applicationId", @"brandId": @"brandId", @"status": @"status", @"invitedEmail": @"invitedEmail", @"invitedAt": @"invitedAt", @"acceptedAt": @"acceptedAt", @"createdAt": @"createdAt", @"updatedAt": @"updatedAt", @"configList": @"configList", @"configFeed": @"configFeed", @"configNeedFollow": @"configNeedFollow", @"configAcceptEvent": @"configAcceptEvent", @"configAcceptCoupon": @"configAcceptCoupon", @"configAcceptDealsimple": @"configAcceptDealsimple", @"configAcceptDealgroup": @"configAcceptDealgroup", @"configAcceptNotificationpush": @"configAcceptNotificationpush", @"configAcceptNotificationbeacon": @"configAcceptNotificationbeacon", @"isOwner": @"isOwner", @"configApproveEvent": @"configApproveEvent", @"configApproveCoupon": @"configApproveCoupon", @"configApproveDealsimple": @"configApproveDealsimple", @"configApproveDealgroup": @"configApproveDealgroup", @"udropshipVendor": @"udropshipVendor" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"invitedEmail", @"invitedAt", @"acceptedAt", @"createdAt", @"updatedAt", @"configList", @"configFeed", @"configNeedFollow", @"configAcceptEvent", @"configAcceptCoupon", @"configAcceptDealsimple", @"configAcceptDealgroup", @"configAcceptNotificationpush", @"configAcceptNotificationbeacon", @"isOwner", @"configApproveEvent", @"configApproveCoupon", @"configApproveDealsimple", @"configApproveDealgroup", @"udropshipVendor"];
  return [optionalProperties containsObject:propertyName];
}

@end
