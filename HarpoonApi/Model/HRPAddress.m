#import "HRPAddress.h"

@implementation HRPAddress

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.countryCode = @"IE";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"streetAddress": @"streetAddress", @"streetAddressComp": @"streetAddressComp", @"city": @"city", @"region": @"region", @"countryCode": @"countryCode", @"postcode": @"postcode", @"attnFirstName": @"attnFirstName", @"attnLastName": @"attnLastName", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"streetAddress", @"streetAddressComp", @"city", @"region", @"countryCode", @"postcode", @"attnFirstName", @"attnLastName", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
