#import "HRPAwCollpurDeal.h"

@implementation HRPAwCollpurDeal

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"productId": @"productId", @"productName": @"productName", @"storeIds": @"storeIds", @"isActive": @"isActive", @"isSuccess": @"isSuccess", @"closeState": @"closeState", @"qtyToReachDeal": @"qtyToReachDeal", @"purchasesLeft": @"purchasesLeft", @"maximumAllowedPurchases": @"maximumAllowedPurchases", @"availableFrom": @"availableFrom", @"availableTo": @"availableTo", @"price": @"price", @"autoClose": @"autoClose", @"name": @"name", @"description": @"_description", @"fullDescription": @"fullDescription", @"dealImage": @"dealImage", @"isFeatured": @"isFeatured", @"enableCoupons": @"enableCoupons", @"couponPrefix": @"couponPrefix", @"couponExpireAfterDays": @"couponExpireAfterDays", @"expiredFlag": @"expiredFlag", @"sentBeforeFlag": @"sentBeforeFlag", @"isSuccessedFlag": @"isSuccessedFlag", @"awCollpurDealPurchases": @"awCollpurDealPurchases" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"availableFrom", @"availableTo", @"fullDescription", @"isFeatured", @"couponExpireAfterDays", @"expiredFlag", @"sentBeforeFlag", @"isSuccessedFlag", @"awCollpurDealPurchases"];
  return [optionalProperties containsObject:propertyName];
}

@end
