#import "HRPStripeCoupon.h"

@implementation HRPStripeCoupon

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"object": @"object", @"amountOff": @"amountOff", @"created": @"created", @"currency": @"currency", @"durationInMonths": @"durationInMonths", @"livemode": @"livemode", @"maxRedemptions": @"maxRedemptions", @"metadata": @"metadata", @"percentOff": @"percentOff", @"redeemBy": @"redeemBy", @"timesRedeemed": @"timesRedeemed", @"valid": @"valid", @"duration": @"duration", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"object", @"amountOff", @"created", @"currency", @"durationInMonths", @"livemode", @"maxRedemptions", @"metadata", @"percentOff", @"redeemBy", @"timesRedeemed", @"valid", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
