#import "HRPFacebookConfig.h"

@implementation HRPFacebookConfig

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.scopes = @"first_name, last_name, email, id, gender, cover{source}";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"appId": @"appId", @"appToken": @"appToken", @"scopes": @"scopes" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"scopes"];
  return [optionalProperties containsObject:propertyName];
}

@end
