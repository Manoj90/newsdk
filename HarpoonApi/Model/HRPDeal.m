#import "HRPDeal.h"

@implementation HRPDeal

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.price = @0.0;
    self.hasClaimed = @0;
    self.qtyLeft = @0.0;
    self.qtyTotal = @0.0;
    self.qtyClaimed = @0.0;
    self.baseCurrency = @"EUR";
    self.actionText = @"Sold Out";
    self.status = @"soldOut";
    self.isFeatured = @0;
    self.qtyPerOrder = @1.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"type": @"type", @"price": @"price", @"hasClaimed": @"hasClaimed", @"qtyLeft": @"qtyLeft", @"qtyTotal": @"qtyTotal", @"qtyClaimed": @"qtyClaimed", @"name": @"name", @"description": @"_description", @"cover": @"cover", @"campaignType": @"campaignType", @"category": @"category", @"topic": @"topic", @"alias": @"alias", @"from": @"from", @"to": @"to", @"baseCurrency": @"baseCurrency", @"priceText": @"priceText", @"bannerText": @"bannerText", @"checkoutLink": @"checkoutLink", @"nearestVenue": @"nearestVenue", @"actionText": @"actionText", @"status": @"status", @"collectionNotes": @"collectionNotes", @"termsConditions": @"termsConditions", @"locationLink": @"locationLink", @"altLink": @"altLink", @"redemptionType": @"redemptionType", @"brand": @"brand", @"closestPurchase": @"closestPurchase", @"isFeatured": @"isFeatured", @"qtyPerOrder": @"qtyPerOrder", @"shareLink": @"shareLink", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"type", @"price", @"hasClaimed", @"qtyLeft", @"qtyTotal", @"qtyClaimed", @"name", @"_description", @"cover", @"campaignType", @"category", @"topic", @"alias", @"from", @"to", @"baseCurrency", @"priceText", @"bannerText", @"checkoutLink", @"nearestVenue", @"actionText", @"status", @"collectionNotes", @"termsConditions", @"locationLink", @"altLink", @"redemptionType", @"brand", @"closestPurchase", @"isFeatured", @"qtyPerOrder", @"shareLink", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
