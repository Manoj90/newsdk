#import "HRPBrand.h"

@implementation HRPBrand

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.followerCount = @0.0;
    self.isFollowed = @0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"name": @"name", @"logo": @"logo", @"cover": @"cover", @"description": @"_description", @"email": @"email", @"phone": @"phone", @"address": @"address", @"managerName": @"managerName", @"followerCount": @"followerCount", @"isFollowed": @"isFollowed", @"categories": @"categories", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"name", @"logo", @"cover", @"_description", @"email", @"phone", @"address", @"managerName", @"followerCount", @"isFollowed", @"categories", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
