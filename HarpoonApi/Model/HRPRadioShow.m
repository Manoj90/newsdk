#import "HRPRadioShow.h"

@implementation HRPRadioShow

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.contentType = @"image";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"name": @"name", @"description": @"_description", @"content": @"content", @"contentType": @"contentType", @"contact": @"contact", @"starts": @"starts", @"ends": @"ends", @"type": @"type", @"radioShowTimeCurrent": @"radioShowTimeCurrent", @"imgShow": @"imgShow", @"sponsorTrack": @"sponsorTrack", @"id": @"_id", @"radioStreamId": @"radioStreamId", @"playlistItemId": @"playlistItemId", @"radioPresenters": @"radioPresenters", @"radioStream": @"radioStream", @"radioShowTimes": @"radioShowTimes", @"playlistItem": @"playlistItem" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_description", @"content", @"contentType", @"contact", @"starts", @"ends", @"type", @"radioShowTimeCurrent", @"imgShow", @"sponsorTrack", @"_id", @"radioStreamId", @"playlistItemId", @"radioPresenters", @"radioStream", @"radioShowTimes", @"playlistItem"];
  return [optionalProperties containsObject:propertyName];
}

@end
