#import "HRPStripeInvoice.h"

@implementation HRPStripeInvoice

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"object": @"object", @"amountDue": @"amountDue", @"applicationFee": @"applicationFee", @"attemptCount": @"attemptCount", @"attempted": @"attempted", @"charge": @"charge", @"closed": @"closed", @"currency": @"currency", @"date": @"date", @"description": @"_description", @"endingBalance": @"endingBalance", @"forgiven": @"forgiven", @"livemode": @"livemode", @"metadata": @"metadata", @"nextPaymentAttempt": @"nextPaymentAttempt", @"paid": @"paid", @"periodEnd": @"periodEnd", @"periodStart": @"periodStart", @"receiptNumber": @"receiptNumber", @"subscriptionProrationDate": @"subscriptionProrationDate", @"subtotal": @"subtotal", @"tax": @"tax", @"taxPercent": @"taxPercent", @"total": @"total", @"webhooksDeliveredAt": @"webhooksDeliveredAt", @"customer": @"customer", @"discount": @"discount", @"statmentDescriptor": @"statmentDescriptor", @"subscription": @"subscription", @"lines": @"lines", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"object", @"amountDue", @"applicationFee", @"attemptCount", @"attempted", @"charge", @"closed", @"currency", @"date", @"_description", @"endingBalance", @"forgiven", @"livemode", @"metadata", @"nextPaymentAttempt", @"paid", @"periodEnd", @"periodStart", @"receiptNumber", @"subscriptionProrationDate", @"subtotal", @"tax", @"taxPercent", @"total", @"webhooksDeliveredAt", @"discount", @"statmentDescriptor", @"subscription", @"lines", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
