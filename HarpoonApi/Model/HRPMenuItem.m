#import "HRPMenuItem.h"

@implementation HRPMenuItem

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.featureRadioDontShowOnCamera = @1;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"name": @"name", @"url": @"url", @"image": @"image", @"visibility": @"visibility", @"sortOrder": @"sortOrder", @"pageAction": @"pageAction", @"pageActionIcon": @"pageActionIcon", @"pageActionLink": @"pageActionLink", @"styleCss": @"styleCss", @"featureRadioDontShowOnCamera": @"featureRadioDontShowOnCamera", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"name", @"url", @"image", @"visibility", @"sortOrder", @"pageAction", @"pageActionIcon", @"pageActionLink", @"styleCss", @"featureRadioDontShowOnCamera", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
