#import "HRPCompetitionCheckout.h"

@implementation HRPCompetitionCheckout

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.status = @"unknown";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"cartId": @"cartId", @"url": @"url", @"status": @"status", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"cartId", @"url", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
