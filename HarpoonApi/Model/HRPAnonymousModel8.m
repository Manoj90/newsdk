#import "HRPAnonymousModel8.h"

@implementation HRPAnonymousModel8

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"brandId": @"brandId", @"competitionId": @"competitionId", @"couponId": @"couponId", @"customerId": @"customerId", @"eventId": @"eventId", @"dealPaid": @"dealPaid", @"dealGroup": @"dealGroup" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"brandId", @"competitionId", @"couponId", @"customerId", @"eventId", @"dealPaid", @"dealGroup"];
  return [optionalProperties containsObject:propertyName];
}

@end
