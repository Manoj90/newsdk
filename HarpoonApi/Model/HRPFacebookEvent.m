#import "HRPFacebookEvent.h"

@implementation HRPFacebookEvent

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.maybeCount = @0.0;
    self.noreplyCount = @0.0;
    self.attendingCount = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"node": @"node", @"category": @"category", @"updatedTime": @"updatedTime", @"maybeCount": @"maybeCount", @"noreplyCount": @"noreplyCount", @"attendingCount": @"attendingCount", @"place": @"place", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"node", @"category", @"updatedTime", @"maybeCount", @"noreplyCount", @"attendingCount", @"place", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
