#import "HRPRssFeed.h"

@implementation HRPRssFeed

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.sortOrder = @0.0;
    self.noAds = @0;
    self.adMRectVisible = @0;
    self.adMRectPosition = @0.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"category": @"category", @"subCategory": @"subCategory", @"url": @"url", @"name": @"name", @"sortOrder": @"sortOrder", @"styleCss": @"styleCss", @"noAds": @"noAds", @"adMRectVisible": @"adMRectVisible", @"adMRectPosition": @"adMRectPosition", @"id": @"_id", @"appId": @"appId", @"rssFeedGroupId": @"rssFeedGroupId", @"rssFeedGroup": @"rssFeedGroup" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"subCategory", @"name", @"sortOrder", @"styleCss", @"noAds", @"adMRectVisible", @"adMRectPosition", @"_id", @"appId", @"rssFeedGroupId", @"rssFeedGroup"];
  return [optionalProperties containsObject:propertyName];
}

@end
