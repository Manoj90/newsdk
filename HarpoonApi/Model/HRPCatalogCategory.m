#import "HRPCatalogCategory.h"

@implementation HRPCatalogCategory

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"parentId": @"parentId", @"createdAt": @"createdAt", @"updatedAt": @"updatedAt", @"path": @"path", @"position": @"position", @"level": @"level", @"childrenCount": @"childrenCount", @"storeId": @"storeId", @"allChildren": @"allChildren", @"availableSortBy": @"availableSortBy", @"children": @"children", @"customApplyToProducts": @"customApplyToProducts", @"customDesign": @"customDesign", @"customDesignFrom": @"customDesignFrom", @"customDesignTo": @"customDesignTo", @"customLayoutUpdate": @"customLayoutUpdate", @"customUseParentSettings": @"customUseParentSettings", @"defaultSortBy": @"defaultSortBy", @"description": @"_description", @"displayMode": @"displayMode", @"filterPriceRange": @"filterPriceRange", @"image": @"image", @"includeInMenu": @"includeInMenu", @"isActive": @"isActive", @"isAnchor": @"isAnchor", @"landingPage": @"landingPage", @"metaDescription": @"metaDescription", @"metaKeywords": @"metaKeywords", @"metaTitle": @"metaTitle", @"name": @"name", @"pageLayout": @"pageLayout", @"pathInStore": @"pathInStore", @"thumbnail": @"thumbnail", @"ummCatLabel": @"ummCatLabel", @"ummCatTarget": @"ummCatTarget", @"ummDdBlocks": @"ummDdBlocks", @"ummDdColumns": @"ummDdColumns", @"ummDdProportions": @"ummDdProportions", @"ummDdType": @"ummDdType", @"ummDdWidth": @"ummDdWidth", @"urlKey": @"urlKey", @"urlPath": @"urlPath", @"catalogProducts": @"catalogProducts" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"createdAt", @"updatedAt", @"allChildren", @"availableSortBy", @"children", @"customApplyToProducts", @"customDesign", @"customDesignFrom", @"customDesignTo", @"customLayoutUpdate", @"customUseParentSettings", @"defaultSortBy", @"_description", @"displayMode", @"filterPriceRange", @"image", @"includeInMenu", @"isActive", @"isAnchor", @"landingPage", @"metaDescription", @"metaKeywords", @"metaTitle", @"name", @"pageLayout", @"pathInStore", @"thumbnail", @"ummCatLabel", @"ummCatTarget", @"ummDdBlocks", @"ummDdColumns", @"ummDdProportions", @"ummDdType", @"ummDdWidth", @"urlKey", @"urlPath", @"catalogProducts"];
  return [optionalProperties containsObject:propertyName];
}

@end
