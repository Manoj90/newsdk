#import "HRPAwCollpurCoupon.h"

@implementation HRPAwCollpurCoupon

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"dealId": @"dealId", @"purchaseId": @"purchaseId", @"couponCode": @"couponCode", @"status": @"status", @"couponDeliveryDatetime": @"couponDeliveryDatetime", @"couponDateUpdated": @"couponDateUpdated", @"redeemedAt": @"redeemedAt", @"redeemedByTerminal": @"redeemedByTerminal" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"couponDeliveryDatetime", @"couponDateUpdated", @"redeemedAt", @"redeemedByTerminal"];
  return [optionalProperties containsObject:propertyName];
}

@end
