#import "HRPRadioStream.h"

@implementation HRPRadioStream

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"name": @"name", @"description": @"_description", @"urlHQ": @"urlHQ", @"urlLQ": @"urlLQ", @"starts": @"starts", @"ends": @"ends", @"metaDataUrl": @"metaDataUrl", @"imgStream": @"imgStream", @"sponsorTrack": @"sponsorTrack", @"tritonConfig": @"tritonConfig", @"id": @"_id", @"appId": @"appId", @"radioShows": @"radioShows" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_description", @"starts", @"ends", @"metaDataUrl", @"imgStream", @"sponsorTrack", @"tritonConfig", @"_id", @"appId", @"radioShows"];
  return [optionalProperties containsObject:propertyName];
}

@end
