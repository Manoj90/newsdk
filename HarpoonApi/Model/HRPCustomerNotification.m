#import "HRPCustomerNotification.h"

@implementation HRPCustomerNotification

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.actionCode = @"post";
    self.status = @"unread";
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"message": @"message", @"cover": @"cover", @"coverUpload": @"coverUpload", @"from": @"from", @"related": @"related", @"link": @"link", @"actionCode": @"actionCode", @"status": @"status", @"sentAt": @"sentAt", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"message", @"cover", @"coverUpload", @"from", @"related", @"link", @"actionCode", @"status", @"sentAt", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
