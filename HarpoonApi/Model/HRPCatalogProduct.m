#import "HRPCatalogProduct.h"

@implementation HRPCatalogProduct

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"attributeSetId": @"attributeSetId", @"typeId": @"typeId", @"actionText": @"actionText", @"agreementId": @"agreementId", @"alias": @"alias", @"altLink": @"altLink", @"campaignId": @"campaignId", @"campaignLiveFrom": @"campaignLiveFrom", @"campaignLiveTo": @"campaignLiveTo", @"campaignShared": @"campaignShared", @"campaignStatus": @"campaignStatus", @"campaignType": @"campaignType", @"checkoutLink": @"checkoutLink", @"collectionNotes": @"collectionNotes", @"cost": @"cost", @"couponCodeText": @"couponCodeText", @"couponCodeType": @"couponCodeType", @"couponType": @"couponType", @"createdAt": @"createdAt", @"creatorId": @"creatorId", @"description": @"_description", @"externalId": @"externalId", @"facebookAttendingCount": @"facebookAttendingCount", @"facebookCategory": @"facebookCategory", @"facebookId": @"facebookId", @"facebookMaybeCount": @"facebookMaybeCount", @"facebookNode": @"facebookNode", @"facebookNoreplyCount": @"facebookNoreplyCount", @"facebookPlace": @"facebookPlace", @"facebookUpdatedTime": @"facebookUpdatedTime", @"giftMessageAvailable": @"giftMessageAvailable", @"hasOptions": @"hasOptions", @"image": @"image", @"imageLabel": @"imageLabel", @"isRecurring": @"isRecurring", @"linksExist": @"linksExist", @"linksPurchasedSeparately": @"linksPurchasedSeparately", @"linksTitle": @"linksTitle", @"locationLink": @"locationLink", @"msrp": @"msrp", @"msrpDisplayActualPriceType": @"msrpDisplayActualPriceType", @"msrpEnabled": @"msrpEnabled", @"name": @"name", @"newsFromDate": @"varNewsFromDate", @"newsToDate": @"varNewsToDate", @"notificationBadge": @"notificationBadge", @"notificationInvisible": @"notificationInvisible", @"notificationSilent": @"notificationSilent", @"partnerId": @"partnerId", @"passedValidation": @"passedValidation", @"price": @"price", @"priceText": @"priceText", @"priceType": @"priceType", @"priceView": @"priceView", @"pushwooshIds": @"pushwooshIds", @"pushwooshTokens": @"pushwooshTokens", @"recurringProfile": @"recurringProfile", @"redemptionType": @"redemptionType", @"requiredOptions": @"requiredOptions", @"reviewHtml": @"reviewHtml", @"shipmentType": @"shipmentType", @"shortDescription": @"shortDescription", @"sku": @"sku", @"skuType": @"skuType", @"smallImage": @"smallImage", @"smallImageLabel": @"smallImageLabel", @"specialFromDate": @"specialFromDate", @"specialPrice": @"specialPrice", @"specialToDate": @"specialToDate", @"taxClassId": @"taxClassId", @"thumbnail": @"thumbnail", @"thumbnailLabel": @"thumbnailLabel", @"udropshipCalculateRates": @"udropshipCalculateRates", @"udropshipVendor": @"udropshipVendor", @"udropshipVendorValue": @"udropshipVendorValue", @"udtiershipRates": @"udtiershipRates", @"udtiershipUseCustom": @"udtiershipUseCustom", @"unlockTime": @"unlockTime", @"updatedAt": @"updatedAt", @"urlKey": @"urlKey", @"urlPath": @"urlPath", @"validationReport": @"validationReport", @"venueList": @"venueList", @"visibility": @"visibility", @"visibleFrom": @"visibleFrom", @"visibleTo": @"visibleTo", @"weight": @"weight", @"weightType": @"weightType", @"termsConditions": @"termsConditions", @"competitionQuestion": @"competitionQuestion", @"competitionAnswer": @"competitionAnswer", @"competitionWinnerType": @"competitionWinnerType", @"competitionMultijoinStatus": @"competitionMultijoinStatus", @"competitionMultijoinReset": @"competitionMultijoinReset", @"competitionMultijoinCooldown": @"competitionMultijoinCooldown", @"competitionWinnerEmail": @"competitionWinnerEmail", @"competitionQuestionValidate": @"competitionQuestionValidate", @"udropshipVendors": @"udropshipVendors", @"productEventCompetition": @"productEventCompetition", @"inventory": @"inventory", @"catalogCategories": @"catalogCategories", @"checkoutAgreement": @"checkoutAgreement", @"awCollpurDeal": @"awCollpurDeal", @"creator": @"creator", @"udropshipVendorProducts": @"udropshipVendorProducts" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"actionText", @"agreementId", @"alias", @"altLink", @"campaignId", @"campaignLiveFrom", @"campaignLiveTo", @"campaignShared", @"campaignStatus", @"campaignType", @"checkoutLink", @"collectionNotes", @"cost", @"couponCodeText", @"couponCodeType", @"couponType", @"createdAt", @"creatorId", @"_description", @"externalId", @"facebookAttendingCount", @"facebookCategory", @"facebookId", @"facebookMaybeCount", @"facebookNode", @"facebookNoreplyCount", @"facebookPlace", @"facebookUpdatedTime", @"giftMessageAvailable", @"image", @"imageLabel", @"isRecurring", @"linksExist", @"linksPurchasedSeparately", @"linksTitle", @"locationLink", @"msrp", @"msrpDisplayActualPriceType", @"msrpEnabled", @"name", @"varNewsFromDate", @"varNewsToDate", @"notificationBadge", @"notificationInvisible", @"notificationSilent", @"partnerId", @"passedValidation", @"price", @"priceText", @"priceType", @"priceView", @"pushwooshIds", @"pushwooshTokens", @"recurringProfile", @"redemptionType", @"reviewHtml", @"shipmentType", @"shortDescription", @"sku", @"skuType", @"smallImage", @"smallImageLabel", @"specialFromDate", @"specialPrice", @"specialToDate", @"taxClassId", @"thumbnail", @"thumbnailLabel", @"udropshipCalculateRates", @"udropshipVendor", @"udropshipVendorValue", @"udtiershipRates", @"udtiershipUseCustom", @"unlockTime", @"updatedAt", @"urlKey", @"urlPath", @"validationReport", @"venueList", @"visibility", @"visibleFrom", @"visibleTo", @"weight", @"weightType", @"termsConditions", @"competitionQuestion", @"competitionAnswer", @"competitionWinnerType", @"competitionMultijoinStatus", @"competitionMultijoinReset", @"competitionMultijoinCooldown", @"competitionWinnerEmail", @"competitionQuestionValidate", @"udropshipVendors", @"productEventCompetition", @"inventory", @"catalogCategories", @"checkoutAgreement", @"awCollpurDeal", @"creator", @"udropshipVendorProducts"];
  return [optionalProperties containsObject:propertyName];
}

@end
