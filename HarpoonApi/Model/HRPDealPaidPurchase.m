#import "HRPDealPaidPurchase.h"

@implementation HRPDealPaidPurchase

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.isAvailable = @0;
    self.status = @"unknown";
    self.unlockTime = @15.0;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"orderId": @"orderId", @"name": @"name", @"code": @"code", @"isAvailable": @"isAvailable", @"createdAt": @"createdAt", @"expiredAt": @"expiredAt", @"redeemedAt": @"redeemedAt", @"redeemedByTerminal": @"redeemedByTerminal", @"redemptionType": @"redemptionType", @"status": @"status", @"unlockTime": @"unlockTime", @"id": @"_id" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"orderId", @"name", @"code", @"isAvailable", @"createdAt", @"expiredAt", @"redeemedAt", @"redeemedByTerminal", @"redemptionType", @"status", @"unlockTime", @"_id"];
  return [optionalProperties containsObject:propertyName];
}

@end
