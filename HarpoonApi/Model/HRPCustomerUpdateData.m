#import "HRPCustomerUpdateData.h"

@implementation HRPCustomerUpdateData

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"firstName": @"firstName", @"lastName": @"lastName", @"email": @"email", @"gender": @"gender", @"dob": @"dob", @"metadata": @"metadata", @"privacy": @"privacy", @"profilePictureUpload": @"profilePictureUpload", @"coverUpload": @"coverUpload" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"lastName", @"email", @"gender", @"dob", @"metadata", @"privacy", @"profilePictureUpload", @"coverUpload"];
  return [optionalProperties containsObject:propertyName];
}

@end
