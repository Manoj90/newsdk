#import "HRPFormRow.h"

@implementation HRPFormRow

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    self.type = @"text";
    self.hidden = @0;
    self.required = @1;
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"metadataKey": @"metadataKey", @"type": @"type", @"title": @"title", @"defaultValue": @"defaultValue", @"placeholder": @"placeholder", @"hidden": @"hidden", @"required": @"required", @"validationRegExp": @"validationRegExp", @"validationMessage": @"validationMessage", @"selectorOptions": @"selectorOptions" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"defaultValue", @"placeholder", @"hidden", @"required", @"validationRegExp", @"validationMessage", @"selectorOptions"];
  return [optionalProperties containsObject:propertyName];
}

@end
